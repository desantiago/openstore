import { ApolloClient, gql, InMemoryCache } from '@apollo/client';
import { GetStaticPaths, GetStaticProps } from 'next';
import { useRouter } from 'next/router';
import SingleProduct from '../../components/SingleProduct';

const PRODUCT_QUERY = gql`
    query PRODUCT_QUERY($key: ID!) {
        product(key: $key) {
            name
            description
            sizeAndFit
            details
            category {
                key
                description
            }
            subCategory {
                key
                description
            }
            brand {
                key
                description
            }
            colors {
                key
                name
            }
            images {
                color {
                    key
                    name
                }
                images {
                    key
                    filename
                }
            }
            sizes {
                color {
                    key
                    name
                }
                sizes
            }
            prices {
                color {
                    key
                    name
                }
                prices {
                    size
                    price
                }
            }
        }
    }
`

function ProductPage({ product }) {
    const router = useRouter()
    const { id } = router.query

    // const {data, loading, error} = useQuery(PRODUCT_QUERY, {
    //     variables: id
    // })

    // if (loading) return <p>Loading...</p> 
    // if (error) return <p>Error {error}</p>

    return (
        <SingleProduct id={id} product={product} />
    )
}

// export const getStaticPaths: GetStaticPaths<{ slug: string }> = async () => {
export const getStaticPaths: GetStaticPaths<{ id: string }> = async () => {
    return {
        paths: [], //indicates that no page needs be created at build time
        fallback: 'blocking' //indicates the type of fallback
    }
}

export const getStaticProps: GetStaticProps = async (context) => {
    console.log("getStaticProps SingleProduct");
    console.log("context: ", context);

    const client2 = new ApolloClient({
        uri: 'http://localhost:3000/graphql',
        cache: new InMemoryCache()
    });

    const { data } = await client2.query({
        query: PRODUCT_QUERY,
        variables: {
            key: context.params.id
        }
    });

    console.log(data);

    return {
        props: {
            product: data.product,
        }
    }
}

export default ProductPage;