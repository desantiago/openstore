// import { useState } from 'react';
import styled from 'styled-components';

export default function Drawer({ title, open, onClose, children }) {
    return (
        <DrawerStyles open={open}>
            <header>
                <Title>{title}</Title>
                <button onClick={onClose}>Close</button>
            </header>
            {children}
            <footer>
            </footer>
        </DrawerStyles>
    )
}

const Title = styled.span`
    font-family: 'Inter', sans-serif;
    font-size: 1.5rem;
`

const DrawerStyles = styled.div`
    padding: 20px;
    position: relative;
    background: white;
    position: fixed;
    height: 100%;
    top: 0;
    right: 0;
    width: 30%;
    min-width: 400px;
    bottom: 0;
    transform: translateX(100%);
    transition: all 0.3s;
    box-shadow: 0 0 10px 3px rgba(0, 0, 0, 0.2);
    z-index: 5;
    display: grid;
    grid-template-rows: auto 1fr auto;

    ${(props) => props.open && `transform: translateX(0);`};

    header {
        border-bottom: 2px solid #eaeaea;
        margin-bottom: 1rem;
        padding-bottom: 1rem;
    }
    footer {
        font-family: 'Inter', sans-serif;
        margin-top: 1rem;
        padding-top: 1rem;
        display: grid;
        grid-template-columns: auto auto;
        justify-content: top;
        font-size: 2rem;
        font-weight: 900;
        p {
            margin: 0;
        }
    }
    ul {
        margin: 0;
        padding: 0;
        list-style: none;
        overflow: scroll;
    }
`;
