import { gql, useMutation } from '@apollo/client';
import styled from 'styled-components';
import Drawer from "./Drawer";
import useFormSimple from "../lib/useFormSimple";
import { useToasts } from 'react-toast-notifications';

import {
    ALL_BRANDS,
} from '../lib/queryProducts';

const CREATE_BRAND_MUTATION = gql`
    mutation CREATE_BRAND_MUTATION(
        $description: String!
        $slug: String
    ) {
        createBrand(description: $description, slug: $slug) {
            id
        }
    }
`

export default function CreateBrand({ open, onClose }) {
    const { addToast } = useToasts();

    const {
        inputs,
        handleChange,
        clearForm
    } = useFormSimple({ brandDescription: '', brandSlug: '' });

    // const [createBrand, { loading, error, data }] = useMutation(CREATE_BRAND_MUTATION, {
    //     variables: inputs,
    //     refetchQueries: [{
    //         query: ALL_BRANDS, variables: {}
    //     }]
    // });
    const [createBrand, { loading, error, data }] = useMutation(CREATE_BRAND_MUTATION);

    const saveBrand = async () => {
        const res = await createBrand({
            variables: {
                description: inputs.brandDescription,
                slug: inputs.brandSlug
            },
            refetchQueries: [{
                query: ALL_BRANDS, variables: {}
            }]
        });

        if (res.data.createBrand.id) {
            clearForm();
            addToast("Brand was inserted", {
                appearance: 'success',
                autoDismiss: true,
            });
        }
    }

    // <fieldset disabled={loading} aria-busy={loading}>
    return (
        <Drawer title="Add Brand" open={open} onClose={onClose}>
            <Form onSubmit={(e) => {
                e.preventDefault();
                saveBrand();
            }}>
                <fieldset disabled={loading} aria-busy={loading}>
                    <label htmlFor="brandDescription">
                        Brand Name
                        <input
                            type="text"
                            value={inputs.brandDescription}
                            id="brandDescription" name="brandDescription"
                            placeholder="Description"
                            onChange={handleChange} />
                    </label>

                    <label htmlFor="slug">
                        Slug
                        <input
                            type="text"
                            value={inputs.brandSlug}
                            id="brandSlug" name="brandSlug"
                            placeholder="Slug"
                            onChange={handleChange} />
                    </label>

                    <button type="submit">Add Brand</button>
                </fieldset>
            </Form>
        </Drawer>
    )
}

const Form = styled.form`
    min-width: 350px;
    max-width: 550px;
    font-size: 1.15rem;
    line-height: 1.5;
    font-weight: 500;
    font-family: Helvetica;
    label {
        display: block;
        margin-bottom: 1rem;
    }
    input,
    textarea,
    select {
        width: 100%;
        padding: 0.5rem;
        font-size: 1rem;
        border: 1px solid #989898;
        &:focus {
            outline: 0;
            border-color: var(--main-color);
        }
    }
    button,
    input[type='submit'] {
        width: auto;
        background: var(--main-color);
        color: white;
        border: 0;
        font-size: 1.5rem;
        font-weight: 600;
        padding: 0.25rem 1.2rem;
    }
    fieldset {
        border: 0;
        padding: 0;
        
        &[disabled] {
            opacity: 0.5;
        }
    }
`
