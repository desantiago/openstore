import Link from 'next/link'
import styled from 'styled-components'

export default function SideMenu() {
    return (
        <Container>
            <ul>
                <li>
                    <Link href="/addproduct">
                        <a>
                            <i className="lni lni-layers"></i>
                        </a>
                    </Link>
                    Products
                </li>
                <li>
                    <Link href="/orders">
                        <a><i className="lni lni-files"></i></a>
                    </Link>
                    Orders
                </li>
            </ul>
        </Container>
    )
}

const Container = styled.div`
    color: #eaeaea;
    background-color: #494949;

    ul {
        margin-top: 1.0rem;
        list-style: none;
    }

    li {
        font-size: 0.6rem;
        text-align:center;
        font-family: Inter;
        margin-bottom: 1.0rem;
    }

    a {
        color: inherit;
        text-decoration: none;
    }

    i {
        padding: 0.0rem 1.0rem;
        font-size: 1.5rem;
        margin-bottom: 0.2rem;
    }
`
