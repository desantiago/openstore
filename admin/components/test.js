
const text = null;
// const text = "one two three fourth five";

function reverseWords(text) {
    if (!text) return '';
    let words = text.split(' ');

    // for (let i = 0; i < words.length - 1; i++) {
    //     const word = words.pop();
    //     words.splice(i, 0, word);
    //     console.log(words);
    // }

    const length = words.length / 2;

    for (let i = 0; i < length; i++) {
        const lastPos = (words.length - 1) - i;
        const t = words[lastPos];
        words[lastPos] = words[i];
        words[i] = t;
    }

    // let result = [];
    // for (let i = words.length - 1; i >= 0; i--) {
    //     result.push(words[i]);
    // }

    return words;
}

console.log(reverseWords(text));