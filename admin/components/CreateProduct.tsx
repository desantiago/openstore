import { ChangeEvent, useState, useEffect, useContext, Fragment, useRef } from "react";
import { gql, useMutation } from "@apollo/client";

import styled, { keyframes } from 'styled-components';
import { ToastProvider, useToasts } from 'react-toast-notifications';

import { useForm, FormFields, File, ValidationMessages } from "../lib/useForm"
// import { useFormFile, FormFieldsFile } from "../lib/useFormFile"
import Form from "../components/styles/Form";
import ProductList from "./ProductList";
// import { ProductProvider } from '../lib/productContext';
import { PRODUCT_LIST_QUERY } from '../lib/queryProducts';
import ProductContext from '../lib/productContext';
import FileUpload from "./FileUpload";
import MarkdownEditor from './MarkdownEditor';
import Header from './Header';
import ImageSelector from './ImageSelector';

// import { Image, Video, Transformation, CloudinaryContext } from 'cloudinary-react';
import { Image, CloudinaryContext } from 'cloudinary-react';
// import { useCombobox } from 'downshift';
import { cloudName } from '../config';
import Drawer from "./Drawer";
import CreateBrand from "./CreateBrand";
import CreateColor from "./CreateColor";
import Modal from "./Modal";
import ModalRelatedProducts from "./ModalRelatedProducts";
import { ProductStateProvider } from '../lib/relatedProductsContext';

const CREATE_PRODUCT_MUTATION = gql`
    mutation CREATE_PRODUCT_MUTATION(
        $name: String!
        $id: String
        $slug: String
        $price: Float
        $startSize: Int
        $endSize: Int
        $description: String!
        $sizeAndFit: String
        $details: String
        $brand: String!
        $category: String!
        $subCategory: String!
        $colors: [String]
        $images: [ImagesInput]
        $sizes: [SizesInput]
        $prices: [PricesInput]
        $thumbnail: ImageColorInput
        $mainImage: ImageColorInput
        $relatedProducts: [RelatedProductInput]
    ) {
        createProduct(input: {
            key: ""
            name: $name
            externalId: $id
            slug: $slug
            price: $price
            startSize: $startSize
            endSize: $endSize
            description: $description
            sizeAndFit: $sizeAndFit
            details: $details
            timeStamp: ""
            brand: $brand
            category: $category
            subCategory: $subCategory
            colors: $colors
            images: $images
            sizes: $sizes
            prices: $prices
            thumbnail: $thumbnail
            mainImage: $mainImage
            relatedProducts: $relatedProducts
        }) {
            id
        }
    }    
`

const UPDATE_PRODUCT_MUTATION = gql`
    mutation UPDATE_PRODUCT_MUTATION(
        $key: ID!
        $name: String!
        $externalId: String
        $slug: String
        $price: Float
        $startSize: Int
        $endSize: Int
        $description: String!
        $sizeAndFit: String
        $details: String
        $brand: String!
        $category: String!
        $subCategory: String!
        $colors: [String],
        $images: [ImagesInput]
        $sizes: [SizesInput]
        $prices: [PricesInput]
        $thumbnail: ImageColorInput
        $mainImage: ImageColorInput
        $relatedProducts: [RelatedProductInput]
    ) {
        updateProduct(key: $key, input: {
            key: ""
            name: $name
            externalId: $externalId
            slug: $slug
            price: $price
            startSize: $startSize
            endSize: $endSize
            description: $description
            sizeAndFit: $sizeAndFit
            details: $details
            timeStamp: ""
            brand: $brand
            category: $category
            subCategory: $subCategory
            colors: $colors
            images: $images
            sizes: $sizes
            prices: $prices
            thumbnail: $thumbnail
            mainImage: $mainImage
            relatedProducts: $relatedProducts
        }) {
            id
        }
    }
`

const DELETE_PRODUCT_MUTATION = gql`
    mutation DELETE_PRODUCT_MUTATION( $key: ID! ) {
        deleteProduct(key: $key) {
            id
        }
    }
`

const UPLOAD_FILES = gql`
    mutation multiUpload($files: Upload!) {
        multiUpload(files: $files) {
            id
            public_id
            filename
            mimetype
            encoding
        }
    }
`;

const DELETE_FILE_MUTATION = gql`
    mutation DELETE_FILE_MUTATION($key: String) {
        deleteFile(key: $key) {
            id
        }
    }
`

function CreateProduct() {
    const [searchId, setSearchId] = useState('');
    const [dragId, setDragId] = useState('');
    const [relatedDragId, setRelatedDragId] = useState('');

    const { addToast } = useToasts();
    const {
        brands,
        categories,
        colors,
        product,
        selectProduct,
        setSelectedProduct,
        selectedSubCategory,
        selectedBrand,
    }: any = useContext(ProductContext);

    const INSERT = 'insert';
    const UPDATE = 'update';

    const initialValue: FormFields = {
        name: '',
        price: 0,
        description: '',
        sizeAndFit: '',
        details: '',
        colors: [],
        images: [],
        prices: [],
        sizes: [],
        brand: brands && brands[0] && brands[0].id,
        category: categories && categories[0] && categories[0].id,
        subCategory: categories && categories[0] && categories[0].subcategories[0].id,
        startSize: '',
        endSize: '',
        files: [],
        externalId: '',
        slug: '',
        imagesStack: [],
        thumbnail: null,
        mainImage: null,
        thumbnailImg: null,
        mainImgImg: null,
        relatedProducts: [],
        relatedProductsData: [],
        slugExists: false,
    };

    // const [images, setImages] = useState<File[]>([]);
    const { inputs,
        handleChange,
        // clearForm,
        setColor,
        // updatePrice,
        // updateSizes,
        removeImageFromList,
        removeSizeFromList,
        changePrice,
        // addImages,
        setFiles,
        loadProduct,
        initialValues,
        setSlug,
        swapImages,
        setThumbnail,
        setMainImg,
        setRelatedProducts,
        swapRelatedProducts,
        colorSelected,
        setColorSelected,
        validate,
        pristine
    } = useForm(initialValue, brands);

    const [createProduct, { loading, error, data }] = useMutation(CREATE_PRODUCT_MUTATION, {
        variables: inputs,
        refetchQueries: [{
            query: PRODUCT_LIST_QUERY, variables: {
                subCategory: selectedSubCategory,
                brands: selectedBrand
            }
        }]
    });
    const [updateProduct, { loading: updateLoading, error: updateError, data: updateData }] = useMutation(UPDATE_PRODUCT_MUTATION, {
        variables: {
            ...inputs,
            key: product?.id,
        },
        refetchQueries: [{
            query: PRODUCT_LIST_QUERY, variables: {
                subCategory: selectedSubCategory,
                brands: selectedBrand
            }
        }]
    });
    const [deleteProduct, { loading: loadingDelete }] = useMutation(DELETE_PRODUCT_MUTATION, {
        variables: {
            key: product?.id
        },
        refetchQueries: [{
            query: PRODUCT_LIST_QUERY, variables: {
                subCategory: selectedSubCategory,
                brands: selectedBrand
            }
        }]
    });

    const [deleteFile, { loading: loadingDeleteFile }] = useMutation(DELETE_FILE_MUTATION);

    const [uploadFiles] = useMutation(UPLOAD_FILES);
    const [isUploading, setIsUploading] = useState(false);
    const [mode, setMode] = useState(INSERT);
    const [errors, setErrors] = useState(null);

    // useEffect(() => {
    //     setColorSelected(products[0].key);
    // }, []);

    // useEffect(() => {
    //     //checkSizes()
    //     updateSizes();
    // }, [inputs.startSize, inputs.endSize]);

    // useEffect(() => {
    //     //checkPrices()
    //     console.log("useEffect", inputs.price);
    //     updatePrice();
    // }, [inputs.price]);

    useEffect(() => {
        if (mode === INSERT) {
            // console.log("cambiando");
            const { name, brand } = inputs;
            if (brands && !Array.isArray(brands)) return;
            const slug = createSlug(name, brands.find(b => b.id === brand).description);
            setSlug(slug);
        }
    }, [inputs.name, inputs.brand])

    useEffect(() => {
        if (product) {
            if (!product.name) return;
            loadProduct(product);
            setColorSelected(product.colors[0].id);
            setMode(UPDATE);
        }
    }, [product]);

    // when the user clicks on the available colors
    const clickColor = (id) => {
        setColor(id);
        // setColorSelected(id);
    }

    // when the user click on the colors, that are going to be available
    // the list above the images
    const clickColorSelected = (id) => {
        setColorSelected(id);
    }

    // const getFileNames = (files: FileList): string[] => {
    //     let fileNames: string[] = [];
    //     for (let i = 0; i < files.length; i++) {
    //         console.log(files[i].name);
    //         fileNames.push(files[i].name);
    //     }
    //     return fileNames;
    // }

    const onChange = (event: ChangeEvent<HTMLInputElement>): void => {
        const input = event.target as HTMLInputElement;
        setIsUploading(true);

        if (!input.files?.length) {
            return;
        }

        uploadFiles({ variables: { files: input.files } }).then(({ data }) => {
            setFiles(colorSelected, data.multiUpload);
            setIsUploading(false);
        });
    }

    const onChangePrice = (size: string, value: string): void => {
        changePrice(colorSelected, size, Number(value));
    }

    const removeSize = (size: string): void => {
        removeSizeFromList(colorSelected, size);
    }

    const removeImage = (imageKey: string): void => {
        // console.log('removeImage', imageKey);
        deleteFile({ variables: { key: imageKey } }).catch(err => alert(err.message));
        removeImageFromList(colorSelected, imageKey);
    }

    const clearForm = (): void => {
        initialValues(initialValue);
        setMode(INSERT);
        setSelectedProduct('');
        setErrors(null);
    }
    const onNewProduct = () => {
        if (!pristine) {
            if (confirm("You have made changes on the current product, are you sure you want clear the form and lost your changes?")) {
                clearForm();
            }
        }
        else {
            clearForm();
        }
    }

    const saveProduct = async () => {
        const validation: ValidationMessages = await validate();
        if (validation.error) {
            setErrors(validation.message);
            addToast("There was an error", {
                appearance: 'error',
                autoDismiss: true,
            });
            return;
        }

        setErrors(null);

        if (mode === INSERT) {
            // const res = await createProduct({
            //     variables: inputs,
            //     refetchQueries: [{ query: PRODUCT_LIST_QUERY, variables: {} }]
            // });
            const res = await createProduct();
            selectProduct(res.data.createProduct.id);
            setMode(UPDATE);
            addToast("Product was inserted", {
                appearance: 'success',
                autoDismiss: true,
            });
        }
        else {
            const res = await updateProduct();
            addToast("Product was saved", {
                appearance: 'success',
                autoDismiss: true,
            });
        }
    }

    const onDeleteProduct = () => {
        if (confirm("Delete product?")) {
            deleteProduct().catch(err => alert(err.message));
        }
    }

    const createSlug = (name: string, brand: string): string => {
        return [
            ...brand.toLocaleLowerCase().split(' '),
            ...name.toLocaleLowerCase().split(' '),
        ].flat().join('-');
    }

    const onChangeSearch = (e): void => {
        let { value } = e.target;
        setSearchId(value);
    }
    const searchProduct = () => {
        // console.log("---", searchId);
        selectProduct(searchId);
    }

    const handleDrag = (ev) => {
        setDragId(ev.currentTarget.id);
    };

    const handleDrop = (ev) => {
        if (dragId !== ev.currentTarget.id) {
            // console.log("swapping");
            swapImages(colorSelected, dragId, ev.currentTarget.id);
        }
    };

    // console.log(inputs);
    const buttonText = mode === INSERT ? 'Add Product' : 'Save Changes';

    const [drawerBrandOpen, setDrawerBrandOpen] = useState(false);
    const openDrawerBrand = () => {
        setDrawerBrandOpen(true);
    }
    const onCloseDrawerBrand = () => {
        setDrawerBrandOpen(false);
    }

    const [drawerColorOpen, setDrawerColorOpen] = useState(false);
    const openDrawerColor = () => {
        setDrawerColorOpen(true);
    }
    const onCloseDrawerColor = () => {
        setDrawerColorOpen(false);
    }

    // const [modalOpen, setModalOpen] = useState(false);
    // const [firstOpen, setFirstOpen] = useState(false);

    const modalRef = useRef(null);

    const openModal = () => {
        // setModalOpen(true);
        // setFirstOpen(true);
        modalRef.current.open();
    }

    const onSelectProducts = (relatedProducts) => {
        // console.log('createProduct', relatedProducts);
        setRelatedProducts(relatedProducts);
        modalRef.current.close();
    }

    const removeRelatedProduct = (id: string): void => {
        // console.log('removeRelatedProduct', id);
        const { relatedProductsData } = inputs;
        const index = relatedProductsData.findIndex(related => related.id === id);
        relatedProductsData.splice(index, 1);

        setRelatedProducts([...relatedProductsData]);
    }

    const handleRelatedDrag = (ev) => {
        setRelatedDragId(ev.currentTarget.id);
    };

    const handleRelatedDrop = (ev) => {
        if (relatedDragId !== ev.currentTarget.id) {
            // console.log("swapping");
            swapRelatedProducts(relatedDragId, ev.currentTarget.id);
        }
    };

    return (
        <div className="content">
            <ProductStateProvider>
                <ModalRelatedProducts
                    ref={modalRef}
                    onSelectProducts={onSelectProducts}
                    relatedProducts={inputs.relatedProductsData} />
            </ProductStateProvider>

            <Header>
                <ButtonContainer>
                    <ButtonNew onClick={onNewProduct}><i className="lni lni-add-files"></i> New Product</ButtonNew>
                    <ButtonDelete onClick={onDeleteProduct} disabled={loadingDelete}>Delete Product</ButtonDelete>
                    <SearchForm onSubmit={(e) => {
                        e.preventDefault();
                        searchProduct();
                    }}>
                        <input
                            type="text"
                            name="idtosearch"
                            placeholder="Id or slug to search"
                            value={searchId}
                            onChange={onChangeSearch}></input>
                        <button type="submit">Search</button>
                    </SearchForm>
                    &nbsp;<ButtonNew onClick={openDrawerBrand}>Add Brand</ButtonNew>
                    &nbsp;<ButtonNew onClick={openDrawerColor}>Add Color</ButtonNew>
                </ButtonContainer>
            </Header>

            <CreateBrand open={drawerBrandOpen} onClose={onCloseDrawerBrand} />
            <CreateColor open={drawerColorOpen} onClose={onCloseDrawerColor} />

            <MainWraper>
                <SideBar>
                    <ProductList />
                </SideBar>
                <Content>
                    {errors && <ErrorMessageTop>
                        {errors}
                    </ErrorMessageTop>}

                    <Form onSubmit={(e) => {
                        e.preventDefault();
                        saveProduct();
                    }}>
                        <fieldset disabled={loading} aria-busy={loading}>
                            <label htmlFor="name">
                                Name
                                <input
                                    type="text"
                                    value={inputs.name}
                                    id="name" name="name"
                                    placeholder="Name"
                                    onChange={handleChange} />
                            </label>
                            <label htmlFor="id">
                                External Id
                                <input
                                    type="text"
                                    value={inputs.externalId}
                                    id="id" name="externalId"
                                    placeholder="External Id"
                                    onChange={handleChange} />
                            </label>
                            <label htmlFor="description">
                                {/* Description
                                <textarea
                                    value={inputs.description}
                                    id="description"
                                    name="description"
                                    placeholder="Description"
                                    onChange={handleChange} /> */}
                                Description
                                <MarkdownEditor
                                    value={inputs.description}
                                    id="description"
                                    name="description"
                                    onTextChange={handleChange}
                                />

                            </label>
                            <label htmlFor="subCategory">
                                Category
                                <select
                                    id="subCategory"
                                    name="subCategory"
                                    value={inputs.subCategory}
                                    onChange={handleChange}>
                                    {
                                        Array.isArray(categories) && categories.map(category => {
                                            return (
                                                <optgroup label={category.description} key={category.id}>
                                                    {
                                                        category.subcategories.map(subCategory => <option key={subCategory.id} value={subCategory.id}>{subCategory.description}</option>)
                                                    }
                                                </optgroup>
                                            )
                                        })
                                    }
                                </select>
                            </label>

                            <label htmlFor="brand">
                                Brand
                                <select
                                    id="brand"
                                    name="brand"
                                    value={inputs.brand}
                                    onChange={handleChange}
                                >
                                    {
                                        Array.isArray(brands) && brands.map(brand => <option key={brand.id} value={brand.id}>{brand.description}</option>)
                                    }
                                </select>
                            </label>
                            <label htmlFor="name">
                                Slug
                                <input
                                    type="text"
                                    value={inputs.slug}
                                    id="slug" name="slug"
                                    placeholder="Slug"
                                    onChange={handleChange} />
                                {inputs.slugExists && <ErrorMessage>The slug it already exists, please choose another.</ErrorMessage>}
                            </label>

                            <label htmlFor="colors">
                                Colors
                                <ColorOptions>
                                    {
                                        Array.isArray(colors) && colors.map(color => {
                                            return (
                                                <ColorOption
                                                    key={color.id}
                                                    className={inputs.colors.includes(color.id) ? 'selected' : ''}
                                                    onClick={() => clickColor(color.id)}>

                                                    {color.name}
                                                </ColorOption>
                                            )
                                        })
                                    }
                                </ColorOptions>
                            </label>

                            <label htmlFor="startSize">
                                Sizes
                                <ul style={{ listStyleType: 'none' }}>
                                    <li style={{ display: 'inline-block' }}>
                                        <input
                                            type="number"
                                            value={inputs.startSize}
                                            id="startSize"
                                            name="startSize"
                                            placeholder="startSize"
                                            onChange={handleChange} />
                                    </li>
                                    <li style={{ display: 'inline-block' }}>
                                        <input
                                            type="number"
                                            value={inputs.endSize}
                                            id="endSize"
                                            name="endSize"
                                            placeholder="endSize"
                                            onChange={handleChange} />
                                    </li>
                                </ul>
                            </label>

                            <label htmlFor="price">
                                Price
                                <input
                                    type="text"
                                    value={inputs.price}
                                    id="price"
                                    name="price"
                                    placeholder="Price"
                                    onChange={handleChange} />
                            </label>

                            <label htmlFor="details">
                                {/* Details
                                <textarea
                                    value={inputs.details}
                                    id="details"
                                    name="details"
                                    placeholder="Details"
                                    onChange={handleChange} /> */}
                                Details
                                <MarkdownEditor
                                    value={inputs.details}
                                    id="details"
                                    name="details"
                                    onTextChange={handleChange}
                                />
                            </label>
                            <label htmlFor="sizeAndFit">
                                {/* Size And Fit
                                <textarea
                                    value={inputs.sizeAndFit}
                                    id="sizeAndFit" name="sizeAndFit"
                                    placeholder="Size And Fit"
                                    onChange={handleChange} /> */}

                                Size And Fit
                                <MarkdownEditor
                                    value={inputs.sizeAndFit}
                                    id="sizeAndFit"
                                    name="sizeAndFit"
                                    onTextChange={handleChange}
                                />
                            </label>

                            <label>
                                <RelatedProductsHeader>
                                    <span>Related Products</span>
                                    <a onClick={() => openModal()}>Edit</a>
                                </RelatedProductsHeader>
                                <RelatedProducts>
                                    {
                                        inputs.relatedProductsData.map(related => {
                                            return (
                                                <RelProduct
                                                    key={"related" + related.id}
                                                    id={related.id}
                                                    draggable="true"
                                                    onDragOver={(ev) => ev.preventDefault()}
                                                    onDragStart={handleRelatedDrag}
                                                    onDrop={handleRelatedDrop}
                                                >
                                                    <Image cloudName={cloudName} publicId={related.public_id} width="80" crop="scale" />
                                                    <ProductData>
                                                        <Brand> {related.brand} </Brand>
                                                        <Name> {related.name} </Name>
                                                    </ProductData>
                                                    <RemoveMark>
                                                        <a onClick={() => removeRelatedProduct(related.id)}>
                                                            <i className="lni lni-close"></i>
                                                        </a>
                                                    </RemoveMark>
                                                </RelProduct>
                                            )
                                        })
                                    }
                                </RelatedProducts>
                            </label>
                            <button type="submit">{buttonText}</button>
                        </fieldset>
                    </Form>
                </Content>

                <ImageContainer>

                    <label htmlFor="startSize">

                        <ul style={{ listStyleType: 'none' }} >
                            <li style={{ display: 'inline-block' }}>
                                <ImageSelector
                                    title="Thumbnail"
                                    files={inputs.files}
                                    image={inputs.thumbnailImg}
                                    setImage={setThumbnail}
                                />
                            </li>
                            <li style={{ display: 'inline-block' }}>
                                <ImageSelector
                                    title="Main Image"
                                    files={inputs.files}
                                    image={inputs.mainImgImg}
                                    setImage={setMainImg}
                                />
                            </li>
                        </ul>
                    </label>

                    <AvailableColorOptions>
                        {
                            inputs.colors.map((color) =>
                                <AvailableColor
                                    key={color}
                                    onClick={() => clickColorSelected(color)}
                                    className={color === colorSelected ? 'selected' : ''}
                                >
                                    {colors.find(colorData => colorData.id === color).name}
                                </AvailableColor>
                            )
                        }
                    </AvailableColorOptions>
                    <Form >
                        <fieldset>
                            {!isUploading && inputs.colors.length &&
                                <FileUpload
                                    label={"Images for color "}
                                    onChange={onChange}
                                    multiple={true} />}
                            {isUploading &&
                                <SpinnerContainer>
                                    <Spinner />
                                </SpinnerContainer>
                            }
                        </fieldset>
                        <ImageList>
                            {
                                inputs.imagesStack.find((image) => image.color === colorSelected)?.images.map((imageId) => {
                                    // console.log(inputs.files, imageId);
                                    const file = inputs.files.find(file => {
                                        return file.id === imageId
                                    });
                                    if (!file) return <span></span>
                                    return (
                                        <li
                                            key={file.id}
                                            style={{ position: 'relative' }}
                                            id={file.id}
                                            onDragOver={(ev) => ev.preventDefault()}
                                            onDragStart={handleDrag}
                                            onDrop={handleDrop}
                                        >
                                            {/* <img src={'http://127.0.0.1:8080/' + file.filename} /> */}
                                            <Image cloudName={cloudName} publicId={file.public_id} width="500" crop="scale" />
                                            <RemoveImage>
                                                <a onClick={() => removeImage(file.id)}><i className="lni lni-cross-circle"></i></a>
                                            </RemoveImage>
                                        </li>
                                    )
                                })
                            }
                        </ImageList>

                        <SizeGrid>
                            {
                                inputs.prices.find((image) => image.color === colorSelected)?.prices.map((price) => {
                                    return (
                                        <Fragment key={"option-" + price.size}>
                                            <SizeCont>{price.size}</SizeCont>
                                            <PriceCont>
                                                <input type="text" value={price.price} onChange={(e) => onChangePrice(price.size, e.target.value)} />
                                            </PriceCont>
                                            <RemoveCont>
                                                <a onClick={() => removeSize(price.size)}><i className="lni lni-cross-circle"></i></a>
                                            </RemoveCont>
                                        </Fragment>
                                    )
                                })
                            }
                        </SizeGrid>
                        <pre className="language-bash">{JSON.stringify(inputs, null, 2)}</pre>
                    </Form>
                </ImageContainer>
            </MainWraper>
        </div>
    )
}

export default CreateProduct;

const ImageList = styled.ul`
    list-style-type: none;

    li {
        display: inline-block
    }
    img {
        width: 150px; 
        height:auto;
    }
`

const MainWraper = styled.div`
    display:block;

    @media only screen and (min-width: 800px)   {
        height: 95vh;
        display: grid;
        grid-gap: 0px;
        grid-template-columns: 400px auto auto;
        grid-template-areas:
        "sidebar content images"
    }
`
const SideBar = styled.aside`
    display:none;
    @media only screen and (min-width: 800px)   {
        display: block;
        grid-area: sidebar;
        height: 95vh;
        overflow: auto;
    }
`;

const Content = styled.main`
    grid-area: content;
    height: 95vh;
    overflow: auto;
`
const ImageContainer = styled.div`
    grid-area: images;
    height: 95vh;
    overflow: auto;
`

const ColorOptions = styled.div`
    // margin-top: 0.5rem;
    padding: 1.0rem 1.0rem 0 1.0rem;
    display: grid;
    grid-gap: 0px;
    grid-template-columns: repeat(auto-fill, minmax(70px, 1fr));
    max-width: none;
`

const ColorOption = styled.div`
    width: auto;
    height: 40px;
    background-color: #efefef;
    color: #494949;
    font-weight: normal;
    line-height: 40px;
    font-family: Helvetica, sans-serif;
    font-size: 0.8rem;
    text-align: center;
    margin-bottom: 0.15rem;
    margin-right: 0.1rem;
    padding: 0 0.2rem;
    white-space: nowrap;
    cursor: pointer;    

    &.selected { 
        background-color:#EF3B68;
        color: #fff;
    }
`

// const ColorSelected = styled(ColorOption)`
//     border: 1px solid #efefef;
//     background-color: #fff;
//     color: #494949;

//     &.selected { 
//         background-color: #efefef;
//         color: #494949;
//         font-weight: bolder;
//     }
// `
// grid-template-columns: 100px 100px 50px;
// grid-template-areas:
// "size price remove"

const SizeGrid = styled.div`
    display: grid;
    grid-gap: 2px;
    grid-template-columns: 50px 100px 50px;
`

const SizeCont = styled.div`
    padding-top: 0.25rem;
`
const PriceCont = styled.div`
    justify-self: center;
`
const RemoveCont = styled.div`
    padding-top: 0.25rem;
    justify-self: center;
`
const RemoveImage = styled.div`
    position: absolute;
    top: 0;
    right: 0;

    a {
        text-decoration: none;
        color: var(--main-color);
        font-size: 2.0rem;
        cursor: pointer;
    }
`
const ButtonContainer = styled.div`
    padding: 0.5rem 1.0rem 0.5rem 1.0rem;
`
const ButtonNew = styled.button`
    background-color: #eaeaea;
    color: #494949
    border: none;
    padding: 5px;
    font-family: Helvetica;
    font-weight: bold;
    margin-right: 5px;
`
const ButtonDelete = styled(ButtonNew)`
`;

const AvailableColorOptions = styled.div`
    padding: 1.0rem 1.0rem 0 1.0rem;
    display: grid;
    grid-gap: 0px;
    grid-template-columns: repeat(auto-fill, minmax(70px, 1fr));
`

const AvailableColor = styled.div`
    border: 1px solid #eaeaea;
    color: #494949
    font-weight: normal;
    font-size: 0.8rem;
    padding: 5px;
    text-align: center;
    
    font-family: Helvetica;
    margin-right: 2px;

    &.selected { 
        background-color: #efefef;
        color: #494949;
        font-weight: bold;
    }
`

const SearchForm = styled.form`
    display: inline-block;
    margin: 0;
    margin-left: 2.0rem;

    input {
        border: 1px solid #eaeaea;
        padding: 0.25rem;
        outline: none;
        height: 26px;
    }

    button,
    input[type='submit'] {
        height: 26px;
        width: auto;
        background: #eaeaea;
        color: #494949;
        border: 0;
        font-size: 0.8rem;
        font-weight: 400;
        padding: 0 1.0rem;
    }    
`

const RelatedProductsHeader = styled.div`
    display: grid;
    grid-template-columns: auto auto;

    a {
        justify-self: end;
        cursor: pointer;
        color: var(--main-color)
    }
`
const RelatedProducts = styled.div`
`

const RelProduct = styled.div`
    display: grid;
    grid-gap: 0px;
    grid-template-columns: 60px auto 60px;
    border-top: 1px solid #eaeaea;
    border-left: 1px solid #eaeaea;
    border-right: 1px solid #eaeaea;

    cursor: move;

    &:last-child {
        border-bottom: 1px solid #eaeaea;
    }

    img {
        width: 100%;
    }
`

const Brand = styled.p`
    padding: 1px 3px 1px 3px;
    color: #494949;
    font-weight: bold;
    font-size: 0.8rem;
    text-align: left;
`
const Name = styled.p`
    font-size: 0.8rem;
    padding: 1px 3px 1px 3px;
    color: #494949;
    text-align: left;
`

const ProductData = styled.div`
    margin-top: 1.0rem;
`

const RemoveMark = styled.div`
    font-size: 1.25rem;
    font-weight: bolder;
    padding-top: 1.5rem;
    text-align: center;
    background-color: #fafafa;

    a {
        display: block;
        text-decoration: none;
        cursor: pointer;
    }
`
const ErrorMessage = styled.div`
    font-size: 0.9rem;
    color: red;
`

const ErrorMessageTop = styled.div`
    font-family: Helvetica;
    font-size: 1.0rem;
    color: #EC0101;
    border: 1px solid #CD0A0A;
    
    padding: 0.5rem;
    margin: 1.25rem;
`

const rotateAnimation = keyframes`
    0% {
        transform: rotate(0deg);
    }

    100% {
        transform: rotate(360deg);
    }
`
const antiRotateAnimation = keyframes`
    0% {
        transform: rotate(0deg);
    }

    100% {
        transform: rotate(-360deg);
    }
`

// width: var(--main-spinner-dimensions);
// height: var(--main-spinner-dimensions);
// https://codepen.io/foxeisen/pen/YZxGed

export const SpinnerContainer = styled.section`
  position: relative;
  margin: 25px 0 15px;
  border: 2px dotted lightgray;
  padding: 12px;
  border-radius: 6px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: white;
`;

const Spinner = styled.div`
    position: relative;
    width: 100px;
    height: 100px;


    &:before {
        content: "";
        display: block;
        position: absolute;
        border-width: 4px;
        border-style: solid;
        border-radius: 50%;
  
        width: 100px;
        height: 100px;
        border-bottom-color: #494949;
        border-right-color: #494949;
        border-top-color: white;
        border-left-color: white;
        top: 0px;
        left: 0px;
        animation: ${rotateAnimation} 1s linear 0s infinite;
    }
    
    &:after {
        content: "";
        display: block;
        position: absolute;
        border-width: 4px;
        border-style: solid;
        border-radius: 50%;
        
        width: 75px;
        height: 75px;
        border-bottom-color: #494949;
        border-right-color: #494949;
        border-top-color: white;
        border-left-color: white;
        top: calc((100px - 75px) / 2);
        left: calc((100px - 75px) / 2);
        animation: ${antiRotateAnimation} 0.75s linear 0s infinite;
    }    
`

// /* spinner-1 styles */
// .spinner.spinner-1 {

//     @keyframes rotate-animation {
//     0% {
//       transform: rotate(0deg);
//     }

//     100% {
//       transform: rotate(360deg);
//     }
//   }

//   @keyframes anti-rotate-animation {
//     0% {
//       transform: rotate(0deg);
//     }

//     100% {
//       transform: rotate(-360deg);
//     }
//   }

// }


// const unfoldIn = keyframes`
//     0% {
//         transform:scaleY(.005) scaleX(0);
//     }
//     50% {
//         transform:scaleY(.005) scaleX(1);
//     }
//     100% {
//         transform:scaleY(1) scaleX(1);
//     }
// `;

// const unfoldOut = keyframes`
//     0% {
//         transform:scaleY(1) scaleX(1);
//     }
//     50% {
//         transform:scaleY(.005) scaleX(1);
//     }
//     100% {
//         transform:scaleY(.005) scaleX(0);
//     }
// `

// const zoomIn = keyframes`
//     0% {
//         transform:scale(0);
//     }
//     100% {
//         transform:scale(1);
//     }
// `

// const zoomOut = keyframes`
//     0% {
//         transform:scale(1);
//     }
//     100% {
//         transform:scale(0);
//     }
// `

// const fadeIn = keyframes`
//     0% {
//         background:rgba(0,0,0,.0);
//     }
//     100% {
//         background:rgba(0,0,0,.7);
//     }
// `
// const fadeOut = keyframes`
//     0% {
//         background:rgba(0,0,0,.7);
//     }
//     100% {
//         background:rgba(0,0,0,.0);
//     }
// `
// const scaleUp = keyframes`
//     0% {
//         transform:scale(.8) translateY(1000px);
//         opacity:0;
//     }
//     100% {
//         transform:scale(1) translateY(0px);
//         opacity:1;
//     }
// `
// const scaleBack = keyframes`
//     0% {
//         transform:scale(1);
//     }
//     100% {
//         transform:scale(.85);
//     }
// `
// const quickScaleDown = keyframes`
//     0% {
//         transform:scale(1);
//     }
//     99.9% {
//         transform:scale(1);
//     }
//     100% {
//         transform:scale(0);
//     }
// `
// const scaleDown = keyframes`
//     0% {
//         transform:scale(1) translateY(0px);
//         opacity:1;
//     }
//     100% {
//         transform:scale(.8) translateY(1000px);
//         opacity:0;
//     }
// `
// const scaleForward = keyframes`
//     0% {
//         transform:scale(.85);
//     }
//     100% {
//         transform:scale(1);
//     }
// `

// const ModalContainer = styled.div`
//     position: fixed;
//     display: table;
//     height: 100%;
//     width: 100%;
//     top: 0;
//     left: 0;
//     transform: scale(0);
//     z-index: 1;

//     &.one {
//         transform: scaleY(.01) scaleX(0);
//         animation: ${unfoldIn} 1s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
//         .modal-background {
//             .modal {
//                 transform: scale(0);
//                 animation: ${zoomIn} .5s .8s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
//             }
//         }
//         &.out {
//             transform: scale(1);
//             animation: ${unfoldOut} 1s .3s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
//             .modal-background {
//                 .modal {
//                     animation: ${zoomOut} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
//                 }
//             }
//         }
//     }

//     &.two {
//         transform:scale(1);
//         .modal-background {
//             background:rgba(0,0,0,.0);
//             animation: ${fadeIn} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
//             .modal {
//                 opacity:0;
//                 animation: ${scaleUp} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
//             }
//         }
//         + .content {
//           animation: ${scaleBack} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
//         }
//         &.out {
//             animation: ${quickScaleDown} 0s .5s linear forwards;
//             .modal-background {
//                 animation: ${fadeOut} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
//                 .modal {
//                     animation: ${scaleDown} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
//                 }
//             }
//             + .content {
//                 animation: ${scaleForward} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
//             }
//         }
//     }


//     .modal-background {
//         display: table-cell;
//         background: rgba(0,0,0,.8);
//         text-align: center;
//         vertical-align: middle;

//         .modal {
//             background: white;
//             padding: 50px;
//             display: inline-block;
//             border-radius: 3px;
//             font-weight: 300;
//             position: relative;
//             h2 {
//                 font-size: 25px;
//                 line-height: 25px;
//                 margin-bottom: 15px;
//             }
//             p {
//                 font-size: 18px;
//                 line-height: 22px;
//             }
//             .modal-svg {
//                 position: absolute;
//                 top: 0;
//                 left: 0;
//                 height: 100%;
//                 width: 100%;
//                 border-radius: 3px;
//                 rect {
//                     stroke: #fff;
//                     stroke-width: 2px;
//                     stroke-dasharray: 778;
//                     stroke-dashoffset: 778;
//                 }
//             }
//         }
//     }    
// `
