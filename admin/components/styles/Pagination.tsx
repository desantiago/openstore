import styled from 'styled-components';

// .pagination
export const Pagination = styled.div`
  font-family: Helvetica, Arial, sans-serif;
  text-align: center;
  margin-bottom: 2.0rem;
  width: 100vw;

  @media only screen and (min-width: 800px) {
    width: inherit;
  }
`

//.pagination ul
export const PaginationList = styled.ul`
  list-style: none;
`
//.pagination li
export const PaginationPage = styled.li`
  display: inline-block;
  width: 40px;
  height: 40px;
  line-height: 40px;
  padding: auto;

  &.selected {
    font-weight: bold;
    background-color: #eaeaea;
  }

  a {
    display: block;
    color: inherit;
    text-decoration: none;
  }
 
`
