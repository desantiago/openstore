// import React, { useContext } from "react";
import styled from "styled-components";
import { useMarkdownContext } from "./context";

export default function MarkedInput({ id, name, onTextChange }) {
    //const { setMarkdownText } = useContext(editorContext);
    const { markdownText, setMarkdownText } = useMarkdownContext();

    const onInputChange = e => {
        const newValue = e.currentTarget.value;
        setMarkdownText(newValue);
        onTextChange(e);
    };

    return (
        <Container>
            <TextArea id={id} name={name} value={markdownText} onChange={onInputChange} />
        </Container>
    );
}

const Container = styled.div`
  width: 100%;
  height: 100%;
  font-family: "Lato", sans-serif;
`;

// const Title = styled.div`
//   font-size: 22px;
//   font-weight: 600;
//   margin-bottom: 1em;
//   padding: 8px 0;
//   border-bottom: 1px solid rgba(15, 15, 15, 0.3);
// `;

const TextArea = styled.textarea`
  width: 100%;
  height: 150px;
  border: none;
  outline: none;
  font-size: 17px;
`;
