// import { useState } from 'react';
import styled from 'styled-components';
import { MarkdownStateProvider } from './context';
import MarkedInput from './markedInput'
import Result from './result';

// https://codesandbox.io/s/markdown-editor-7c199?file=/src/components/result.jsx

export default function Index({ value, id, name, onTextChange }) {
    // const [markdownText, setMarkdownText] = useState('');
    console.log("----------", value);
    return (
        <MarkdownStateProvider value={value}>
            <ComponentContainer>
                <EditorContainer>
                    <MarkedInput
                        id={id}
                        name={name}
                        onTextChange={onTextChange}
                    />
                    <Result />
                </EditorContainer>
            </ComponentContainer>
        </MarkdownStateProvider>
    );
}

const ComponentContainer = styled.div`
  width: 100%;
  height: 100%;
`;

// const Title = styled.div`
//   font-size: 25px;
//   font-weight: 700;
//   font-family: "Lato", sans-serif;
//   margin-bottom: 1em;
// `;

const EditorContainer = styled.div`
  width: 100%;
  height: 100%;
`;