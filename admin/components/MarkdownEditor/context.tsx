import React, { useState, useContext, useEffect } from 'react';

export type State = {
    markdownText?: string,
    setMarkdownText?: (string) => void
}

const LocalStateContext = React.createContext<State>({});
const LocalStateProvider = LocalStateContext.Provider;

function MarkdownStateProvider({ children, value }) {
    //console.log('value on context', value);
    const [markdownText, setMarkdownText] = useState('');

    const values: State = {
        markdownText,
        setMarkdownText
    }

    useEffect(() => {
        setMarkdownText(value)
    }, [value])

    return (
        <LocalStateProvider value={values}>{children}</LocalStateProvider>
    );
}

function useMarkdownContext(): State {
    const all = useContext(LocalStateContext);
    return all;
}

export { MarkdownStateProvider, useMarkdownContext }
//export  React.createContext(defaultContext);
