import { useEffect } from 'react';
import styled from 'styled-components';
import moment from 'moment'

import { useOrders } from '../lib/ordersContext';
import formatMoney from '../lib/formatMoney';
import { ProvidedRequiredArgumentsOnDirectivesRule } from 'graphql/validation/rules/ProvidedRequiredArgumentsRule';

const OrderHeader = styled.div`
    display: grid;
    grid-template-columns: 60px 200px 80px 120px auto;
    grid-auto-rows: 1fr;
    font-family: 'Mada', sans-serif;
    font-weight: bold;
    font-size: 0.9rem;
    border-top: 1px solid #eaeaea;
    border-bottom: 1px solid #eaeaea;

    margin-top: 1.0rem;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
`

const Order = styled.div`
    display: grid;
    grid-template-columns: 60px 200px 80px 120px auto;
    grid-auto-rows: 1fr;
    font-family: 'Mada', sans-serif;
    font-size: 1.0rem;
    align-items: center;
    border-bottom: 1px solid #eaeaea;
    cursor:pointer;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    &:nth-child(even) {
        background-color: #f9f9f9;
    }
    &.selected {
        background-color:#d0e8f2;
    }
`
const OrderId = styled.p`
    padding: 1px 3px 1px 3px;
    color: #494949;
    font-weight: bold;
    font-size: 0.8rem;
`
const CustomerName = styled.p`
`
const Date = styled.p`
    
`
const Total = styled.p`
    text-align: right;
    padding-right: 1.0rem;
`
const Items = styled.p`
`

// const Details = styled.p`
// padding: 1px 3px 1px 3px;
// color: #494949;
// `

// const Img = styled.img`
// width: 50px;
// heigth: auto;
// `
// const ProductData = styled.div`
// `

export default function OrderList() {

    const { orders, setOrder, orderSelected } = useOrders();

    const onSelectOrder = (key) => {
        setOrder(key);
    }

    useEffect(() => {
        setOrder(Array.isArray(orders) && orders[0].key);
    }, [orders])

    return (
        <div style={{ marginLeft: "1.0rem" }}>
            <OrderHeader>
                <p>OrderId</p>
                <p>Name</p>
                <p>Date</p>
                <Total>Total</Total>
                <Items />
            </OrderHeader>
            {
                Array.isArray(orders) && orders.map(order => {
                    return (
                        <Order
                            key={order.key}
                            onClick={() => { onSelectOrder(order.key) }}
                            className={order.key === orderSelected ? 'selected' : null}
                        >
                            <OrderId>{order.id}</OrderId>
                            <CustomerName>{order.user.name}</CustomerName>
                            <Date>{moment(order.date, 'x').format('MM/DD/YYYY')}</Date>
                            <Total>{formatMoney(order.total)}</Total>
                            <Items>
                                {order.orderItems.length} Products
                            </Items>
                        </Order>
                    )
                })
            }
        </div>
    )
}