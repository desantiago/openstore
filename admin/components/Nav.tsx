import Link from 'next/link'
import styled, { css } from 'styled-components';

{/* <ul>
<li id="option-designers"><Link href="/designers">Designers</Link></li>
<li id="option-shoes"><Link href="/shoes">Shoes</Link></li>
<li id="option-bags"><Link href="/bags">Handbags</Link></li>
</ul> */}

//nav-wrapper
const NavWrapper = styled.div`
display: none;

@media only screen and (min-width: 800px)   {
    border-top: 1px solid #eaeaea;
    border-bottom: 1px solid #eaeaea;
    display: flex;
    flex-direction: row;
}
`
//nav-column-left 
const NavColumnLeft = styled.div`
width: 25%;
`
//nav-column-center
const NavColumnCenter = styled.div`
text-align: center;
width: 50%;
position: relative;
`
//nav-column-right
const NavColumnRight = styled.div`
width: 25%;
`

//common menu-style
const commonMenuStyle = css`
position: absolute;

left: 0;
margin-top: 2px;
min-width: 5rem;
text-align: left;
transition: all 0.5s ease;
width:100%;

display: none;
opacity: 0;
visibility: hidden;

&:hover {
    visibility: visible;
    opacity: 1;
    display: block;
}
`

//menu-designers
const MenuDesigners = styled.div`
${commonMenuStyle}
`

//menu-designers-container
const MenuContainer = styled.div`
display: flex;
flex-direction: row;
background-color: white;

h3 {
    font-size: 0.8rem;
    font-weight: bold; 
    margin-bottom: 0.5rem;
    font-family: 'M PLUS 1p', sans-serif;
    text-transform: uppercase;
}
p {
    margin-bottom: 0.25rem;
}
a {
    text-decoration: none;
    color: inherit;
}
img { 
    width: 100%;
    height: auto;
}
`
//menu-designers-column-left-1
const MenuColumnLeftSpace = styled.div`
width: 20%;
`
//menu-designers-column-left
const MenuColumnLeft = styled.div`
width: 30%;
margin-top: 1.0rem;
font-size: 0.9rem;
`
//menu-designers-column-right
const MenuColumnRight = styled.div`
width: 30%;
`

//nav-wrapper ul
const TopMenuUL = styled.ul`
font-family: 'Inter', sans-serif;
list-style: none;
margin-top: 0.25rem;
margin-bottom: 0.25rem;
`

const commonMenuOptionStyle = css`
display: inline-block;
width: 10%;
margin-left: 2.0rem;
position: relative; 

a {
    text-decoration: none;
}
`

//nav-wrapper li
const MenuDesignersOption = styled.ul`
${commonMenuOptionStyle}
&:hover ~ ${MenuDesigners} {
    visibility: visible;
    opacity: 1;
    display: block;
}
`

//menu-shoes
const MenuShoes = styled.div`
${commonMenuStyle}
`
//nav-wrapper li
const MenuShoesOption = styled.ul`
${commonMenuOptionStyle}
&:hover ~ ${MenuShoes} {
    visibility: visible;
    opacity: 1;
    display: block;
}
`

//menu-bags
const MenuBags = styled.div`
${commonMenuStyle}
`

//nav-wrapper li
const MenuBagsOption = styled.ul`
${commonMenuOptionStyle}
&:hover ~ ${MenuBags} {
    visibility: visible;
    opacity: 1;
    display: block;
}
`

export default function Nav() {
    return (
        <nav>
        </nav>
    )
}