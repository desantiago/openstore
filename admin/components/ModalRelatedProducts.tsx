import { useState, useContext, useCallback, useEffect, useImperativeHandle, forwardRef } from 'react';
import styled from 'styled-components';
import { Image } from 'cloudinary-react';

import Modal from './Modal';
import SelectBrand from './SelectBrand';
import SelectSubCategory from './SelectSubCategory';
// import ProductContext from '../lib/productContext';
import { cloudName } from '../config';
import { useProducts } from '../lib/relatedProductsContext';

export function ModalRelatedProducts({ onSelectProducts, relatedProducts }, ref) {
    const {
        products,
        categories,
        brands,
        setSelectedBrand,
        setSelectedSubCategory
    } = useProducts();

    const [selections, setSelections] = useState([]);
    const [selectionsProd, setSelectionsProd] = useState([]);
    const [subCategory, setSubCategory] = useState('');
    const [brand, setBrand] = useState('all');

    useEffect(() => {
        setSelectionsProd(relatedProducts);
        setSelections(relatedProducts.map(related => related.id));
    }, [relatedProducts])

    const handleChangeSubCategory = (subCategory: string): void => {
        setSubCategory(subCategory);
        setSelectedSubCategory(subCategory);
    }

    const handleChangeBrand = (brand: string): void => {
        setBrand(brand);
        setSelectedBrand(brand);
    }

    const onOk = () => {
        onSelectProducts(selectionsProd);
    };

    // const selectProduct = useCallback((id: string): void => {
    const selectProduct = (id: string): void => {
        const selProds = [...selectionsProd];
        if (selections.includes(id)) {
            const index = selProds.findIndex(product => product.id === id);
            selProds.splice(index, 1);
        }
        else {
            const product = products.find(product => product.id === id);
            const { thumbnail, brand, name } = product;

            selProds.push({
                id: id,
                public_id: thumbnail.image.public_id,
                name,
                brand: brand.description
            });
        }

        setSelections(selProds.map(product => product.id));
        setSelectionsProd(selProds.map((prod, index) => { return { ...prod, order: index } }));
    }

    // console.log("selections ", selections);
    // console.log("selectionsProd ", selectionsProd);

    return (
        <Modal ref={ref} onOk={onOk}>
            <Title>Select Related Products</Title>
            <ProductSelections>
                {selectionsProd.map(pr => {
                    const { public_id, id } = pr;
                    return (
                        <div key={'selprod' + id} style={{ position: 'relative' }}>
                            <Image cloudName={cloudName} publicId={public_id} width="60" crop="scale" />
                            <RemoveImage>
                                <a onClick={() => selectProduct(id)}><i className="lni lni-cross-circle"></i></a>
                            </RemoveImage>
                        </div>
                    )
                })}
            </ProductSelections>

            <Filters>
                <SelectSubCategory subCategory={subCategory} categories={categories} onChange={handleChangeSubCategory} />
                <SelectBrand brand={brand} brands={brands} onChange={handleChangeBrand} />
            </Filters>

            <ProductContainer>
                {
                    Array.isArray(products) && products.map(pr => {
                        const { thumbnail, brand, name, id } = pr;
                        return (
                            <Product
                                key={id}
                                onClick={() => { selectProduct(id) }}
                            >
                                <Image cloudName={cloudName} publicId={thumbnail.image.public_id} width="60" crop="scale" />

                                <ProductData>
                                    <Brand> {brand.description} </Brand>
                                    <Name> {name} </Name>
                                </ProductData>
                                <CheckMark>
                                    {selections.includes(id) && <i className="lni lni-checkmark"></i>}
                                </CheckMark>
                            </Product>
                        )
                    })
                }
            </ProductContainer>
        </Modal>
    )
}

export default forwardRef(ModalRelatedProducts);

const Title = styled.h2`
    font-family: 'Mada', sans-serif;
    font-size: 1.2rem;
    border-bottom: 1px solid var(--light-grey);
    text-align: left;
    margin-bottom: 0.5rem;
`

const ProductContainer = styled.div`
    display: grid;
    grid-gap: 0px;
    grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
    max-height: 500px;
    overflow: auto;
    width: 700px;
`

const Filters = styled.div`
    display: grid;
    grid-template-columns: auto auto;
    padding: 0.25rem;
    border: 1px solid #eaeaea;
    margin-bottom: 0.25rem;
`

const Product = styled.div`
    display: grid;
    grid-template-columns: 60px auto 60px;
    grid-auto-rows: 1fr;
    font-family: 'Mada', sans-serif;
    font-size: 1.0rem;
    border-bottom: 1px solid #eaeaea;
    cursor:pointer;

    &.selected {
        background-color:#eaeaea;
    }

`
const Brand = styled.p`
    padding: 1px 3px 1px 3px;
    color: #494949;
    font-weight: bold;
    font-size: 0.8rem;
    text-align: left;
`
const Name = styled.p`
    font-size: 0.8rem;
    padding: 1px 3px 1px 3px;
    color: #494949;
    text-align: left;
`

const ProductData = styled.div`
    margin-top: 1.0rem;
`
// color: var(--main-color);
const CheckMark = styled.div`
    font-size: 1.25rem;
    font-weight: bolder;
    padding-top: 1.5rem;    
`

const ProductSelections = styled.div`
    display: grid;
    grid-gap: 0px;
    grid-template-columns: repeat(auto-fill, minmax(70px, 1fr));
`

const RemoveImage = styled.div`
    position: absolute;
    top: 0;
    right: 0;

    a {
        text-decoration: none;
        color: var(--main-color);
        font-size: 1.0rem;
    }
`
