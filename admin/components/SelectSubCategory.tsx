export default function SelectSubCategory({ categories, subCategory, onChange }) {

    const handleChange = (e) => {
        let { value } = e.target;
        // setSubCategory(value);
        // setSelectedSubCategory([value]);
        onChange(value)
    }

    return (
        <select
            id="subCategory"
            name="subCategory"
            value={subCategory}
            onChange={handleChange}>
            {
                Array.isArray(categories) && categories.map(category => {
                    return (
                        <optgroup label={category.description} key={category.slug}>
                            {
                                category.subcategories.map(subCategory => <option key={subCategory.id} value={subCategory.slug}>{subCategory.description}</option>)
                            }
                        </optgroup>
                    )
                })
            }
        </select>
    )
}