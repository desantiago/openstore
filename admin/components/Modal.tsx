import styled, { keyframes } from 'styled-components';
import { useState, useCallback, useEffect, useImperativeHandle, forwardRef } from 'react';
import Button from './styles/Button';

export function Modal({ children, onOk }, ref) {
    const [modalOpen, setModalOpen] = useState(false);
    const [firstOpen, setFirstOpen] = useState(false);

    const close = useCallback(() => setModalOpen(false), []);
    const onClickOk = () => { onOk() };

    useImperativeHandle(ref, () => ({
        open: () => { setModalOpen(true); setFirstOpen(true) },
        close,
    }), [close]);

    const handleEscape = useCallback(event => {
        if (event.keyCode === 27) close()
    }, [close]);

    useEffect(() => {
        if (modalOpen) document.addEventListener('keydown', handleEscape, false);

        return () => {
            document.removeEventListener('keydown', handleEscape, false)
        }
    }, [handleEscape, modalOpen]);

    // const closeModal = () => {
    //     setModalOpen(false);
    // }

    return (
        <ModalContainer className={firstOpen ? modalOpen ? 'two' : 'two out' : null}>
            <div className="modal-background">
                <div className="modal">
                    {children}
                    {/* <svg className="modal-svg" xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none">
                        <rect x="0" y="0" fill="none" width="226" height="162" rx="3" ry="3"></rect>
                    </svg> */}
                    <footer>
                        <Button secondary onClick={close}>
                            Cancel
                        </Button>
                        <Button primary onClick={onClickOk}>
                            Ok
                        </Button>
                    </footer>
                </div>
            </div>
        </ModalContainer>
    )
}

export default forwardRef(Modal);

const unfoldIn = keyframes`
    0% {
        transform:scaleY(.005) scaleX(0);
    }
    50% {
        transform:scaleY(.005) scaleX(1);
    }
    100% {
        transform:scaleY(1) scaleX(1);
    }
`;

const unfoldOut = keyframes`
    0% {
        transform:scaleY(1) scaleX(1);
    }
    50% {
        transform:scaleY(.005) scaleX(1);
    }
    100% {
        transform:scaleY(.005) scaleX(0);
    }
`

const zoomIn = keyframes`
    0% {
        transform:scale(0);
    }
    100% {
        transform:scale(1);
    }
`

const zoomOut = keyframes`
    0% {
        transform:scale(1);
    }
    100% {
        transform:scale(0);
    }
`

const fadeIn = keyframes`
    0% {
        background:rgba(0,0,0,.0);
    }
    100% {
        background:rgba(0,0,0,.7);
    }
`
const fadeOut = keyframes`
    0% {
        background:rgba(0,0,0,.7);
    }
    100% {
        background:rgba(0,0,0,.0);
    }
`
const scaleUp = keyframes`
    0% {
        transform:scale(.8) translateY(1000px);
        opacity:0;
    }
    100% {
        transform:scale(1) translateY(0px);
        opacity:1;
    }
`
const scaleBack = keyframes`
    0% {
        transform:scale(1);
    }
    100% {
        transform:scale(.85);
    }
`
const quickScaleDown = keyframes`
    0% {
        transform:scale(1);
    }
    99.9% {
        transform:scale(1);
    }
    100% {
        transform:scale(0);
    }
`
const scaleDown = keyframes`
    0% {
        transform:scale(1) translateY(0px);
        opacity:1;
    }
    100% {
        transform:scale(.8) translateY(1000px);
        opacity:0;
    }
`
const scaleForward = keyframes`
    0% {
        transform:scale(.85);
    }
    100% {
        transform:scale(1);
    }
`


// .modal-svg {
//     position: absolute;
//     top: 0;
//     left: 0;
//     height: 100%;
//     width: 100%;
//     border-radius: 3px;
//     rect {
//         stroke: #fff;
//         stroke-width: 2px;
//         stroke-dasharray: 778;
//         stroke-dashoffset: 778;
//     }
// }

const ModalContainer = styled.div`
    position: fixed;
    display: table;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    transform: scale(0);
    z-index: 1;

    &.one {
        transform: scaleY(.01) scaleX(0);
        animation: ${unfoldIn} 1s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
        .modal-background {
            .modal {
                transform: scale(0);
                animation: ${zoomIn} .5s .8s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
            }
        }
        &.out {
            transform: scale(1);
            animation: ${unfoldOut} 1s .3s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
            .modal-background {
                .modal {
                    animation: ${zoomOut} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
                }
            }
        }
    }

    &.two {
        transform:scale(1);
        .modal-background {
            background:rgba(0,0,0,.0);
            animation: ${fadeIn} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
            .modal {
                opacity:0;
                animation: ${scaleUp} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
            }
        }
        + .content {
          animation: ${scaleBack} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
        }
        &.out {
            animation: ${quickScaleDown} 0s .5s linear forwards;
            .modal-background {
                animation: ${fadeOut} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
                .modal {
                    animation: ${scaleDown} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
                }
            }
            + .content {
                animation: ${scaleForward} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;
            }
        }
    }

    .modal-background {
        display: table-cell;
        background: rgba(0,0,0,.8);
        text-align: center;
        vertical-align: middle;

        .modal {
            background: white;
            padding: 25px;
            display: inline-block;
            border-radius: 3px;
            position: relative;

            footer {
                text-align:right;
                padding-top: 0.5rem;
            }
        }
    }    
`
