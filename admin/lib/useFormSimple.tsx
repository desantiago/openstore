import { useState } from 'react';

export default function useFormSimple(initial) {
    const [inputs, setInputs] = useState(initial);

    function handleChange(e): void {
        let { value, name, type } = e.target;
        if (type === 'number') {
            value = Number(value);
        }
        if (type === 'file') {
            [value] = e.target.files;
        }

        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        });
    }

    function clearForm() {
        setInputs(initial);
    }

    return {
        inputs,
        handleChange,
        clearForm
    }
}