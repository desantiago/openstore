import { gql } from "@apollo/client";

export const PRODUCT_LIST_QUERY = gql`
    query PRODUCT_LIST_QUERY($category: [String], $subCategory: [String], $brands: [String]) {
        products(category: $category, subCategory: $subCategory, brands: $brands) {
            id
            name
            brand {
                description
            }
            thumbnail {
                image {
                    filename
                    public_id
                }
            }            
        }
    }
`
export const ALL_BRANDS = gql`
    query ALL_BRANDS {
        brands {
            description,
            id,
            slug
        }          
    }
`
export const ALL_CATEGORIES = gql`
    query ALL_CATEGORIES {
        categories {
            id
            description
            slug
            subcategories {
                id
                description
                slug
            }
        }
    }
`
export const ALL_COLORS = gql`
    query ALL_COLORS {
        colors {
            id
            name
            hex
        }
    }
`
export const PRODUCT_QUERY = gql`
    query PRODUCT_QUERY($key: ID!) {
        product(key: $key) {
            id
            name
            description
            sizeAndFit
            details
            externalId
            slug
            price
            startSize
            endSize
            category {
                id
                description
            }
            subCategory {
                id
                description
            }
            brand {
                id
                description
            }
            colors {
                id
                name
            }
            images {
                color {
                    id
                    name
                }
                images {
                    file {
                        id
                        server
                        public_id
                        filename
                    }
                    order
                }
            }
            sizes {
                color {
                    id
                    name
                }
                sizes
            }
            prices {
                color {
                    id
                    name
                }
                prices {
                    size
                    price
                }
            }
            thumbnail {
                color {
                    id
                }
                image {
                    id
                }
            }
            mainImage {
                color {
                    id
                }
                image {
                    id
                }
            }
            relatedProducts {
                product {
                    id
                    name
                    brand {
                        description
                    }
                    thumbnail {
                        image {
                            public_id
                        }
                    }
                }
                order
            }                     
        }
    }
`

export const CHECK_SLUG_QUERY = gql`
    query CHECK_SLUG_QUERY($id: String) {
        productByIds(id: $id) {
            id
        }
    }
`

// export default PRODUCT_LIST_QUERY;