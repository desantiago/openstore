import { gql, useLazyQuery, useQuery } from "@apollo/client";
import { createContext, useContext, useState } from "react";

const ORDERS_QUERY = gql`
    query ORDERS_QUERY {
        orders {
            key
            id
            total
            date
            user {
                name
            }
            orderItems {
                key
            }
        }
    }
`

const ORDER_QUERY = gql`
    query ORDER_QUERY($key: ID!) {
        order(key: $key) {
            key
            id
            total
            date
            user {
                name
            }
            orderItems {
                key
                quantity
                total
                price 
                size               
                color {
                    key
                    name
                }
                user {
                    name
                }
                product {
                    name
                    brand {
                        description
                    }
                    images {
                        color {
                            key
                        }
                        images {
                            key
                            filename
                        }
                    }
                }
            }
        }
    }
`


export type OrdersState = {
    order?: any,
    orders?: any[],
    orderSelected?: string,
    setOrderSelected?: (string) => void,
    setOrder?: (string) => void,
}

const LocalStateContext = createContext<OrdersState>({});
const LocalStateProvider = LocalStateContext.Provider;

function OrdersStateProvider({ children }) {

    const { loading, data } = useQuery(ORDERS_QUERY);
    const orders: any[] = loading || data.orders;
    console.log("Order ", orders);

    const [
        getOrder,
        { loading: loadingOrder, data: dataOrder }
    ] = useLazyQuery(ORDER_QUERY, {
        fetchPolicy: "network-only"
    });
    const order: any[] = loadingOrder || dataOrder && dataOrder.order;

    const [orderSelected, setOrderSelected] = useState(orders[0] ? orders[0].key : '');

    const setOrder = (key: string) => {
        setOrderSelected(key);
        getOrder({
            variables: {
                key
            }
        });
    }

    const values: OrdersState = {
        order,
        orders,
        orderSelected,
        setOrder,
    }

    return (
        <LocalStateProvider value={values}>{children}</LocalStateProvider>
    );
}

function useOrders(): OrdersState {
    const all = useContext(LocalStateContext);
    return all;
}

export { OrdersStateProvider, useOrders }