// import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useLazyQuery, useQuery } from '@apollo/client';
import { createContext, useContext, useState } from "react";

import {
    PRODUCT_LIST_QUERY,
    ALL_BRANDS,
    ALL_CATEGORIES,
    // ALL_COLORS,
    // PRODUCT_QUERY
} from './queryProducts';

// const ProductContext = React.createContext({});

export type ProductState = {
    products?: any[],
    brands?: any[],
    categories?: any[],
    selectedSubCategory?: string,
    selectedBrand?: string,
    setSelectedSubCategory?: (string) => void,
    setSelectedBrand?: (string) => void,
}

const LocalStateContext = createContext<ProductState>({});
const LocalStateProvider = LocalStateContext.Provider;

const ProductStateProvider = props => {
    // const [selectedProduct, setSelectedProduct] = useState('');
    const [selectedSubCategory, setSelectedSubCategory] = useState('pumps');
    const [selectedBrand, setSelectedBrand] = useState(undefined);

    // console.log('selectedSubCategory', selectedSubCategory);
    const { loading, data } = useQuery(PRODUCT_LIST_QUERY, {
        variables: {
            subCategory: selectedSubCategory,
            brands: selectedBrand
        },
        fetchPolicy: "network-only"
    });
    const products: any[] = loading || data.products;

    const { loading: loadingBrands, data: dataBrands } = useQuery(ALL_BRANDS);
    const brands: any[] = loadingBrands || dataBrands.brands;

    const { loading: loadingCat, data: dataCat } = useQuery(ALL_CATEGORIES);
    const categories: any[] = loadingCat || dataCat.categories;

    // const { loading: loadingColors, data: dataColors } = useQuery(ALL_COLORS);
    // const colors: any[] = loadingColors || dataColors.colors;

    // const [
    //     getProduct,
    //     { loading: loadingProduct, data: dataProduct }
    // ] = useLazyQuery(PRODUCT_QUERY, {
    //     fetchPolicy: "network-only"
    // });
    // const product: any[] = loadingProduct || dataProduct && dataProduct.product;
    // console.log(product);
    // const selectProduct = (id: string) => {
    //     getProduct({
    //         variables: {
    //             key: id
    //         }
    //     });
    //     setSelectedProduct(id);
    // }

    const values = {
        products,
        brands,
        categories,
        selectedSubCategory,
        selectedBrand,
        setSelectedSubCategory,
        setSelectedBrand
    }

    return (
        // <ProductContext.Provider value={values}>{props.children}</ProductContext.Provider>
        <LocalStateProvider value={values}>{props.children}</LocalStateProvider>

    );
};

// export const ProductConsumer = ProductContext.Consumer;
// export default ProductContext;

function useProducts(): ProductState {
    const all = useContext(LocalStateContext);
    return all;
}

export { ProductStateProvider, useProducts }

ProductStateProvider.propTypes = {
    children: PropTypes.node.isRequired,
};