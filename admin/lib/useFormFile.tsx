import { useState } from 'react';

export interface FormFileFields {
    file: any
}

export function useFormFile(initial: FormFileFields = {
    file: '',
}) {
    const [inputsFile, setInputs] = useState(initial);

    function handleChangeFile(e) {
        let { value, name, type } = e.target;
        if (type === 'file') {
            [value] = e.target.files;
        }
        setInputs({
            ...inputsFile,
            [e.target.name]: e.target.value
        });
    }

    function resetForm() {
        setInputs(initial);
    }

    // function clearForm() {
    //     //const blankState:FormFields = Object.fromEntries(Object.entries(inputs).map(([key, value]) => [key, '']))
    //     let blankState: FormFileFields = { ...inputsFile };
    //     Object.keys(blankState).forEach(key => {
    //         blankState[key] = '';
    //     })
    //     console.log('blankState', blankState);
    //     setInputs(blankState);
    // }

    return {
        inputsFile,
        handleChangeFile
    }
}