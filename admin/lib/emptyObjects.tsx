
import { Color, File, Sizes } from './useForm';

const emptyColor: Color = {
    id: '',
    name: '',
    hex: ''
}
const emptyFile: File = {
    id: '',
    public_id: '',
    filename: '',
    mimetype: '',
    encoding: ''
}
const emptySizes: Sizes = {
    color: null,
    sizes: []
}

export {
    emptyColor,
    emptyFile,
    emptySizes,
}