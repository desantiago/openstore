import { AppProps } from 'next/app'
import './_app.css'
import Page from '../components/Page'
import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { ApolloProvider } from '@apollo/client';
import withData from '../lib/withData';
import { AuthProvider } from '../lib/auth';
import { CartStateProvider } from '../lib/cartContext';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

// function App({ Component, pageProps, apollo }) {
//   return (
//     <AuthProvider>
//       <ApolloProvider client={apollo}>
//         <Page>
//           <Component {...pageProps} />
//         </Page>
//       </ApolloProvider>
//     </AuthProvider>
//   )
// }

function App({ Component, pageProps, apollo }) {
  return (
    <AuthProvider>
      <CartStateProvider>
        <Page>
          <Component {...pageProps} />
        </Page>
      </CartStateProvider>
    </AuthProvider>
  )
}


App.getInitialProps = async function ({ Component, ctx }) {
  let pageProps = {};
  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }
  pageProps.query = ctx.query;
  return pageProps;
}

export default withData(App)