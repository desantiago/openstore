import CreateProduct from "../components/CreateProduct";
import { gql } from "@apollo/client";
import { GetStaticProps } from 'next'
import { ApolloClient, InMemoryCache } from '@apollo/client';

const ALL_QUERIES = gql`
    query ALL_QUERIES {
        categories {
            key
            description
            subcategories {
                key
                description
            }
        }
        colors {
            key
            name
            hex
        }
        brands {
            description,
            key
        }          
    }
`

function AddProduct({ categories, colors, brands }) {
    return (
        <div>
            <CreateProduct categories={categories} colors={colors} brands={brands} />
        </div>
    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    const client2 = new ApolloClient({
        uri: 'http://localhost:3000/graphql',
        cache: new InMemoryCache()
    });

    const { data } = await client2.query({
        query: ALL_QUERIES
    });

    return {
        props: {
            categories: data.categories,
            colors: data.colors,
            brands: data.brands
        }
    }
}

export default AddProduct;