import { useState, useRef, useEffect } from 'react';
import Head from 'next/head'
import styled from 'styled-components';
import { GetStaticProps } from 'next'
import { ApolloClient, InMemoryCache } from '@apollo/client';

import { FiltersSelected, FilterOption } from '../../components/styles/FiltersSelected';
import { Products, ProductData, PlaceHolder, ProductImg, ProductBrand, ProductName, ProductPrice } from '../../components/styles/Product';

import FilterMenu from '../../components/styles/FilterMenu';
import SidebarFilters from '../../components/SidebarFilters';
import { gql, useQuery, useLazyQuery } from '@apollo/client';
import Link from 'next/link';
import PaginationLinks from '../../components/PaginationLinks';
import { useRouter } from 'next/router';

const ALL_CATEGORIES_QUERY = gql`
  query ALL_CATEGORIES_QUERY {
    categories {
      key
      description
      subcategories {
        key
        description
      }
    }
  }
`

const ALL_PRODUCTS_QUERY = gql`
  query ALL_PRODUCTS_QUERY($subCategory: String) {
    products(subCategory: $subCategory) {
      key
      name
      brand {
        key
        description
      }
      images {
        color {
          name
        }
        images {
          key
          filename
        }
      }
      prices {
        prices {
          size
          price
        }
      }    
    }
    aggregations(subCategory: $subCategory) {
      count
      colors {
        key
        name
      }
      sizes
      brands {
        key
        description
      }
    }
  }
`

function ProductPage() {
    // console.log('categories', categories, productsServer, aggregationsServer);
    // if (!categories) return <div></div>

    const { loading: loadingCategories, data: dataCategories } = useQuery(ALL_CATEGORIES_QUERY)
    const categories: any[] = loadingCategories || dataCategories.categories;

    const [categorySelected, setCategorySelected] = useState('1c83e697-79bd-4e63-b933-3b5505ac9123');
    const [subCategorySelected, setSubCategorySelected] = useState('987bd7cc-0b79-4aff-ba7e-207af19e90eb');

    const { loading, data, refetch } = useQuery(ALL_PRODUCTS_QUERY, {
        variables: {
            subCategory: subCategorySelected
        }
    });
    const products: any[] = loading || data.products;
    const aggregations: any[] = loading || data.aggregations;

    // const [products, setProducts] = useState(productsServer);
    // const [aggregations, setAggregations] = useState(aggregationsServer);

    // console.log('subCategorySelected', subCategorySelected);

    // const [searchProducts] = useLazyQuery(ALL_PRODUCTS_QUERY, {
    //     variables: { subCategory: subCategorySelected },
    //     onCompleted: data => {
    //         console.log("aggregation", data.aggregations);
    //         setProducts(data.products)
    //         setAggregations(data.aggregations);
    //     }
    // });

    // if (loading) return <p>Loading..</p>
    // if (error) return <p>Error.. </p>

    const changeFilter = (subCategoryKey) => {
        // console.log("SubCategory Selected :", subCategoryKey);
        setSubCategorySelected(subCategoryKey);
        refetch();
        // searchProducts({
        //     variables: {
        //         subCategory: subCategoryKey
        //     }
        // });
    }

    const { query } = useRouter();
    //const page = parseInt(query.page);    
    //console.log(query.page);
    const page = Number(query.page);

    //if (!Array.isArray(products))
    if (loadingCategories) return <div></div>
    if (loading) return <div></div>

    console.log(categories);
    return (
        <MainWraper>
            <Head>
                <title>Always Avantgarde</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <SideBar>
                <SidebarFilters
                    changeFilter={changeFilter}
                    categories={categories}
                    categorySelected={categorySelected}
                    subCategorySelected={subCategorySelected}
                    aggregations={aggregations} />
            </SideBar>

            <Content>
                <FiltersSelected >
                    <FilterOption href="#">Pumps <i className="lni lni-cross-circle"></i></FilterOption>
                    <FilterOption href="#">12 <i className="lni lni-cross-circle"></i></FilterOption>
                    <FilterOption href="#">Clear All</FilterOption>
                </FiltersSelected>
                <FilterMenu />

                <Products >
                    {products && products.map(product => {
                        return (
                            <Link key={product.key + "link"} href={"product/" + product.key}>
                                <PlaceHolder key={product.key}>
                                    <ProductImg src={"http://127.0.0.1:8080/" + product.images[0].images[0].filename} />
                                    <ProductData>
                                        <ProductBrand>{product.brand.description}</ProductBrand>
                                        <ProductName>{product.name}</ProductName>
                                        <ProductPrice>${product.prices[0].prices[0].price}</ProductPrice>
                                    </ProductData>
                                </PlaceHolder>
                            </Link>
                        )
                    })}
                    {/* <PlaceHolder>
            <ProductImg src="images/intermix/9XX03186703_1.jpeg" />
            <ProductData>
              <ProductBrand>MANOLO BLAHNIK</ProductBrand>
              <ProductName>Hangisi Crystal Satin Pumps</ProductName>
              <ProductPrice>$995</ProductPrice>
            </ProductData>
          </PlaceHolder>
          */}
                </Products>

                <PaginationLinks currentPage={page} numProducts={aggregations.count} />

                {/* <Pagination>
          <PaginationList>
            <PaginationPage className="selected"><a href="#">1</a></PaginationPage>
            <PaginationPage><a href="#">2</a></PaginationPage>
            <PaginationPage><a href="#">3</a></PaginationPage>
            <PaginationPage><a href="#">4</a></PaginationPage>
            <PaginationPage><a href="#">5</a></PaginationPage>
            <PaginationPage><a href="#"><i className="lni lni-arrow-right"></i></a></PaginationPage>
            <PaginationPage><a href="#"><i className="lni lni-arrow-right-circle"></i></a></PaginationPage>
          </PaginationList>
        </Pagination> */}
            </Content>

        </MainWraper>
    )
}


// export const getStaticProps: GetStaticProps = async (context) => {
//     // console.log("context: ", context);

//     const client2 = new ApolloClient({
//         uri: 'http://localhost:3000/graphql',
//         cache: new InMemoryCache()
//     });

//     const { data } = await client2.query({
//         query: ALL_CATEGORIES_QUERY
//     });

//     // console.log("---------------------------------");
//     if (data.categories) {
//         const subCategory = data.categories[0].subcategories[0].key;
//         // console.log("1", subCategory);

//         const { data: dataProducts } = await client2.query({
//             query: ALL_PRODUCTS_QUERY,
//             variables: {
//                 subCategory
//             }
//         });

//         return {
//             props: {
//                 categories: data.categories,
//                 productsServer: dataProducts.products,
//                 aggregationsServer: dataProducts.aggregations
//             }
//         }
//     }
//     else {
//         return {
//             props: {
//                 categories: data.categories,
//                 productsServer: [],
//                 aggregations: []
//             }
//         }
//     }
// }

export default ProductPage;

const MainWraper = styled.div`
  display:block;

  @media only screen and (min-width: 800px)   {
    display: grid;
    grid-gap: 0px;
    grid-template-columns: 200px auto;
    grid-template-areas:
    "sidebar content"
  }
`
const SideBar = styled.aside`
  display:none;
  @media only screen and (min-width: 800px)   {
    display: block;
    grid-area: sidebar;
  }
`;

const Content = styled.main`
  grid-area: content;
`
