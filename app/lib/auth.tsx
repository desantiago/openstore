import React, { useState, useContext, createContext } from 'react'
import {
    ApolloProvider,
    ApolloClient,
    InMemoryCache,
    HttpLink,
    gql,
    ApolloLink,
    useMutation,
} from '@apollo/client'
import { onError } from '@apollo/link-error';
import { endpoint, prodEndpoint } from '../config';
import { createUploadLink } from 'apollo-upload-client';
import { getToken, saveToken } from '../lib/storage';
// import { CURRENT_USER_QUERY } from '../components/User';

export const authContext = createContext({});

export function AuthProvider({ children }) {
    const auth = useProvideAuth()

    return (
        <authContext.Provider value={auth} >
            <ApolloProvider client={auth.createApolloClient()}>
                {children}
            </ApolloProvider>
        </authContext.Provider>
    )
}

export const useAuth = () => {
    return useContext(authContext)
}

const SIGNIN_MUTATION = gql`
    mutation SIGNIN_MUTATION($email: String!, $password: String!) {
        signIn(email: $email, password: $password) {
            token
        }
    }
`

const SIGNUP_MUTATION = gql`
    mutation SIGNUP_MUTATION($name: String!, $email: String!, $password: String!) {
        signUp(name: $name, email: $email, password: $password) {
            token
        }
    }
`

function useProvideAuth() {
    const [authToken, setAuthToken] = useState(getToken())

    const isSignedIn = () => {
        if (authToken) {
            return true
        } else {
            return false
        }
    }

    const getAuthHeaders = () => {
        if (!authToken) return null

        return {
            authorization: `Bearer ${authToken}`,
        }
    }

    // const createApolloClient = () => {
    //     const link = new HttpLink({
    //         uri: 'http://localhost:4001/graphql',
    //         headers: getAuthHeaders(),
    //     })

    //     return new ApolloClient({
    //         link,
    //         cache: new InMemoryCache(),
    //     })
    // }

    const createApolloClient = () => {
        return new ApolloClient({
            link: ApolloLink.from([
                onError(({ graphQLErrors, networkError }) => {
                    if (graphQLErrors)
                        graphQLErrors.forEach(({ message, locations, path }) =>
                            console.log(
                                `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
                            )
                        );
                    if (networkError)
                        console.log(
                            `[Network error]: ${networkError}. Backend is unreachable. Is it running?`
                        );
                }),
                // this uses apollo-link-http under the hood, so all the options here come from that package
                createUploadLink({
                    uri: process.env.NODE_ENV === 'development' ? endpoint : prodEndpoint,
                    fetchOptions: {
                        // credentials: 'include',
                    },
                    // pass the headers along from this request. This enables SSR with logged in state
                    headers: {
                        //...headers,
                        'x-token': authToken
                    }
                }),
            ]),
            cache: new InMemoryCache({
                typePolicies: {
                    Query: {
                        fields: {
                            // TODO: We will add this together!
                            // allProducts: paginationField(),
                        },
                    },
                },
            }).restore({}),
        });
    }

    const signIn = async (email, password) => {
        // console.log(email, password);
        const client = createApolloClient()

        const res = await client.mutate({
            mutation: SIGNIN_MUTATION,
            variables: {
                email,
                password
            },
        })

        // console.log(res)

        if (res?.data?.signIn?.token) {
            setAuthToken(res.data.signIn.token)
            saveToken(res.data.signIn.token);
            return res.data.signIn.token;
        }
    }

    const signUp = async (name, email, password) => {
        console.log(name, email, password);
        const client = createApolloClient()

        const res = await client.mutate({
            mutation: SIGNUP_MUTATION,
            variables: {
                name,
                email,
                password
            },
        })

        console.log(res)

        if (res?.data?.signUp?.token) {
            setAuthToken(res.data.signUp.token)
            saveToken(res.data.signUp.token);
            return res.data.signUp.token;
        }
    }

    const signOut = () => {
        setAuthToken(null)
        saveToken('');
    }

    return {
        setAuthToken,
        isSignedIn,
        signIn,
        signOut,
        signUp,
        createApolloClient,
    }
}

// function createUploadLink(arg0: {
//     uri: any; fetchOptions: {};
//     // pass the headers along from this request. This enables SSR with logged in state
//     headers: any;
// }): ApolloLink | import("@apollo/client").RequestHandler {
//     throw new Error('Function not implemented.');
// }
