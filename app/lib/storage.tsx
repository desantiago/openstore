
function saveToken(token) {
    if (typeof window === 'undefined') return;
    localStorage.setItem('token', token);
}

function getToken() {
    console.log('getToken');
    if (typeof window === 'undefined') return;
    console.log('getting totken', localStorage.getItem('token'))
    return localStorage.getItem('token');
}

export {
    saveToken,
    getToken
}