export interface Color {
    id: string
    key: string
    name: string
    hex: string
}

export interface File {
    id?: string
    key?: string
    filename?: string
    public_id?: string
    mimetype?: string
    encoding?: string
}

export interface ImageFiles {
    file: File
    order: number
}

export interface Brand {
    id: string
    key: string
    description: String
    slug: string
    timeStamp: string
}

export interface Category {
    id: string
    key: string
    description: string
    slug: string,
    timeStamp: string
    subcategories: SubCategory[]
}

export interface SubCategory {
    id: string
    key: string
    description: string
    slug: string
    timeStamp: string
    categoryKey: string
    category: Category
}

export interface Image {
    file: File
    order: number
}

export interface Images {
    color: Color
    images: Image[]
}

export interface Price {
    size: string,
    price: number
}

export interface Prices {
    color: Color
    prices: Price[]
}

export interface Sizes {
    color: Color
    sizes: string[]
}

export interface ImageColor {
    color: Color;
    image: File;
}

export interface RelatedProduct {
    product: Product;
    order: number;
}

export interface Product {
    id: string;
    key: string;

    externalId?: string;
    slug: string;

    name: string;
    description: string;
    sizeAndFit: string;

    price: number;
    startSize: number;
    endSize: number;

    details: string;
    colors: Color[];

    images: Images[];
    sizes?: Sizes[];
    prices?: Prices[];

    // images: DynamicObject<string[]>;
    // sizes?: DynamicObject<number[]>;
    // price?: DynamicObject<DynamicObject<number>>;
    timeStamp?: string;
    brand?: Brand;
    category?: Category;
    subCategory?: SubCategory;

    thumbnail: ImageColor;
    mainImage: ImageColor;

    relatedProducts: RelatedProduct[];
}

export interface Aggregation {
    count: number
    mode: string
    colors: Color[]
    size: string[]
    brands: Brand[]
}

export interface QueryParams {
    param?: string[],
    category?: string[],
    subCategory?: string[],
    brands?: string[],
    colors?: string[],
    sizes?: string[],
}


export const BRAND: string = 'BRAND';
export const CATEGORY: string = 'CATEGORY';
export const SUBCATEGORY: string = 'SUBCATEGORY';