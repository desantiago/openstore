import { gql, useQuery } from "@apollo/client";
import { useRouter } from "next/router";
import { createContext, useContext, useState } from "react";

const ALL_CATEGORIES_QUERY = gql`
    query ALL_CATEGORIES_QUERY {
        categories {
            key
            slug
            description
            subcategories {
                slug
                key
                description
            }
        }
    }
`

export type CategoriesState = {
    currentPage?: number;
    mode?: string,
    category?: string,
    subCategory?: string,
    brand?: string,

    categories?: string[],
    subCategories?: string[],
    brands?: string[],
    sizes?: string[],
    colors?: string[],

    allCategories?: any,
    loadingCategories?: boolean,

    setCategories?: (string) => void,
    setSubCategories?: (string) => void,
    setBrands?: (string) => void,
    setSizes?: (string) => void,
    setColors?: (string) => void,

    // openCart?: () => void,
    // closeCart?: () => void,

    // colorSelected?: string,
    // sizeSelected?: string,
    // setColorSelected?: (string) => void,
    // setSizeSelected?: (string) => void
}

const LocalStateContext = createContext<CategoriesState>({});
const LocalStateProvider = LocalStateContext.Provider;

function isNumeric(str) {
    if (typeof str != "string") return false // we only process strings!  
    return !isNaN(Number(str));
}

function getContext() {
    if (typeof window === "undefined") return null;
    // window.localStorage
    const context = localStorage.getItem('context');
    if (!context) return null;

    try {
        return JSON.parse(context);
    } catch (error) {
        return null;
    }
}

function CategoriesStateProvider({ children }) {

    // const { query } = useRouter();
    // // console.log(query);
    // // console.log(query.page);\
    // const first: string = query.first ? query.first.toString() : null;
    // const second: string = query.second ? query.second.toString() : null;
    // const third = query.page ? Number(query.page) : 1;

    // let mode = '';
    // let category: string = '';
    // let subCategory: string = '';
    // let brand: string = '';

    // let categoriesInit: string[] = [first];
    // //if (first) categoriesInit.push(first);

    // let subCategoriesInit: string[] = [];
    // let currentPage = 0;
    // if (!second) {
    //     currentPage = 1;
    // }
    // else if (isNumeric(second)) {
    //     currentPage = Number(second);
    // }
    // else {
    //     subCategoriesInit = [second];
    //     currentPage = third ? Number(third) : 1;
    // }
    // console.log('categories : ', categoriesInit, subCategoriesInit, currentPage);

    // let brandsInit = [];
    // let sizesInit = [];
    // let colorsInit = [];

    // // const [selectedProduct, setSelectedProduct] = useState('');
    // //const [page, setPage] = useState(currentPage);
    // const page = currentPage;

    // const context = getContext();

    const { loading: loadingCategories, data: dataCategories } = useQuery(ALL_CATEGORIES_QUERY)
    const allCategories: any[] = loadingCategories || dataCategories.categories;

    // if (!loadingCategories && allCategories) {
    //     //console.log('allCategories', allCategories);
    //     const cat = allCategories.find(cat => cat.slug === categoriesInit[0]);
    //     if (cat) {
    //         mode = 'category';
    //         category = categoriesInit[0];
    //         subCategory = subCategoriesInit.length ? subCategoriesInit[0] : '';
    //         brand = null;
    //     }
    //     else {
    //         // console.log('--', categories[0]);
    //         mode = 'brand'
    //         brandsInit = [categoriesInit[0]];
    //         brand = brandsInit[0];
    //         category = null;
    //         subCategory = null;
    //         categoriesInit = [];
    //         subCategoriesInit = [];
    //         // console.log('--', mode, brand, brandsInit);
    //         //setBrands(brandsInit);
    //         console.log('categories2 : ', categoriesInit, subCategoriesInit, brandsInit, currentPage);
    //     }
    //     //mode = cat ? 'category' : 'brand';
    // }

    // const equals = (a, b) => a.length === b.length && a.every((v, i) => v === b[i]);
    // // if (context && context.category === category && context.subcategory === subcategory) {
    // if (context && mode === 'category' && equals(context.categories, categoriesInit) && equals(context.subCategories, subCategoriesInit)) {
    //     brandsInit = context.brands;
    //     sizesInit = context.sizes;
    //     colorsInit = context.colors;
    //     // console.log('sizesInit', sizesInit);
    // }

    // const [categories, setCategories] = useState([...categoriesInit]);
    // const [subCategories, setSubCategories] = useState([...subCategoriesInit]);
    const [brands, setBrands] = useState([]);
    // const [sizes, setSizes] = useState(sizesInit);
    // const [colors, setColors] = useState(colorsInit);

    // const [cartOpen, setCartOpen] = useState(false);
    // const [colorSelected, setColorSelected] = useState('');
    // const [sizeSelected, setSizeSelected] = useState('');

    // const openCart = () => {
    //     setCartOpen(true);
    // }

    // const closeCart = () => {
    //     console.log("here");
    //     setCartOpen(false);
    // }

    // const toogleCart = () => {
    //     setCartOpen(!cartOpen);
    // }

    const values: CategoriesState = {
        // mode,
        // category,
        // subCategory,
        // brand,

        // categories,
        // subCategories,
        brands,
        // sizes,
        // colors,
        // setCategories,
        // setSubCategories,
        setBrands,
        // setSizes,
        // setColors,

        allCategories,
        loadingCategories,

        // currentPage
        // cartOpen,
        // colorSelected,
        // sizeSelected,
        // openCart,
        // closeCart,
        // setColorSelected,
        // setSizeSelected
    }

    return (
        <LocalStateProvider value={values}>{children}</LocalStateProvider>
    );
}

function useCategories(): CategoriesState {
    const all = useContext(LocalStateContext);
    return all;
}

export { CategoriesStateProvider, useCategories }