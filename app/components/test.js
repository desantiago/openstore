
const text = null;
// const text = "one two three fourth five";

function reverseWords(text) {
    if (!text) return '';
    let words = text.split(' ');

    // for (let i = 0; i < words.length - 1; i++) {
    //     const word = words.pop();
    //     words.splice(i, 0, word);
    //     console.log(words);
    // }

    const length = words.length / 2;

    for (let i = 0; i < length; i++) {
        const lastPos = (words.length - 1) - i;
        const t = words[lastPos];
        words[lastPos] = words[i];
        words[i] = t;
    }

    // let result = [];
    // for (let i = words.length - 1; i >= 0; i--) {
    //     result.push(words[i]);
    // }

    return words;
}

console.log(reverseWords(text));

import { ChangeEvent, useState, useEffect } from "react";
import { gql, useMutation } from "@apollo/client";

import { useForm, FormFields } from "../lib/useForm"
// import { useFormFile, FormFieldsFile } from "../lib/useFormFile"
import Form from "../components/styles/Form";
import styled from 'styled-components';

const CREATE_PRODUCT_MUTATION = gql`
    mutation CREATE_PRODUCT_MUTATION(
        $name: String!
        $description: String!
        $sizeAndFit: String
        $details: String
        $brand: String!
        $category: String!
        $subCategory: String!
        $colors: [String],
        $images: [ImagesInput]
        $sizes: [SizesInput]
        $prices: [PricesInput]
    ) {
        createProduct(input: {
            key: ""
            name: $name
            description: $description
            sizeAndFit: $sizeAndFit
            details: $details
            timeStamp: ""
            brand: $brand
            category: $category
            subCategory: $subCategory
            colors: $colors
            images: $images
            sizes: $sizes
            prices: $prices
        }) {
            key
        }
    }    
`

// const UPLOAD_FILE = gql`
//     mutation SingleUpload($file: Upload!) {
//         singleUpload(file: $file) {
//             filename
//             mimetype
//             encoding
//         }
//     }
// `;

const UPLOAD_FILES = gql`
    mutation multiUpload($files: Upload!) {
        multiUpload(files: $files) {
            key
            filename
            mimetype
            encoding
        }
    }
`;

const ImageList = styled.ul`
    list-style-type: none;

    li {
        display: inline-block
    }
    img {
        width: 150px; 
        height:auto;
    }
`

const MainWraper = styled.div`
    display:block;

    @media only screen and (min-width: 800px)   {
        display: grid;
        grid-gap: 0px;
        grid-template-columns: 400px auto auto;
        grid-template-areas:
        "sidebar content images"
    }
`
const SideBar = styled.aside`
    display:none;
    @media only screen and (min-width: 800px)   {
        display: block;
        grid-area: sidebar;
    }
`;

const Content = styled.main`
    grid-area: content;
`
const ImageContainer = styled.div`
    grid-area: images;
`

const ColorOptions = styled.div`
    margin-top: 0.5rem;
    display: grid;
    grid-gap: 0px;
    grid-template-columns: repeat(auto-fill, minmax(70px, 1fr));
    max-width: none;
`

const ColorOption = styled.div`
    width: auto;
    height: 40px;
    background-color: #efefef;
    color: #494949;
    font-weight: normal;
    line-height: 40px;
    font-family: Helvetica, sans-serif;
    font-size: 0.8rem;
    text-align: center;
    margin-bottom: 0.2rem;
    margin-right: 0.1rem;
    padding: 0 0.1rem;
    white-space: nowrap;
    cursor: pointer;

    &.selected { 
        background-color:#EF3B68;
        color: #fff;
    }
`

const ColorSelected = styled(ColorOption)`
    border: 1px solid #efefef;
    background-color: #fff;
    color: #494949;

    &.selected { 
        background-color: #efefef;
        color: #494949;
        font-weight: bolder;
    }
`
// grid-template-columns: 100px 100px 50px;
// grid-template-areas:
// "size price remove"

const SizeGrid = styled.div`
    display: grid;
    grid-gap: 2px;
    grid-template-columns: 50px 100px 50px;
`

const SizeCont = styled.div`
    padding-top: 0.25rem;
`
const PriceCont = styled.div`
    justify-self: center;
`
const RemoveCont = styled.div`
    padding-top: 0.25rem;
    justify-self: center;
`
const RemoveImage = styled.div`
    position: absolute;
    top: 0;
    right: 0;

    a {
        text-decoration: none;
        color: var(--main-color);
        font-size: 2.0rem;
    }
`


function CreateProduct({ categories, brands, colors }) {
    const initialValue: FormFields = {
        name: '',
        price: 0,
        description: '',
        sizeAndFit: '',
        details: '',
        colors: [],
        images: [],
        prices: [],
        sizes: [],
        brand: '',
        category: '',
        subCategory: '',
        startSize: '',
        endSize: ''
    };

    // console.log("Create Product", categories, brands, colors)

    const [images, setImages] = useState([]);
    const { inputs, handleChange, clearForm, setColor } = useForm(initialValue);

    // const [createProduct, { loading, error, data }] = useMutation(CREATE_PRODUCT_MUTATION, {
    //     variables: inputs
    // });
    const [createProduct, { loading, error, data }] = useMutation(CREATE_PRODUCT_MUTATION);

    // const [uploadFile, { loading: loadingFile, error: errorFile, data: dataFile }] = useMutation(UPLOAD_FILE);
    const [uploadFiles] = useMutation(UPLOAD_FILES);
    const [colorsSelected, setColorsSelected] = useState([]);

    // const onChange = ({
    //     target: {
    //         validity,
    //         files: [file],
    //     },
    // }: ChangeEvent<HTMLInputElement>) =>
    //     validity.valid &&
    //     uploadFile({ variables: { file } }).then(() => {
    //         //apolloClient.resetStore();
    //         console.log("upload");
    //     });

    // const onChange = (event: ChangeEvent<HTMLInputElement>): void => {
    //     const input = event.target as HTMLInputElement;

    //     if (!input.files?.length) {
    //         return;
    //     }

    //     const file = input.files[0];
    //     console.log(file);

    //     uploadFile({ variables: { file } }).then(() => {
    //         //         console.log("upload");
    //     });
    // }

    const checkColors = (colorKey) => {
        var index = colorsSelected.findIndex(color => color.key === colorKey);
        if (index !== -1) {
            colorsSelected.splice(index, 1);
            if (!colorsSelected.some(color => color.selected)) {
                colorsSelected[0].selected = true;
            }
        }
        else {
            colorsSelected.forEach(color => {
                color.selected = false;
            });
            colorsSelected.push({
                ...colors.find(color => color.key == colorKey),
                selected: true,
                images: [],
                sizes: getSizes()
            });
            setColorsSelected([...colorsSelected]);
        }
    }

    const getSizes = () => {
        let sizes = [];
        if (Number(inputs.startSize) && Number(inputs.endSize)) {
            const start = Number(inputs.startSize);
            const end = Number(inputs.endSize);
            for (let i = start; i <= end; i++) {
                sizes.push({
                    size: i + '',
                    price: Number(inputs.price)
                });
            }
        }
        return sizes;
    }

    const getSizesColor = (color, start, end) => {
        let sizes = [];
        for (let i = start; i <= end; i++) {
            const prevSize = color.sizes.find(size => size.size === i);
            sizes.push({
                size: i + '',
                price: prevSize ? prevSize.price : inputs.price
            });
        }
        console.log(sizes);
        return sizes;
    }

    const checkSizes = () => {
        if (Number(inputs.startSize) && Number(inputs.endSize)) {
            const start = Number(inputs.startSize);
            const end = Number(inputs.endSize);

            if (start < end) {
                console.log("vamos a ir", start, end);

                // colorsSelected.forEach(color => {
                //     let sizes = [];
                //     for (let i = start; i <= end; i++) {
                //         const prevSize = color.sizes.find(size => size.size === i);
                //         sizes.push({
                //             size: i,
                //             price: prevSize ? prevSize.price : 0
                //         });
                //     }
                //     console.log(sizes);
                //     color.sizes = [...sizes];
                // });

                colorsSelected.forEach(color => {
                    color.sizes = getSizesColor(color, start, end);
                });

                setColorsSelected([...colorsSelected]);
            }
        }
    }

    const getPrice = (sizes, price) => {
        sizes.forEach(size => {
            size.price = price
        });
        return sizes;
    }

    const checkPrices = () => {
        if (Number(inputs.price)) {
            colorsSelected.forEach(color => {
                color.sizes = getPrice(color.sizes, Number(inputs.price));
            });
        }
        setColorsSelected([...colorsSelected]);
    }

    // useEffect(() => {
    //     console.log('colorsSelected', colorsSelected);
    // }, [colorsSelected])

    useEffect(() => {
        checkSizes()
    }, [inputs.startSize, inputs.endSize]);

    useEffect(() => {
        checkPrices()
    }, [inputs.price]);

    const clickColor = (key) => {
        // console.log("color", key);
        setColor(key);
        checkColors(key);
    }

    const clickColorSelected = (key) => {
        for (let i = 0; i < colorsSelected.length; i++) {
            const color = colorsSelected[i];
            colorsSelected[i].selected = false;
            if (color.key === key) {
                colorsSelected[i].selected = true;
            }
        }
        // var index = colorsSelected.findIndex(color => color.key === key);
        // colorsSelected[index].selected = true;
        // console.log(colorsSelected[index]);
        setColorsSelected([...colorsSelected]);
    }

    const onChange = (event: ChangeEvent<HTMLInputElement>): void => {
        const input = event.target as HTMLInputElement;

        if (!input.files?.length) {
            return;
        }

        uploadFiles({ variables: { files: input.files } }).then(({ data }) => {
            // console.log("upload", [...images, ...data.multiUpload]);
            const index = colorsSelected.findIndex(color => color.selected);
            console.log("upload", [...colorsSelected[index].images, ...data.multiUpload]);
            colorsSelected[index].images = [...colorsSelected[index].images, ...data.multiUpload]
            setColorsSelected([...colorsSelected]);
            // setImages([...images, ...data.multiUpload]);
        });
    }

    const onChangePrice = (size: string, value: string): void => {
        const index = colorsSelected.findIndex(color => color.selected);
        const index2 = colorsSelected[index].sizes.findIndex(s => s.size === size);
        colorsSelected[index].sizes[index2].price = value;
        setColorsSelected([...colorsSelected]);
    }

    const removeSize = (size: string): void => {
        const index = colorsSelected.findIndex(color => color.selected);
        const index2 = colorsSelected[index].sizes.findIndex(s => s.size === size);
        colorsSelected[index].sizes.splice(index2, 1);
        setColorsSelected([...colorsSelected]);
    }

    const removeImage = (imageKey: string): void => {
        const index = colorsSelected.findIndex(color => color.selected);
        const index2 = colorsSelected[index].images.findIndex(image => image.key === imageKey);
        colorsSelected[index].images.splice(index2, 1);
        setColorsSelected([...colorsSelected]);
    }

    const saveProduct = async () => {
        console.log(inputs);

        // #     colors: [{
        //     #       name: "white"
        //     #       hex: "#fff"
        //     #     }],
        //     #     images: [
        //     #       {
        //     #         name: "white"
        //     #         images: ["image1.jpg", "image2.jpg"]
        //     #       }
        //     #     ],
        //     #     sizes: [
        //     #       {
        //     #         name: "white"
        //     #         sizes: ["5","6","7","8","9","10"]
        //     #       }
        //     #     ],
        //     #     prices: [
        //     #       {
        //     #         name: "white"
        //     #         prices: [
        //     #           {
        //     #             size: "5"
        //     #             price: 500
        //     #           },

        // colorsSelected.push({
        //     ...colors.find(color => color.key == colorKey),
        //     selected: true,
        //     images: [],
        //     sizes: getSizes()
        // });

        // inputs.colors = [];
        // inputs.images = [];
        // inputs.sizes = [];
        // inputs.prices = [];

        let productData = { ...inputs }
        colorsSelected.forEach(color => {
            //inputs.colors.push(color.key);
            productData.images.push({
                color: color.key,
                images: color.images.map(image => image.key)
            });
            productData.sizes.push({
                color: color.key,
                sizes: color.sizes.map(size => size.size)
            });
            productData.prices.push({
                color: color.key,
                prices: [...color.sizes]
            });
        });

        console.log(productData);
        // productData.colors.push("11111111");

        const res = await createProduct({
            variables: productData
        });

        console.log(res);
    }

    return (
        <>
            <MainWraper>
                <SideBar></SideBar>
                <Content>
                    <Form onSubmit={(e) => {
                        e.preventDefault();
                        saveProduct();
                    }}>
                        <fieldset disabled={loading} aria-busy={loading}>
                            <label htmlFor="name">
                                Name
                                <input
                                    type="text"
                                    value={inputs.name}
                                    id="name" name="name"
                                    placeholder="Name"
                                    onChange={handleChange} />
                            </label>
                            <label htmlFor="description">
                                Description
                                <textarea
                                    value={inputs.description}
                                    id="description"
                                    name="description"
                                    placeholder="Description"
                                    onChange={handleChange} />
                            </label>
                            <label htmlFor="subCategory">
                                Category
                                {/* <input
                                    type="text"
                                    value={inputs.subCategory}
                                    id="subCategory"
                                    name="subCategory"
                                    placeholder="SubCategory"
                                    onChange={handleChange} /> */}

                                <select
                                    id="subCategory"
                                    name="subCategory"
                                    value={inputs.subCategory}
                                    onChange={handleChange}>
                                    {
                                        categories && categories.map(category => {
                                            return (
                                                <optgroup label={category.description} key={category.key}>
                                                    {
                                                        category.subcategories.map(subCategory => <option key={subCategory.key} value={subCategory.key}>{subCategory.description}</option>)
                                                    }
                                                </optgroup>
                                            )
                                        })
                                    }
                                </select>
                            </label>

                            <label htmlFor="brand">
                                Brand
                                {/* <input
                                    type="text"
                                    value={inputs.brand}
                                    id="brand"
                                    name="brand"
                                    placeholder="Brand"
                                    onChange={handleChange} /> */}

                                <select
                                    id="brand"
                                    name="brand"
                                    value={inputs.brand}
                                    onChange={handleChange}
                                >
                                    {
                                        brands && brands.map(brand => <option key={brand.key} value={brand.key}>{brand.description}</option>)
                                    }
                                </select>

                            </label>

                            <label htmlFor="colors">
                                Colors
                                {/* <input
                                    type="text"
                                    value={inputs.colors}
                                    id="colors"
                                    name="colors"
                                    placeholder="Colors"
                                    onChange={handleChange} /> */}
                                <ColorOptions>
                                    {
                                        colors && colors.map(color => {
                                            return (
                                                <ColorOption
                                                    key={color.key}
                                                    className={inputs.colors.includes(color.key) ? 'selected' : ''}
                                                    onClick={() => clickColor(color.key)}>

                                                    {color.name}
                                                </ColorOption>
                                            )
                                        })
                                    }
                                </ColorOptions>
                            </label>
                            {/* <label htmlFor="category">
                                Category
                                <input
                                    type="text"
                                    value={inputs.category}
                                    id="category"
                                    name="category"
                                    placeholder="Category"
                                    onChange={handleChange} />
                            </label> */}

                            <label htmlFor="price">
                                Sizes
                                <ul style={{ listStyleType: 'none' }}>
                                    <li style={{ display: 'inline-block' }}>
                                        <input
                                            type="text"
                                            value={inputs.startSize}
                                            id="startSize"
                                            name="startSize"
                                            placeholder="startSize"
                                            onChange={handleChange} />
                                    </li>
                                    <li style={{ display: 'inline-block' }}>
                                        <input
                                            type="text"
                                            value={inputs.endSize}
                                            id="endSize"
                                            name="endSize"
                                            placeholder="endSize"
                                            onChange={handleChange} />
                                    </li>
                                </ul>
                            </label>

                            <label htmlFor="price">
                                Price
                                <input
                                    type="text"
                                    value={inputs.price}
                                    id="price"
                                    name="price"
                                    placeholder="Price"
                                    onChange={handleChange} />
                            </label>

                            <label htmlFor="details">
                                Details
                                <textarea
                                    value={inputs.details}
                                    id="details"
                                    name="details"
                                    placeholder="Details"
                                    onChange={handleChange} />
                            </label>
                            <label htmlFor="sizeAndFit">
                                Size And Fit
                                <textarea
                                    value={inputs.sizeAndFit}
                                    id="sizeAndFit" name="sizeAndFit"
                                    placeholder="Size And Fit"
                                    onChange={handleChange} />
                            </label>

                            <button type="submit">Add Product</button>
                        </fieldset>
                    </Form>
                </Content>

                <ImageContainer>
                    <Form >
                        <ColorOptions>
                            {
                                colorsSelected.map((color) =>
                                    <ColorSelected
                                        key={color.key} onClick={() =>
                                            clickColorSelected(color.key)}
                                        className={color.selected ? 'selected' : ''}
                                    >
                                        {color.name}
                                    </ColorSelected>
                                )
                            }
                        </ColorOptions>

                        <fieldset>
                            <label htmlFor="files">
                                Images
                                <input type="file" multiple id="files" name="files" onChange={onChange} />
                            </label>
                        </fieldset>
                        <ImageList>
                            {
                                colorsSelected.find((color) => color.selected)?.images.map(image => {
                                    return (
                                        <li key={image.key} style={{ position: 'relative' }}>
                                            <img src={'http://127.0.0.1:8080/' + image.filename} >
                                            </img>
                                            <RemoveImage>
                                                <a onClick={() => removeImage(image.key)}><i className="lni lni-cross-circle"></i></a>
                                            </RemoveImage>
                                        </li>
                                    )
                                })
                            }
                        </ImageList>
                        <SizeGrid>
                            {
                                colorsSelected.find((color) => color.selected)?.sizes.map((size) => {
                                    return (
                                        <>
                                            <SizeCont>{size.size}</SizeCont>
                                            <PriceCont>
                                                <input type="text" value={size.price} onChange={(e) => onChangePrice(size.size, e.target.value)} />
                                            </PriceCont>
                                            <RemoveCont>
                                                <a onClick={() => removeSize(size.size)}><i className="lni lni-cross-circle"></i></a>
                                            </RemoveCont>
                                        </>
                                    )
                                })
                            }
                        </SizeGrid>
                    </Form>
                </ImageContainer>

                {/* <Form onSubmit={async (e) => {
                    e.preventDefault();
                    console.log(inputsFile);
                    const res = await uploadFile();
                    console.log(res);
                }}>
                    <fieldset disabled={loadingFile} aria-busy={loadingFile}>
                        <label htmlFor="file">
                            Image
                            <input type="file" id="file" name="file" onChange={handleChangeFile} />
                        </label>
                        <button type="submit">Upload File</button>
                    </fieldset>
                </Form> */}

            </MainWraper>
        </>
    )
}

export default CreateProduct;
