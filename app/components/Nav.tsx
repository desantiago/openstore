import Link from 'next/link'
import { useContext, useEffect, useState } from 'react';
import styled, { css } from 'styled-components';

import { authContext } from '../lib/auth';
import { useCart } from '../lib/cartContext';
import CartCount from './CartCount';
import Search from './Search';
import { useUser } from './User';
import MegaMenu from './MegaMenu';

export default function Nav() {

    const {
        signOut
    }: any = useContext(authContext);

    const user = useUser();
    const { openCart } = useCart();

    const onSignOut = () => {
        signOut();
    }

    const count = user ? user.cartItems.reduce((total, cartItem) => { return total += cartItem.quantity }, 0) : 0;

    return (
        <nav>
            <NavWrapper>
                <NavColumnLeft >
                    <Search />
                </NavColumnLeft>
                <NavColumnCenter>

                    <MegaMenu />

                    {/* <NavTest>
                        <ul>
                            <DropDownTest>
                                <button type="button" className="dropdown__title" aria-expanded="false" aria-controls="sweets-dropdown">
                                    Designers
                                </button>
                                <div className="dropdown__menu" id="sweets-dropdown">
                                    <img src="/resources/redshoe.png" />
                                    <MenuOptions>
                                        <h3>Featured Designers</h3>
                                        <Link href="/products/alexandrebirman">Alexandre Birman</Link>
                                        <Link href="/products/alexandermcqueen">Alexander McQueen</Link>
                                        <Link href="/products/aminamuadi">Amina Muadi</Link>
                                        <Link href="/products/aquazzura">Aquazzura</Link>
                                        <Link href="/products/byfar">By Far</Link>
                                        <Link href="/products/staud">Staud</Link>
                                    </MenuOptions>
                                    <MenuOptions>
                                    </MenuOptions>
                                </div>
                            </DropDownTest>
                            <DropDownTest>
                                <button type="button" className="dropdown__title" aria-expanded="false" aria-controls="sweets-dropdown2">
                                    Shoes
                                </button>
                                <div className="dropdown__menu" id="sweets-dropdown2">
                                    <img style={{ width: '300px', height: 'auto' }} src="/resources/jimmychoo.webp" />
                                    <MenuOptions>
                                        <h3>By Type</h3>
                                        <Link href="/products/shoes/boots/1">Boots & Booties</Link>
                                        <Link href="/products/shoes/flats/1">Flats</Link>
                                        <Link href="/products/shoes/pumps/1">Pumps</Link>
                                        <Link href="/products/shoes/sandals/1">Sandals</Link>
                                        <a href="#">Sades</a>
                                        <a href="#">Sangbacks</a>
                                        <a href="#">Sneakers</a>
                                        <a href="#">Wedges</a>
                                    </MenuOptions>
                                    <MenuOptions>
                                        <h3>By Designer</h3>
                                        <Link href="/products/byfar">By Far</Link>
                                        <Link href="/products/francescorusso">Francesco Russo</Link>
                                        <Link href="/products/jimmychoo">Jimmy Choo</Link>
                                        <Link href="/products/manoloblahnik">Manolo Blahnik</Link>
                                        <Link href="/products/valentinogaravani">Valentino Garavani</Link>
                                    </MenuOptions>
                                </div>
                            </DropDownTest>
                            <DropDownTest>
                                <button type="button" className="dropdown__title" aria-expanded="false" aria-controls="sweets-dropdown3">
                                    Bags
                                </button>
                                <div className="dropdown__menu" id="sweets-dropdown3">
                                    <img style={{ width: '300px', height: 'auto' }} src="/resources/stella.webp" />
                                    <MenuOptions>
                                        <h3>By Type</h3>
                                        <a href="#">Backpacks</a>
                                        <a href="#">Belt</a>
                                        <a href="#">Clutch</a>
                                        <Link href="/products/bags/crossbody">Cross Body</Link>
                                        <a href="#">Evening </a>
                                        <a href="#">Luggage and Travel</a>
                                        <Link href="/products/bags/minibags">Mini Bags</Link>
                                        <Link href="/products/bags/shoulder">Shoulder</Link>
                                        <Link href="/products/bags/totes">Totes</Link>
                                    </MenuOptions>
                                    <MenuOptions>
                                        <h3>By Designer</h3>
                                        <a href="#">Balenciaga</a>
                                        <a href="#">Bottega Veneta</a>
                                        <a href="#">Chloé</a>
                                        <a href="#">Fendi</a>
                                        <a href="#">Givenchy</a>
                                        <a href="#">Gucci</a>
                                        <a href="#">Loewe</a>
                                        <a href="#">SAINT LAURENT</a>
                                        <a href="#">Stella McCartney</a>
                                        <a href="#">The Row</a>
                                    </MenuOptions>
                                </div>
                            </DropDownTest>
                        </ul>
                    </NavTest> */}

                </NavColumnCenter>
                <NavColumnRight>
                    <UserOptions>
                        {user &&
                            <>
                                <Option><Link href="/account">Account</Link></Option>
                                <Option onClick={onSignOut}>Sign Out</Option>
                            </>
                        }
                        {!user &&
                            <>
                                <Option></Option>
                                <Option><Link href="/signin">Sign In</Link></Option>
                            </>
                        }
                        <OptionCart onClick={openCart} style={{ margin: '0px' }}>
                            <i className="lni lni-wheelbarrow" style={{ marginTop: '3px' }}></i>
                        </OptionCart>
                        <OptionCart onClick={openCart} style={{ marginTop: '3px' }}>
                            <CartCount count={count}></CartCount>
                        </OptionCart>
                    </UserOptions>
                </NavColumnRight>
            </NavWrapper>
        </nav>
    )
}



const NavTest = styled.div`
    padding: 0 1rem;
    position: sticky;
    top: 0;
    display: grid;
    place-items: center;
  
    > ul {
        grid-auto-flow: column;
  
        > li {
            margin: 0 0.5rem;
  
            a,
            .dropdown__title {
                text-decoration: none;
                display: inline-block;
                color: #494949;
                font-size: 1.0rem;

                &:focus {
                    outline: none;
                }
            }

            > a,
            .dropdown__title {
                padding: 0.25rem 0.25rem;
                background-color: white;
                padding-left: 1.0rem;
                padding-right: 1.0rem;
                border-top: 3px solid transparent;
                transition: 280ms all 120ms ease-out;
        
                &:hover,
                &:focus {
                    border-top-color: var(--main-color);
                    color: #f1f1f1;
                    background-color: #202020;
                }
            }
        }
    }
  
    ul {
        list-style: none;
        margin: 0;
        padding: 0;
        display: grid;
    
        li {
            padding: 0;
        }
    }
`

const DropDownTest = styled.li`
    position: relative;

    .dropdown__title {
        display: inline-flex;
        align-items: center;
        text-align: center;
    }

    .dropdown__menu {
        position: absolute; 
        left: 50%;       
        top: calc(100% - 0.05rem);
        transition: 280ms all 120ms ease-out;
        transform: rotateX(-90deg) translateX(-50%);
        transform-origin: top center;
        visibility: hidden;
        opacity: 0.0;
        box-shadow: 0 0.15em 0.25em rgba(black, 0.25);
        z-index: 1000;

        a {
            color: #f1f1f1;
            font-family: 'Inter', sans-serif;
            font-size: 0.9rem;
            display: block;
            padding-bottom: 0.5em;
            opacity: 0;
            transition: 280ms all 120ms ease-out;
    
            &:hover {
                color: #fff;
                font-weight: bold;
            }
    
            &:focus {
                outline: none;
                color: #fff;
                font-weight: bold;
            }
        }

        background-color: #202020;
        display: grid;
        grid-template-columns: 33fr 33fr 33fr;
        width: 700px;
        text-align: left;

        h3 {
            font-size: 1.0rem;
            font-weight: bold; 
            margin-bottom: 0.5rem;
            font-family: 'M PLUS 1p', sans-serif;
            text-transform: uppercase;
            color: rgb(93,93,93);
        }
        p {
            margin-bottom: 0.25rem;
        }
        img { 
            width: 100%;
            height: auto;
        }
    }

    &:hover,
    &:focus-within {
        .dropdown__title {
            border-top-color: var(--main-color);
            color: #f1f1f1;
            background-color: #202020;
        }

        .dropdown__menu {
            opacity: 1;
            transform: rotateX(0) translateX(-50%);
            visibility: visible;

            a {
                opacity: 1;
            }
        }

        &:after {
            opacity: 1;
        }
    }
`
// -----------------------------------------------

const NavWrapper = styled.div`
display: none;

@media only screen and (min-width: 800px)   {
    border-top: 1px solid #eaeaea;
    border-bottom: 1px solid #eaeaea;
    display: flex;
    flex-direction: row;
}
`
//nav-column-left 
const NavColumnLeft = styled.div`
width: 20%;
`
//nav-column-center
const NavColumnCenter = styled.div`
text-align: center;
width: 60%;
position: relative;
`
//nav-column-right
const NavColumnRight = styled.div`
width: 20%;
text-align: right;
`

const UserOptions = styled.div`
    justify-content: end;
    font-family: 'Inter', sans-serif;
    display: grid;
    grid-gap: 0px;
    grid-template-columns: 70px 70px 30px 30px;
    font-weight: bold;
`
const Option = styled.div`
    margin-right: 10px;
    font-size: 0.8rem;
    padding: 0;
    padding-top: 6px;

    a {
        text-decoration: none;
        color: inherit;
    }
`
const OptionCart = styled.div`
    margin-right: 10px;
    font-size: 1.25rem;
    padding: 0;
    padding-top: 0px;
    cursor: pointer
`
