import styled from 'styled-components';

import Link from 'next/link';
import { Image } from 'cloudinary-react';

import {
    ProductData,
    PlaceHolder,
    ProductBrand,
    ProductName,
    ProductPrice
} from '../components/styles/Product';

import formatMoney from '../lib/formatMoney';
import { cloudName } from '../config';

export default function ProductStrip({ products }) {

    return (
        <ArrivalsStrip>
            {
                products.map(product => {
                    return (
                        <Link key={product.id + 'strip'} href={"/product/" + product.slug}>
                            <a style={{ textDecoration: 'none' }}>
                                <PlaceHolder>
                                    <Image
                                        cloudName={cloudName}
                                        publicId={product.mainImage.image.public_id}
                                        width="500"
                                        crop="scale" />
                                    <ProductData>
                                        <ProductBrand>{product.brand.description}</ProductBrand>
                                        <ProductName>{product.name}</ProductName>
                                        <ProductPrice>{formatMoney(product.prices[0].prices[0].price)}</ProductPrice>
                                    </ProductData>
                                </PlaceHolder>
                            </a>
                        </Link>
                    )
                })
            }
        </ArrivalsStrip>
    )
}

const ArrivalsStrip = styled.div`
    margin-top: 1.0rem;
    margin-bottom: 1.0rem;
    display: grid;
    grid-gap: 0px;
    max-width: none;

    grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));

    @media only screen and (min-width: 1400px) {

    }

    @media only screen and (min-width: 1049px) and (max-width: 1298px) {
        a:nth-child(5) {
            visibility: hidden;
            display: none;
        }
        a:nth-child(6) {
            visibility: hidden;
            display: none;
        }
    }

    @media only screen and (min-width: 1299px) and (max-width: 1426px) {
        a:nth-child(6) {
            visibility: hidden;
            display: none;
        }
    }

    @media only screen and (min-width: 1427px) and (max-width: 1676px) {
        a:nth-child(6) {
            visibility: hidden;
            display: none;
        }
    }

    img {
        width: 100%;
    }
`
