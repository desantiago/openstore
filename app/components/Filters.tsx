import { useContext } from 'react';
import { FiltersSelected, FilterOption } from './styles/FiltersSelected';
import ProductsContext from '../lib/productsContext';
import { useCategories } from '../lib/categoriesProvider';

export default function Filters() {
    const {
        aggregationsCategory,
        loadingAggregations,
        brands,
        colors,
        sizes,
        setBrands,
        setColors,
        setSizes,

    }: any = useContext(ProductsContext);

    // const {
    //     brands,
    //     colors,
    //     sizes,
    //     setBrands,
    //     setColors,
    //     setSizes,
    // }: any = useCategories();

    if (loadingAggregations) return <div></div>

    const { brands: brandsAgg, colors: colorsAgg, sizes: sizesAgg } = aggregationsCategory;

    const selectedBrands = brandsAgg.filter(agg => brands.includes(agg.slug));
    const selectedSizes = sizesAgg.filter(agg => sizes.includes(agg));
    const selectedColors = colorsAgg.filter(agg => colors.includes(agg.key));

    const removeBrand = (slug: string) => {
        const index = brands.indexOf(slug);
        if (index >= 0) {
            brands.splice(index, 1);
            setBrands([...brands]);
        }
    }

    const removeSize = (size: string) => {
        const index = sizes.indexOf(size);
        if (index >= 0) {
            sizes.splice(index, 1);
            setSizes([...sizes]);
        }
    }

    const removeColor = (key: string) => {
        const index = colors.indexOf(key);
        if (index >= 0) {
            colors.splice(index, 1);
            setColors([...colors]);
        }
    }

    return (
        <FiltersSelected >
            {
                selectedBrands.map(brand => {
                    return (
                        <FilterOption key={brand.slug}>
                            {brand.description}
                            <a style={{ cursor: "pointer" }} onClick={() => removeBrand(brand.slug)}>
                                <i className="lni lni-cross-circle"></i>
                            </a>
                        </FilterOption>
                    )
                })
            }
            {
                selectedSizes.map(size => {
                    return (
                        <FilterOption key={`size${size}`}>
                            {size}
                            <a style={{ cursor: "pointer" }} onClick={() => removeSize(size)}>
                                <i className="lni lni-cross-circle"></i>
                            </a>
                        </FilterOption>
                    )
                })
            }
            {
                selectedColors.map(color => {
                    return (
                        <FilterOption key={color.key}>
                            {color.name}
                            <a style={{ cursor: "pointer" }} onClick={() => removeColor(color.key)}>
                                <i className="lni lni-cross-circle"></i>
                            </a>
                        </FilterOption>
                    )
                })
            }

            {/* <FilterOption href="#">Pumps <i className="lni lni-cross-circle"></i></FilterOption>
            <FilterOption href="#">11 <i className="lni lni-cross-circle"></i></FilterOption>
            <FilterOption href="#">Clear All</FilterOption> */}
        </FiltersSelected>
    )
}