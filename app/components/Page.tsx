import { createGlobalStyle } from 'styled-components'
import Footer from './Footer';
import Header from "./Header";

const GlobalStyles = createGlobalStyle`
html {
    --red: #ff0000;
    --black: #ff0000;
    --grey: #3A33A3;
    --main-font-color: #494949;
    --main-color: #EF3B68;
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing:inherit
}
body {
    padding:0;
    margin:0;
    overflow-x: hidden;
}
* {
    padding: 0;
    margin: 0;
    border: 0;
}
`

export default function Page({ children }) {
    return <div >
        <GlobalStyles />
        <Header></Header>
        {children}
        <Footer></Footer>
    </div>
}