import { useContext, useState } from 'react';
import styled from 'styled-components'
import { useForm } from '../lib/useForm';
import Form from './styles/Form';
import { saveToken } from '../lib/storage';
import { CURRENT_USER_QUERY } from './User';
import { authContext } from '../lib/auth';

const Error = styled.div`
    font-family: Helvetica;
    font-weight: bold;
    padding: 5px;
    border: 1px solid red;
    margin-bottom: 1.0rem;
`

export default function SignIn() {
    const {
        signUp
    }: any = useContext(authContext);

    const [signError, setSignError] = useState(false)
    const { inputs, handleChange } = useForm({
        email: '',
        password: '',
        name: '',
    })

    const handleSubmit = async (e) => {
        e.preventDefault();
        // console.log(inputs);
        try {
            // const res = await signin();
            // console.log(res);
            // setSignError(false);
            // saveToken(res.data.signIn.token);
            // setAuthToken(res.data.signIn.token)

            const token = signUp(inputs.name, inputs.email, inputs.password);
            saveToken(token);
            setSignError(false);
        }
        catch (err) {
            //console.log(err);
            setSignError(true);
        }
    }

    return (
        <Form type="POST" onSubmit={handleSubmit}>
            <fieldset>
                {
                    signError && <Error>There has benn an error, please check your credentials and try again</Error>
                }
                <label htmlFor="name">
                    Name
                    <input
                        type="text"
                        name="name"
                        placeholder="name"
                        value={inputs.name}
                        onChange={handleChange}
                    />
                </label>
                <label htmlFor="email">
                    Email
                    <input
                        type="text"
                        name="email"
                        placeholder="Email"
                        value={inputs.email}
                        onChange={handleChange}
                    />
                </label>
                <label htmlFor="password">
                    Password
                    <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={inputs.password}
                        onChange={handleChange}
                    />
                </label>
                <button type="submit">Sign Up</button>
            </fieldset>
        </Form>
    )
}