import styled from 'styled-components';
import { useUser } from './User';
import formatMoney from '../lib/formatMoney';
import { useCart, CartState } from '../lib/cartContext';
import RemoveFromCartButton from './RemoveFromCartButton';
import Link from 'next/link';
import { Image } from 'cloudinary-react';
import { cloudName } from '../config';

export default function Cart() {
    const me = useUser();
    const { cartOpen, closeCart } = useCart();

    if (!me) return null;
    // console.log('In Cart', me, cartOpen);

    const getTotal = (): number => {
        return me.cartItems.reduce((total, cartItem) => {
            total += cartItem.total;
            return total;
        }, 0);
    }

    return (
        <CartStyles open={cartOpen}>
            <header>
                <CartName>Cart : {me.name}</CartName>
                <button onClick={closeCart}>Close</button>
            </header>
            <ul>
                {
                    me.cartItems.map(cartItem => {
                        const { id, product, price, quantity, size, color, total } = cartItem;
                        return (
                            <CartItem key={id}>
                                {/* <img src={'http://127.0.0.1:8080/' + product.images[0].images[0].filename}></img> */}
                                <Image
                                    cloudName={cloudName}
                                    publicId={product.thumbnail.image.public_id}
                                    width="80"
                                    crop="scale" />

                                <div>
                                    <CartBrand>{product.brand.description}</CartBrand>
                                    <CartProductName>{product.name}</CartProductName>
                                    <Divider />
                                    <CartProductDetails><strong>Size:</strong> {size} </CartProductDetails>
                                    <CartProductDetails><strong>Color:</strong> {color.name}</CartProductDetails>
                                    <Divider />
                                    <CartProductDetails><strong>Qty:</strong> {quantity} <Total>{formatMoney(price)}</Total></CartProductDetails>
                                </div>
                                <div>
                                    <RemoveFromCartButton cartItemKey={id} />
                                </div>
                            </CartItem>
                        )
                    })
                }
            </ul>
            <footer>
                <p>Total : {formatMoney(getTotal())}</p>
                <p style={{ textAlign: 'right' }}>
                    <Link href="/checkout">
                        <CheckoutButton>
                            Checkout
                        </CheckoutButton>
                    </Link>
                </p>
            </footer>
        </CartStyles>
    )
}

const CartName = styled.span`
    font-family: 'Inter', sans-serif;
    font-size: 1.5rem;
`

const CartBrand = styled.p`
    font-family: 'Barlow Condensed', sans-serif;
    font-size: 1.0rem;
`
const CartProductName = styled.p`
    font-size: 0.9rem;
`
const CartProductDetails = styled.p`
    font-size: 0.8rem;
`
const Divider = styled.div`
    margin-bottom: 0.35rem;
`
const Total = styled.span`
    font-weight: bold;
    margin-left: 10.0rem;
    font-size: 1.0rem;
`

const CheckoutButton = styled.a`
    color: white;
    background-color: #EF3B68;
    font-weifht: bold;
    padding: 0.25rem 2.0rem;
    font-size: 1.5rem;
    border-radius: 3px;    
`

// const RemoveButton = styled.div`
//     cursor: pointer;
//     font-size: 2.0rem;
//     margin-right: 1.0rem;
//     margin-top: 1.0rem;
// `

const CartItem = styled.li`
    font-family: 'Inter', sans-serif;
    border-bottom: 1px solid #eaeaea;
    padding: 1rem 0;

    display: grid;
    grid-template-columns: auto 1fr auto;
    
    img {
        width: 80px;
        height: auto;
        margin-right: 1rem;
    }
    h3,
    div {
        
    }
`;

const CartStyles = styled.div`
    padding: 20px;
    position: relative;
    background: white;
    position: fixed;
    height: 100%;
    top: 0;
    right: 0;
    width: 40%;
    min-width: 500px;
    bottom: 0;
    transform: translateX(100%);
    transition: all 0.3s;
    box-shadow: 0 0 10px 3px rgba(0, 0, 0, 0.2);
    z-index: 5;
    display: grid;
    grid-template-rows: auto 1fr auto;

    ${(props) => props.open && `transform: translateX(0);`};

    header {
        border-bottom: 2px solid #EF3B68;
        margin-bottom: 2rem;
        padding-bottom: 1rem;
    }
    footer {
        font-family: 'Inter', sans-serif;
        margin-top: 1rem;
        padding-top: 1rem;
        display: grid;
        grid-template-columns: auto auto;
        justify-content: top;
        font-size: 2rem;
        font-weight: 900;
        p {
            margin: 0;
        }
    }
    ul {
        margin: 0;
        padding: 0;
        list-style: none;
        overflow: scroll;
    }
`;
