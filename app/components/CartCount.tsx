import styled from 'styled-components';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

const AnimationStyles = styled.span`
  position: relative;
  .count {
    display: block;
    position: relative;
    transition: transform 0.4s;
    backface-visibility: hidden;
  }
  .count-enter {
    transform: scale(4) rotateX(0.5turn);
  }
  .count-enter-active {
    transform: rotateX(0);
  }
  .count-exit {
    top: 0;
    position: absolute;
    transform: rotateX(0);
  }
  .count-exit-active {
    transform: scale(4) rotateX(0.5turn);
  }
`;

// display: inline-block;
// font-size: 0.8rem;
// background: #EF3B68;
// color: white;
// border-radius: 50%;
// padding: 0.5rem;
// line-height: 2rem;
// min-width: 1rem;
// margin-left: 1rem;
// font-feature-settings: 'tnum';
// font-variant-numeric: tabular-nums;

const Badge = styled.div`
    display: inline-block;
    font-size: 1.0rem;
    color: #EF3B68;
    margin-left: 0.25rem;
    font-feature-settings: 'tnum';
    font-variant-numeric: tabular-nums;
    font-weight: bolder;
`

export default function CartCount({ count }) {
    // return (
    //     <Badge>{count}</Badge>
    // )
    return (
        <AnimationStyles>
            <TransitionGroup>
                <CSSTransition
                    unmountOnExit
                    className="count"
                    classNames="count"
                    key={count}
                    timeout={{ enter: 400, exit: 400 }}
                >
                    <Badge> {count}</Badge>
                </CSSTransition>
            </TransitionGroup>
        </AnimationStyles>
    )
}