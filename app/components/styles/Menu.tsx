import styled from 'styled-components';

const MenuToogle = styled.div`
display: inline-block;
position: relative;
top: -45px;
left: 1.0rem;

z-index: 1;

-webkit-user-select: none;
user-select: none;

@media only screen and (min-width: 800px) {
    display:none;
}
`
//#menuToggle a
const Link = styled.a`
    text-decoration: none;
    color: #232323;

    transition: color 0.3s ease;
`
//#menuToggle input
const Input = styled.input`
    display: block;
    width: 40px;
    height: 32px;
    position: absolute;
    top: -7px;
    left: -5px;

    cursor: pointer;

    opacity: 0;
    z-index: 2;

    -webkit-touch-callout: none;

    /* 
        * Transform all the slices of hamburger
        * into a crossmark.
        */
    &:checked~span {
        opacity: 1;
        transform: rotate(45deg) translate(-2px, -1px);
        background: #232323;
    }
    
    /*
        * But let's hide the middle one.
        */
    &:checked~span:nth-last-child(3) {
        opacity: 0;
        transform: rotate(0deg) scale(0.2, 0.2);
    }
    
    /*
        * Ohyeah and the last one should go the other direction
        */
    &:checked~span:nth-last-child(2) {
        transform: rotate(-45deg) translate(0, -1px);
    }
    
    &:checked~div {
        transform: none;
    }
`

//#menuToggle span {
const Span = styled.span`
    display: block;
    width: 33px;
    height: 4px;
    margin-bottom: 5px;
    position: relative;

    background: #cdcdcd;
    border-radius: 3px;

    z-index: 1;

    transform-origin: 4px 0px;

    transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1.0),
        background 0.5s cubic-bezier(0.77, 0.2, 0.05, 1.0),
        opacity 0.55s ease;


    &:first-child {
        transform-origin: 0% 0%;
    }
    
    &:nth-last-child(2) {
        transform-origin: 0% 100%;
    }    
`

//#menu
const MenuCont = styled.div`
    position: absolute;
    width: 250px;
    min-height: 100vh;
    margin: -100px 0 0 -50px;
    padding: 50px;
    padding-top: 100px;

    background: #fff;
    border-right: 1px solid #eaeaea;

    -webkit-font-smoothing: antialiased;
    transform-origin: 0% 0%;
    transform: translate(-100%, 0);
    transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1.0);
`

//#menu ul
const MenuList = styled.ul`
    list-style-type: none;
    font-family: 'Inter', sans-serif;
    font-size: 0.85rem;
`

//#menu li
const MenuOption = styled.li`
    padding: 5px 0px 3px 0px;
`

//#menu h3
const MenuTitle = styled.h3`
    font-family: 'M PLUS 1p', sans-serif;
    font-size: 0.9rem;
    margin-top: 1.5rem;
    margin-bottom: 0.5rem;
    text-transform: uppercase;
    color: #494949;
`

export default function Menu() {

    return (
        <MenuToogle>
            {/* A fake / hidden checkbox is used as click reciever,
            so you can use the :checked selector on it. */}
            <Input type="checkbox" />

            {/* Some spans to act as a hamburger.
            
            They are acting like a real hamburger,
            not that McDonalds stuff. */}
            <Span></Span>
            <Span></Span>
            <Span></Span>

            {/* Too bad the menu has to be inside of the button
            but hey, it's pure CSS magic. */}
            <MenuCont>
                <MenuTitle>Shoes</MenuTitle>
                <MenuList>
                    <Link href="#">
                        <MenuOption>Boots & Booties</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Flats</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Pumps</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Sandals</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Slides</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Slingbacks</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Sneakers</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Wedges</MenuOption>
                    </Link>
                </MenuList>

                <MenuTitle>Bags</MenuTitle>
                <MenuList>
                    <Link href="#">
                        <MenuOption>Backpacks</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Belt Bags</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Clutch Bags</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Cross Body Bags</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Evening Bags</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Luggage and Travel</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Mini Bags</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Shoulder Bags</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Tote Bags</MenuOption>
                    </Link>
                </MenuList>

                <MenuTitle>SUPPORT</MenuTitle>
                <MenuList>
                    <Link href="#">
                        <MenuOption>Contact Us</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Help & FAQ</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Shipping Policy</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Returns & Exchanges</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>Order Status & Tracking</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>International Shipping</MenuOption>
                    </Link>
                </MenuList>

                <MenuTitle>BROWSE</MenuTitle>
                <MenuList>
                    <Link href="#">
                        <MenuOption>Brands</MenuOption>
                    </Link>
                    <Link href="#">
                        <MenuOption>New Arrivals</MenuOption>
                    </Link>
                </MenuList>

            </MenuCont>
        </MenuToogle>
    )
}