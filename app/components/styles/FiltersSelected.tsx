import styled from 'styled-components';

export const FiltersSelected = styled.div`
  margin-top: 0.5rem;
  font-family: Helvetica, Arial, sans-serif;
  font-size: 0.8rem;
`

export const FilterOption = styled.span`
  color: inherit;
  text-decoration: none;
  margin-left: 2.0rem;
  i {
    margin-left: 0.25rem;
    font-weight: bold;
  }
`
