import styled from 'styled-components';

//.filter-container
export const FilterContainer = styled.div`
  font-family: 'Inter', sans-serif;
  padding: 0 1.0rem;
`

// .filter
export const Filter = styled.div`
  margin-bottom: 2.0rem;
  font-size: 0.9rem;
`
export const CategoryOptions = styled.ul`
  text-transform: uppercase;
  list-style: none;
  margin-top: 1.0rem;
`
// .filter h3
export const FilterTitle = styled.h3`
  font-family: 'M PLUS 1p', sans-serif;
  font-size: 0.9rem;
  margin-bottom: 0.5rem;
  text-transform: uppercase;
`
// .filter ul
export const FilterList = styled.ul`
  list-style: none;
`
// .filter li
export const Li = styled.li`
  margin-bottom: 0.2rem;
`
export const CheckBox = styled.label`
  display: inline-flex;
  cursor: pointer;
  position: relative;

  &.disabled {
    cursor: default;
    color: #a1a1a1;
  }
`

export const Text = styled.span`
  padding: 0 0 0 0.5rem;
`

export const Input = styled.input`
  height: 13px;
  width: 13px;
  -webkit-appearance: none;
  -moz-appearance: none;
  -o-appearance: none;
  appearance: none;
  border: 1px solid #b1b1b1;
  border-radius: 0px;
  outline: none;
  transition-duration: 0.3s;
  background-color: #fff;
  cursor: pointer;
  margin-top: 1px;

  &:active {
    border: 1px solid #34495E;
  }

  &:checked {
      border: 1px solid #EF3B68;
      background-color: #EF3B68;    
  }
`

export const FilterPH = styled.div`
    margin-top: 1.0rem;
    margin-bottom: 2.0rem;
    font-size: 0.9rem;
`
export const FilterTitlePH = styled.h3`
    font-size: 0.9rem;
    margin-bottom: 0.5rem;
    background-color: #f1f1f1;
`
export const FilterListPH = styled.ul`
`
export const LiPH = styled.li`
    margin-bottom: 0.2rem;
    background-color: #fafafa;
    list-style: none;
`
