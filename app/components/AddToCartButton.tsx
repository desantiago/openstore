import { gql, useMutation } from '@apollo/client';
import styled from 'styled-components';
import { useCart } from '../lib/cartContext';
import { CURRENT_USER_QUERY } from './User';

const ADD_TO_CART_MUTATION = gql`
    mutation ADD_TO_CART_MUTATION($quantity: Int!, $color: ID!, $size: String!, $productKey: ID!) {
        addCartItem(quantity: $quantity, color: $color, size: $size, productKey: $productKey) {
            key
        }
    }
`

export default function AddToCartButton({ productKey }) {
    const { colorSelected, sizeSelected } = useCart();

    const [addToCart, { loading }] = useMutation(ADD_TO_CART_MUTATION, {
        variables: {
            quantity: 1,
            color: colorSelected,
            size: sizeSelected,
            productKey,
        },
        refetchQueries: [{ query: CURRENT_USER_QUERY }]
    });

    const onAddToCart = () => {
        console.log("addToCart ", {
            quantity: 1,
            color: colorSelected,
            size: sizeSelected,
            productKey,
        });
        addToCart();
    }

    return (
        <AddButton type="button" disabled={loading} onClick={onAddToCart}>
            <i className="lni lni-briefcase"></i> &nbsp; Add To Cart
        </AddButton>
    )
}

const AddButton = styled.button`
    margin-top: 1.0rem;
    background-color: #494949;
    color: white;
    font-family: Arial, Helvetica, sans-serif;
    font-weight: bold;
    padding: 0.25rem 2.0rem 0.25rem 2.0rem;
    font-family: 'Inter', sans-serif;
`
