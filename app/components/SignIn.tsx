import { useContext, useState } from 'react';
import { gql, useMutation } from '@apollo/client';
import styled from 'styled-components'
import { useForm } from '../lib/useForm';
import Form from './styles/Form';
import { saveToken } from '../lib/storage';
import { CURRENT_USER_QUERY } from './User';
import { authContext } from '../lib/auth';

const Error = styled.div`
    font-family: Helvetica;
    font-weight: bold;
    padding: 5px;
    border: 1px solid red;
    margin-bottom: 1.0rem;
`

const SIGNIN_MUTATION = gql`
    mutation SIGNIN_MUTATION($email: String!, $password: String!) {
        signIn(email: $email, password: $password) {
            token
        }
    }
`

export default function SignIn() {
    const {
        signIn
    }: any = useContext(authContext);

    const [signError, setSignError] = useState(false)
    const { inputs, handleChange } = useForm({
        email: '',
        password: ''
    })

    // const [signin, { data, error, loading }] = useMutation(SIGNIN_MUTATION, {
    //     variables: inputs,
    //     refetchQueries: [{ query: CURRENT_USER_QUERY }]
    // });

    const handleSubmit = async (e) => {
        e.preventDefault();
        // console.log(inputs);
        try {
            // const res = await signin();
            // console.log(res);
            // setSignError(false);
            // saveToken(res.data.signIn.token);
            // setAuthToken(res.data.signIn.token)

            const token = await signIn(inputs.email, inputs.password);
            console.log("Token --- ", token);
            saveToken(token);
            setSignError(false);
        }
        catch (err) {
            //console.log(err);
            setSignError(true);
        }
    }

    return (
        <Form type="POST" onSubmit={handleSubmit}>
            <fieldset>
                {
                    signError && <Error>There has benn an error, please check your credentials and try again</Error>
                }
                <label htmlFor="email">
                    Email
                        <input
                        type="text"
                        name="email"
                        placeholder="Email"
                        value={inputs.email}
                        onChange={handleChange}
                    />
                </label>
                <label htmlFor="password">
                    Password
                        <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={inputs.password}
                        onChange={handleChange}
                    />
                </label>
                <button type="submit">Sign In</button>
            </fieldset>
        </Form>
    )
}