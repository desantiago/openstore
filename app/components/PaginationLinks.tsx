import Link from 'next/link';
import { Pagination, PaginationList, PaginationPage } from './styles/Pagination';
import { perPage } from '../config';

interface PaginationData {
    pages: number[];
    prevPage: number;
    nextPage: number;
}

// export default function PaginationLinks({ currentPage, category, subCategory, numProducts }) {
//     // console.log('PaginationLinks ', currentPage, category, subCategory, numProducts);
//     const numPages = Math.ceil(numProducts / perPage);

//     const {
//         page,
//         setPage,
//     }: any = useContext(ProductsContext);

//     // const getPages = () => {
//     //     let pages = [];
//     //     for (let i = 1; i <= numPages; i++) {
//     //         pages.push(i);
//     //     }
//     //     return pages;    
//     // }

//     const getPages = (): PaginationData => {
//         let pages = [];
//         let prevPage = 0;
//         let nextPage = 0;
//         if (currentPage === 1) {
//             let i = 1;
//             while (i <= 3 && i <= numPages) {
//                 pages.push(i);
//                 i++;
//             }
//             prevPage = 1;
//             nextPage = numPages >= 2 ? 2 : 1;
//         }
//         else if (currentPage === numPages) {
//             let i = numPages;
//             while (pages.length < 3 && i >= 1) {
//                 pages.unshift(i);
//                 i--;
//             }
//             prevPage = numPages >= 2 ? currentPage - 1 : 1;
//             nextPage = numPages;
//         }
//         else {
//             prevPage = currentPage - 1;
//             nextPage = currentPage + 1;

//             pages.push(prevPage);
//             pages.push(currentPage);
//             pages.push(nextPage);
//         }
//         return {
//             pages,
//             prevPage,
//             nextPage
//         };
//     }

//     const { pages, prevPage, nextPage } = getPages();
//     // console.log('pagination', pages, prevPage, nextPage);
//     // const baseURL = subCategory ? `/products/${category}/${subCategory}` : `/products/${category}`;

//     const click = (page: number): void => {
//         setPage(page);
//     }

//     return (
//         <Pagination>
//             <PaginationList>
//                 <PaginationPage>
//                     <a aria-disabled={currentPage === 1 ? true : false} onClick={() => click(1)}>
//                         <i className="lni lni-arrow-left-circle"></i>
//                     </a>
//                 </PaginationPage>
//                 <PaginationPage>
//                     <a aria-disabled={currentPage === 1 ? true : false} onClick={() => click(prevPage)}>
//                         <i className="lni lni-arrow-left"></i>
//                     </a>
//                 </PaginationPage>

//                 {
//                     pages.map(page => {
//                         return (
//                             <PaginationPage className={page === currentPage ? 'selected' : ''} key={`page${page}`}>
//                                 <a aria-disabled={true} onClick={() => click(page)}>{page}</a>
//                             </PaginationPage>
//                         )
//                     })
//                 }

//                 <PaginationPage>
//                     <a aria-disabled={currentPage === numPages ? true : false} onClick={() => click(nextPage)}>
//                         <i className="lni lni-arrow-right"></i>
//                     </a>
//                 </PaginationPage>
//                 <PaginationPage>
//                     <a aria-disabled={currentPage === numPages ? true : false} onClick={() => click(numPages)}>
//                         <i className="lni lni-arrow-right-circle"></i>
//                     </a>
//                 </PaginationPage>
//             </PaginationList>
//         </Pagination>
//     )
// }

export default function PaginationLinks({ currentPage, category, subCategory, brand, numProducts }) {
    // console.log('PaginationLinks ', currentPage, category, subCategory, numProducts);
    const numPages = Math.ceil(numProducts / perPage);

    // const getPages = () => {
    //     let pages = [];
    //     for (let i = 1; i <= numPages; i++) {
    //         pages.push(i);
    //     }
    //     return pages;    
    // }

    const getPages = (): PaginationData => {
        let pages = [];
        let prevPage = 0;
        let nextPage = 0;
        if (currentPage === 1) {
            let i = 1;
            while (i <= 3 && i <= numPages) {
                pages.push(i);
                i++;
            }
            prevPage = 1;
            nextPage = numPages >= 2 ? 2 : 1;
        }
        else if (currentPage === numPages) {
            let i = numPages;
            while (pages.length < 3 && i >= 1) {
                pages.unshift(i);
                i--;
            }
            prevPage = numPages >= 2 ? currentPage - 1 : 1;
            nextPage = numPages;
        }
        else {
            prevPage = currentPage - 1;
            nextPage = currentPage + 1;

            pages.push(prevPage);
            pages.push(currentPage);
            pages.push(nextPage);
        }
        return {
            pages,
            prevPage,
            nextPage
        };
    }

    const { pages, prevPage, nextPage } = getPages();
    // console.log('pagination', pages, prevPage, nextPage);
    const baseURL = subCategory ? `/products/${category}/${subCategory}` : category ? `/products/${category}` : `/products/${brand}`;

    return (
        <Pagination>
            <PaginationList>
                <PaginationPage>
                    <Link href={`${baseURL}/1`}>
                        <a aria-disabled={currentPage === 1 ? true : false}><i className="lni lni-arrow-left-circle"></i></a>
                    </Link>
                </PaginationPage>
                <PaginationPage>
                    <Link href={`${baseURL}/${prevPage}`}>
                        <a aria-disabled={currentPage === 1 ? true : false}><i className="lni lni-arrow-left"></i></a>
                    </Link>
                </PaginationPage>

                {
                    pages.map(page => {
                        return (
                            <PaginationPage className={page === currentPage ? 'selected' : ''} key={`page${page}`}>
                                <Link href={`${baseURL}/${page}`}>
                                    <a aria-disabled={true}>{page}</a>
                                </Link>
                            </PaginationPage>
                        )
                    })
                }

                <PaginationPage>
                    <Link href={`${baseURL}/${nextPage}`}>
                        <a aria-disabled={currentPage === numPages ? true : false}><i className="lni lni-arrow-right"></i></a>
                    </Link>
                </PaginationPage>
                <PaginationPage>
                    <Link href={`${baseURL}/${numPages}`}>
                        <a aria-disabled={currentPage === numPages ? true : false}><i className="lni lni-arrow-right-circle"></i></a>
                    </Link>
                </PaginationPage>
            </PaginationList>
        </Pagination>
    )
}