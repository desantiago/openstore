import { ChangeEvent, useState, useEffect } from "react";
import { gql, useMutation } from "@apollo/client";

import { useForm, FormFields, File } from "../lib/useForm"
// import { useFormFile, FormFieldsFile } from "../lib/useFormFile"
import Form from "../components/styles/Form";
import styled from 'styled-components';

const CREATE_PRODUCT_MUTATION = gql`
    mutation CREATE_PRODUCT_MUTATION(
        $name: String!
        $description: String!
        $sizeAndFit: String
        $details: String
        $brand: String!
        $category: String!
        $subCategory: String!
        $colors: [String],
        $images: [ImagesInput]
        $sizes: [SizesInput]
        $prices: [PricesInput]
    ) {
        createProduct(input: {
            key: ""
            name: $name
            description: $description
            sizeAndFit: $sizeAndFit
            details: $details
            timeStamp: ""
            brand: $brand
            category: $category
            subCategory: $subCategory
            colors: $colors
            images: $images
            sizes: $sizes
            prices: $prices
        }) {
            key
        }
    }    
`

// const UPLOAD_FILE = gql`
//     mutation SingleUpload($file: Upload!) {
//         singleUpload(file: $file) {
//             filename
//             mimetype
//             encoding
//         }
//     }
// `;

const UPLOAD_FILES = gql`
    mutation multiUpload($files: Upload!) {
        multiUpload(files: $files) {
            key
            filename
            mimetype
            encoding
        }
    }
`;

const ImageList = styled.ul`
    list-style-type: none;

    li {
        display: inline-block
    }
    img {
        width: 150px; 
        height:auto;
    }
`

const MainWraper = styled.div`
    display:block;

    @media only screen and (min-width: 800px)   {
        display: grid;
        grid-gap: 0px;
        grid-template-columns: 400px auto auto;
        grid-template-areas:
        "sidebar content images"
    }
`
const SideBar = styled.aside`
    display:none;
    @media only screen and (min-width: 800px)   {
        display: block;
        grid-area: sidebar;
    }
`;

const Content = styled.main`
    grid-area: content;
`
const ImageContainer = styled.div`
    grid-area: images;
`

const ColorOptions = styled.div`
    margin-top: 0.5rem;
    display: grid;
    grid-gap: 0px;
    grid-template-columns: repeat(auto-fill, minmax(70px, 1fr));
    max-width: none;
`

const ColorOption = styled.div`
    width: auto;
    height: 40px;
    background-color: #efefef;
    color: #494949;
    font-weight: normal;
    line-height: 40px;
    font-family: Helvetica, sans-serif;
    font-size: 0.8rem;
    text-align: center;
    margin-bottom: 0.2rem;
    margin-right: 0.1rem;
    padding: 0 0.1rem;
    white-space: nowrap;
    cursor: pointer;

    &.selected { 
        background-color:#EF3B68;
        color: #fff;
    }
`

const ColorSelected = styled(ColorOption)`
    border: 1px solid #efefef;
    background-color: #fff;
    color: #494949;

    &.selected { 
        background-color: #efefef;
        color: #494949;
        font-weight: bolder;
    }
`
// grid-template-columns: 100px 100px 50px;
// grid-template-areas:
// "size price remove"

const SizeGrid = styled.div`
    display: grid;
    grid-gap: 2px;
    grid-template-columns: 50px 100px 50px;
`

const SizeCont = styled.div`
    padding-top: 0.25rem;
`
const PriceCont = styled.div`
    justify-self: center;
`
const RemoveCont = styled.div`
    padding-top: 0.25rem;
    justify-self: center;
`
const RemoveImage = styled.div`
    position: absolute;
    top: 0;
    right: 0;

    a {
        text-decoration: none;
        color: var(--main-color);
        font-size: 2.0rem;
    }
`
/*
brand: "0e66c79a-2d17-479a-bbd9-33c44ba8efaa"
category: ""
colors: Array(1)
    0: "c1df1583-bad0-43fb-9193-d46d2db43552"
description: "aaa"
details: "dd"
endSize: "11"
images: Array(1)
    0:
    color: "c1df1583-bad0-43fb-9193-d46d2db43552"
    images: Array(3)
        0: "b6bafe92-fbe4-4949-b39b-10c57fead4be"
        1: "6c1101a8-7c72-4113-8383-ad454a2fc1b3"
        2: "8765fa28-d90c-4e71-8248-7a67cb34fdd9"
name: "moda operandi"
price: "100"
prices: Array(1)
    0:
        color: "c1df1583-bad0-43fb-9193-d46d2db43552"
        prices: Array(7)
            0: {size: "5", price: 100}
            1: {size: "6", price: 100}
            2: {size: "7", price: 100}
            3: {size: "8", price: 100}
            4: {size: "9", price: 100}
            5: {size: "10", price: 100}
            6: {size: "11", price: 100}
sizeAndFit: "ss"
sizes: Array(1)
    0:
        color: "c1df1583-bad0-43fb-9193-d46d2db43552"
        sizes: (7) ["5", "6", "7", "8", "9", "10", "11"]
startSize: "5"
subCategory: "dbda4264-4469-4e63-9c4c-037a9fa6d44c"
*/

function CreateProduct({ categories, brands, colors }) {
    const initialValue: FormFields = {
        name: '',
        price: 0,
        description: '',
        sizeAndFit: '',
        details: '',
        colors: [],
        images: [],
        prices: [],
        sizes: [],
        brand: brands[0].key,
        category: categories[0].key,
        subCategory: categories[0].subcategories[0].key,
        startSize: '',
        endSize: ''
    };

    const [images, setImages] = useState<File[]>([]);
    const { inputs,
        handleChange,
        clearForm,
        setColor,
        updatePrice,
        updateSizes,
        removeImageFromList,
        removeSizeFromList,
        changePrice,
        addImages } = useForm(initialValue);

    // const [createProduct, { loading, error, data }] = useMutation(CREATE_PRODUCT_MUTATION, {
    //     variables: inputs
    // });
    const [createProduct, { loading, error, data }] = useMutation(CREATE_PRODUCT_MUTATION);

    // const [uploadFile, { loading: loadingFile, error: errorFile, data: dataFile }] = useMutation(UPLOAD_FILE);
    const [uploadFiles] = useMutation(UPLOAD_FILES);
    const [colorsSelected, setColorsSelected] = useState([]);
    const [colorSelected, setColorSelected] = useState('');

    // const addRemoveColor = (colorKey) => {
    //     var index = colorsSelected.findIndex(color => color.key === colorKey);
    //     if (index !== -1) {
    //         colorsSelected.splice(index, 1);
    //         if (!colorsSelected.some(color => color.selected)) {
    //             colorsSelected[0].selected = true;
    //         }
    //     }
    //     else {
    //         colorsSelected.forEach(color => {
    //             color.selected = false;
    //         });
    //         colorsSelected.push({
    //             ...colors.find(color => color.key == colorKey),
    //             selected: true,
    //             images: [],
    //             sizes: getSizes()
    //         });
    //         setColorsSelected([...colorsSelected]);
    //     }
    // }

    // const getSizes = () => {
    //     let sizes = [];
    //     if (Number(inputs.startSize) && Number(inputs.endSize)) {
    //         const start = Number(inputs.startSize);
    //         const end = Number(inputs.endSize);
    //         for (let i = start; i <= end; i++) {
    //             sizes.push({
    //                 size: i + '',
    //                 price: Number(inputs.price)
    //             });
    //         }
    //     }
    //     return sizes;
    // }

    // const getSizesColor = (color, start, end) => {
    //     let sizes = [];
    //     for (let i = start; i <= end; i++) {
    //         const prevSize = color.sizes.find(size => size.size === i);
    //         sizes.push({
    //             size: i + '',
    //             price: prevSize ? prevSize.price : inputs.price
    //         });
    //     }
    //     console.log(sizes);
    //     return sizes;
    // }

    // const checkSizes = () => {
    //     if (Number(inputs.startSize) && Number(inputs.endSize)) {
    //         const start = Number(inputs.startSize);
    //         const end = Number(inputs.endSize);

    //         if (start < end) {
    //             console.log("vamos a ir", start, end);

    //             colorsSelected.forEach(color => {
    //                 color.sizes = getSizesColor(color, start, end);
    //             });

    //             setColorsSelected([...colorsSelected]);
    //         }
    //     }
    // }

    // const getPrice = (sizes, price) => {
    //     sizes.forEach(size => {
    //         size.price = price
    //     });
    //     return sizes;
    // }

    // const checkPrices = () => {
    //     if (Number(inputs.price)) {
    //         colorsSelected.forEach(color => {
    //             color.sizes = getPrice(color.sizes, Number(inputs.price));
    //         });
    //     }
    //     setColorsSelected([...colorsSelected]);
    // }

    useEffect(() => {
        //checkSizes()
        updateSizes();
    }, [inputs.startSize, inputs.endSize]);

    useEffect(() => {
        //checkPrices()
        console.log("useEffect", inputs.price);
        updatePrice();
    }, [inputs.price]);

    // when the user clicks on the available colors
    const clickColor = (key) => {
        // console.log("color", key);
        setColor(key);
        setColorSelected(key);
        //addRemoveColor(key);
    }

    // when the user click on the colors, that are going to be available
    // the list above the images
    const clickColorSelected = (key) => {
        setColorSelected(key);
        // for (let i = 0; i < colorsSelected.length; i++) {
        //     const color = colorsSelected[i];
        //     colorsSelected[i].selected = false;
        //     if (color.key === key) {
        //         colorsSelected[i].selected = true;
        //     }
        // }
        // setColorsSelected([...colorsSelected]);
    }

    const onChange = (event: ChangeEvent<HTMLInputElement>): void => {
        const input = event.target as HTMLInputElement;

        if (!input.files?.length) {
            return;
        }

        uploadFiles({ variables: { files: input.files } }).then(({ data }) => {
            // console.log("upload", [...images, ...data.multiUpload]);
            // const index = colorsSelected.findIndex(color => color.selected);
            // console.log("upload", [...colorsSelected[index].images, ...data.multiUpload]);
            // colorsSelected[index].images = [...colorsSelected[index].images, ...data.multiUpload]
            // setColorsSelected([...colorsSelected]);
            setImages(data.multiUpload);
            addImages(colorSelected, data.multiUpload);
        });
    }

    const onChangePrice = (size: string, value: string): void => {
        // const index = colorsSelected.findIndex(color => color.selected);
        // const index2 = colorsSelected[index].sizes.findIndex(s => s.size === size);
        // colorsSelected[index].sizes[index2].price = value;
        // setColorsSelected([...colorsSelected]);
        changePrice(colorSelected, size, Number(value));
    }

    const removeSize = (size: string): void => {
        // const index = colorsSelected.findIndex(color => color.selected);
        // const index2 = colorsSelected[index].sizes.findIndex(s => s.size === size);
        // colorsSelected[index].sizes.splice(index2, 1);
        // setColorsSelected([...colorsSelected]);
        removeSizeFromList(colorSelected, size);
    }

    const removeImage = (imageKey: string): void => {
        // const index = colorsSelected.findIndex(color => color.selected);
        // const index2 = colorsSelected[index].images.findIndex(image => image.key === imageKey);
        // colorsSelected[index].images.splice(index2, 1);
        // setColorsSelected([...colorsSelected]);
        removeImageFromList(colorSelected, imageKey);
    }

    const saveProduct = async () => {
        console.log(inputs);

        // let productData = { ...inputs }
        // colorsSelected.forEach(color => {
        //     //inputs.colors.push(color.key);
        //     productData.images.push({
        //         color: color.key,
        //         images: color.images.map(image => image.key)
        //     });
        //     productData.sizes.push({
        //         color: color.key,
        //         sizes: color.sizes.map(size => size.size)
        //     });
        //     productData.prices.push({
        //         color: color.key,
        //         prices: [...color.sizes]
        //     });
        // });

        // console.log(productData);
        // // productData.colors.push("11111111");

        //inputs.images = inputs.images.map(image => image.)
        // inputs.images.forEach(images => {
        //     images.images = images.images.map(image => image.key);
        // })

        const res = await createProduct({
            variables: inputs,
            // refetchQueries: [{query: PRODUCT_LIST, variables: {}}]
        });

        console.log(res);
    }

    return (
        <>
            <MainWraper>
                <SideBar></SideBar>
                <Content>
                    <Form onSubmit={(e) => {
                        e.preventDefault();
                        saveProduct();
                    }}>
                        <fieldset disabled={loading} aria-busy={loading}>
                            <label htmlFor="name">
                                Name
                                <input
                                    type="text"
                                    value={inputs.name}
                                    id="name" name="name"
                                    placeholder="Name"
                                    onChange={handleChange} />
                            </label>
                            <label htmlFor="description">
                                Description
                                <textarea
                                    value={inputs.description}
                                    id="description"
                                    name="description"
                                    placeholder="Description"
                                    onChange={handleChange} />
                            </label>
                            <label htmlFor="subCategory">
                                Category
                                <select
                                    id="subCategory"
                                    name="subCategory"
                                    value={inputs.subCategory}
                                    onChange={handleChange}>
                                    {
                                        categories && categories.map(category => {
                                            return (
                                                <optgroup label={category.description} key={category.key}>
                                                    {
                                                        category.subcategories.map(subCategory => <option key={subCategory.key} value={subCategory.key}>{subCategory.description}</option>)
                                                    }
                                                </optgroup>
                                            )
                                        })
                                    }
                                </select>
                            </label>

                            <label htmlFor="brand">
                                Brand
                                <select
                                    id="brand"
                                    name="brand"
                                    value={inputs.brand}
                                    onChange={handleChange}
                                >
                                    {
                                        brands && brands.map(brand => <option key={brand.key} value={brand.key}>{brand.description}</option>)
                                    }
                                </select>
                            </label>

                            <label htmlFor="colors">
                                Colors
                                <ColorOptions>
                                    {
                                        colors && colors.map(color => {
                                            return (
                                                <ColorOption
                                                    key={color.key}
                                                    className={inputs.colors.includes(color.key) ? 'selected' : ''}
                                                    onClick={() => clickColor(color.key)}>

                                                    {color.name}
                                                </ColorOption>
                                            )
                                        })
                                    }
                                </ColorOptions>
                            </label>

                            <label htmlFor="startSize">
                                Sizes
                                <ul style={{ listStyleType: 'none' }}>
                                    <li style={{ display: 'inline-block' }}>
                                        <input
                                            type="text"
                                            value={inputs.startSize}
                                            id="startSize"
                                            name="startSize"
                                            placeholder="startSize"
                                            onChange={handleChange} />
                                    </li>
                                    <li style={{ display: 'inline-block' }}>
                                        <input
                                            type="text"
                                            value={inputs.endSize}
                                            id="endSize"
                                            name="endSize"
                                            placeholder="endSize"
                                            onChange={handleChange} />
                                    </li>
                                </ul>
                            </label>

                            <label htmlFor="price">
                                Price
                                <input
                                    type="text"
                                    value={inputs.price}
                                    id="price"
                                    name="price"
                                    placeholder="Price"
                                    onChange={handleChange} />
                            </label>

                            <label htmlFor="details">
                                Details
                                <textarea
                                    value={inputs.details}
                                    id="details"
                                    name="details"
                                    placeholder="Details"
                                    onChange={handleChange} />
                            </label>
                            <label htmlFor="sizeAndFit">
                                Size And Fit
                                <textarea
                                    value={inputs.sizeAndFit}
                                    id="sizeAndFit" name="sizeAndFit"
                                    placeholder="Size And Fit"
                                    onChange={handleChange} />
                            </label>

                            <button type="submit">Add Product</button>
                        </fieldset>
                    </Form>
                </Content>

                <ImageContainer>
                    <Form >
                        <ColorOptions>
                            {
                                inputs.colors.map((color) =>
                                    <ColorSelected
                                        key={color}
                                        onClick={() => clickColorSelected(color)}
                                        className={color === colorSelected ? 'selected' : ''}
                                    >
                                        {colors.find(colorData => colorData.key === color).name}
                                    </ColorSelected>
                                )
                            }
                        </ColorOptions>

                        <fieldset>
                            <label htmlFor="files">
                                Images
                                <input type="file" multiple id="files" name="files" onChange={onChange} />
                            </label>
                        </fieldset>
                        <ImageList>
                            {
                                inputs.images.find((image) => image.color === colorSelected)?.images.map(imageKey => {
                                    const image = images.find(image => image.key === imageKey);
                                    return (
                                        <li key={image.key} style={{ position: 'relative' }}>
                                            <img src={'http://127.0.0.1:8080/' + image.filename} />
                                            <RemoveImage>
                                                <a onClick={() => removeImage(image.key)}><i className="lni lni-cross-circle"></i></a>
                                            </RemoveImage>
                                        </li>
                                    )
                                })
                            }
                        </ImageList>
                        <SizeGrid>
                            {
                                inputs.prices.find((image) => image.color === colorSelected)?.prices.map((price) => {
                                    return (
                                        <>
                                            <SizeCont key={price.size + "-size"}>{price.size}</SizeCont>
                                            <PriceCont key={price.size + "-price"}>
                                                <input type="text" value={price.price} onChange={(e) => onChangePrice(price.size, e.target.value)} />
                                            </PriceCont>
                                            <RemoveCont key={price.size + "-remove"}>
                                                <a onClick={() => removeSize(price.size)}><i className="lni lni-cross-circle"></i></a>
                                            </RemoveCont>
                                        </>
                                    )
                                })
                            }
                        </SizeGrid>
                        <pre className="language-bash">{JSON.stringify(inputs, null, 2)}</pre>
                    </Form>
                </ImageContainer>
            </MainWraper>
        </>
    )
}

export default CreateProduct;
