import categoryLoaders from './dataloaders/categories';
import subCategoryLoaders from './dataloaders/subCategories';
import brandLoaders from './dataloaders/brands';
import colorLoaders from './dataloaders/colors';
import fileLoaders from './dataloaders/files';
import productLoaders from './dataloaders/products';

import testData from './models/TestData';
import {
    Product,
    Category,
    SubCategory,
    Brand,
    Color,
    File,
    User,
    Order,
    OrderItem,
    CartItem,

    ICategory,
} from './models/ProductSchema'

import { Aggregations2, AggregationCategory2 } from './models/types';
import CartItemResolvers from './service/resolvers/cartItem';

var cloudinary = require('cloudinary').v2;

cloudinary.config({
    cloud_name: '',
    api_key: '',
    api_secret: ''
});

// let streamUpload = (fileStream: any): Promise<File> => {
//     return new Promise((resolve, reject) => {
//         let stream = cloudinary.uploader.upload_stream(
//             {
//                 folder: 'products'
//             },
//             (error: any, result: any) => {
//                 if (result) {
//                     resolve(result);
//                 } else {
//                     reject(error);
//                 }
//             }
//         );

//         fileStream.pipe(stream);
//     });
// }

interface Ids {
    [key: string]: string;
}

const data = testData();

const DataLoader = require('dataloader')
//Set up mongoose connection
var mongoose = require('mongoose');
var mongoDB = 'mongodb+srv://admin:@cluster0.qlkxx.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

db.once('open', async function () {
    // we're connected!
    console.log("conected");
    // console.log(data.products[0]);
    // await saveData();
    // await updateProducts2();
    // await updateProducts3();

    Product.find({ name: { $regex: '.*' + 'sling' + '.*' } })

    // const categories = await dataLoaders.categoriesLoader.load([]);
    // console.log(categories);

    // const category = await categoryLoaders.categoryBySlug.load('shoes');
    // console.log(category);

    // const category2 = await dataLoaders.categoryById.load(['6064c8384ada330aed9abff7']);
    // console.log(category2);

    // categoryLoaders.categoryBySlug.load('shoes')
    //     .then((cat: ICategory) => console.log(cat))
    //     .catch(() => console.log('error'));

    // const subCategories = await subCategoryLoaders.subCategories.load([]);
    // console.log(subCategories);

    // const subCategories = await subCategoryLoaders.subCategoriesByCategory.load('6064c8384ada330aed9abff6');
    // console.log(subCategories);

    // const subCategory = await subCategoryLoaders.subCategoryBySlug.load('pumps');
    // console.log(subCategory);

    // const brands = await brandLoaders.brands.load([]);
    // console.log(brands);

    // const brand = await brandLoaders.brandBySlug.load('jimmychoo');
    // console.log(brand);

    // const colors = await colorLoaders.colors.load([]);
    // console.log(colors);

    // const color = await colorLoaders.colorByName.load('Black')
    // console.log(color);

    // const file = await fileLoaders.fileById.load('6064c84b4ada330aed9ac085');
    // console.log(file);

    // const products = await productLoaders.products.load(category, subCategory, brands, colors, sizes, skip, first);
    // const products = await productLoaders.products.load({ category: [], subCategory: ['pumps'], brands: [], colors: ['6064c83d4ada330aed9ac019'], sizes: [], skip: 0, first: 0 });
    // const products = await productLoaders.products.load({ category: [], subCategory: ['pumps'], brands: [], colors: [], sizes: [], skip: 0, first: 0 });
    // console.log("results", products.length);

    // const files = await fileLoaders.files.load([]);
    // console.log(files);

    // const aggregations = await productLoaders.aggregations.load({ category: [], subCategory: ['sandals'], brands: [], colors: [], sizes: [], skip: 0, first: 0 });
    // console.log(aggregations[0].brands);

    // const product = await productLoaders.productByIds.load('jimmy-choo-romy-85-patent-leather-pumps');
    // console.log(product);

    // const product = await productLoaders.productById.load('606f4864be646f2942a08f0f');

    // console.log(product?.images);

    // export interface AggregationCategory2 {
    //     category: string
    //     subCategories: string[]
    // }

    // export interface Aggregations2 {
    //     count: number
    //     brands: string[]
    //     sizes: string[]
    //     colors: string[]
    //     categories: AggregationCategory2[]
    // }

    // const aggregations = await productLoaders.aggregations.load({ category: [], subCategory: [], brands: ['francescorusso'], colors: [], sizes: [], skip: 0, first: 0 });
    // console.log(JSON.stringify(aggregations, undefined, 2));

    // const agg: Aggregations2 = {
    //     count: aggregations[0].subCategories.reduce((acc: number, cat: any) => acc + cat.count, 0),
    //     brands: aggregations[0].brands.map((brand: any) => brand._id),
    //     sizes: aggregations[0].sizes.map((size: any) => size._id),
    //     colors: aggregations[0].colors.map((color: any) => color._id[0]),
    //     categories: aggregations[0].categories.map((cat: any): AggregationCategory2 => {
    //         return {
    //             category: cat._id,
    //             subCategories: cat.subCategories.map((subCat: any) => subCat.subCat)
    //         }
    //     })
    // }

    // console.log(JSON.stringify(agg, undefined, 2));

    // const docs = await Product.aggregate([
    //     {
    //         $match: {
    //             $and: [{
    //                 $or: [{ subCategory: '6064c8384ada330aed9abff9' }]
    //             }],
    //         },
    //     },
    //     {
    //         $facet: {
    //             brands: [
    //                 {
    //                     $group: {
    //                         _id: "$brand",
    //                         count: { $sum: 1 }
    //                     }
    //                 },
    //                 {
    //                     $project: {
    //                         _id: "$_id",
    //                         count: "$count",
    //                         // brand: "$_id"
    //                     }
    //                 }
    //             ],
    //             categories: [
    //                 {
    //                     $group: {
    //                         _id: "$category",
    //                         count: { $sum: 1 }
    //                     }
    //                 },
    //                 {
    //                     $project: {
    //                         _id: "$_id",
    //                         count: "$count",
    //                         // colors: "$_id"
    //                     }
    //                 }
    //             ],
    //             subCategories: [
    //                 {
    //                     $group: {
    //                         _id: "$subCategory",
    //                         count: { $sum: 1 }
    //                     }
    //                 },
    //                 {
    //                     $project: {
    //                         _id: "$_id",
    //                         count: "$count",
    //                         // colors: "$_id"
    //                     }
    //                 }
    //             ],
    //             colors: [
    //                 {
    //                     $group: {
    //                         _id: "$colors",
    //                         count: { $sum: 1 }
    //                     }
    //                 },
    //                 {
    //                     $project: {
    //                         _id: "$_id",
    //                         count: "$count",
    //                         // colors: "$_id"
    //                     }
    //                 }
    //             ],
    //             sizes: [
    //                 {
    //                     $project: {
    //                         "_id": 1,
    //                         "sizes.sizes": 1
    //                     }
    //                 },
    //                 {
    //                     $unwind: {
    //                         path: "$sizes",
    //                         preserveNullAndEmptyArrays: true
    //                     }
    //                 },
    //                 {
    //                     $unwind: {
    //                         path: "$sizes.sizes",
    //                         preserveNullAndEmptyArrays: true
    //                     }
    //                 },
    //                 {
    //                     $group: {
    //                         _id: "$sizes.sizes",
    //                         count: {
    //                             $sum: 1
    //                         }
    //                     }
    //                 },
    //             ]
    //         }
    //     },
    // ]);

    // console.log("--------- agregations ---------------");

    // console.log(docs);
    // console.log(docs[0].brands);
    // console.log(docs[0].categories);
    // console.log(docs[0].subCategories);
    // console.log(docs[0].colors);
    // console.log(docs[0].sizes);

    // const docs = await Product.aggregate([
    //     {
    //         $match: {},
    //     },
    //     {
    //         $group: {
    //             _id: "$brand",
    //             total_products: {
    //                 $sum: 1
    //             }
    //         }
    //     },
    //     {
    //         $project: {
    //             _id: "$_id",
    //             total_products: "$total_products",
    //             brand: "$_id"
    //         }
    //     }
    // ]);

    // const docs = await Product.aggregate([
    //     {
    //         $match: { },
    //     },
    //     {
    //         $group: {
    //             _id: "$colors",
    //             total_products: {
    //                 $sum: 1
    //             }
    //         }
    //     },
    //     {
    //         $project: {
    //             _id: "$_id",
    //             total_products: "$total_products",
    //             colors: "$_id"
    //         }
    //     }
    // ]);

    // [ 
    //     { 
    //         $project: { 
    //             Count: { 
    //                 $objectToArray: "$Details" 
    //             } 
    //         } 
    //     }, 
    //     { 
    //         $unwind: "$Count" 
    //     }, 
    //     { 
    //         $group: { 
    //             _id: "$Count.k", 
    //             count: { 
    //                 $sum: "$Count.v.Number"
    //             } 
    //         } 
    //     } 
    // ]

    // https://paulrohan.medium.com/aggregation-in-mongodb-8195c8624337
    // https://www.codementor.io/@prasadsaya/mongodb-aggregation-framework-working-with-arrays-18jd5fe2xo
    // https://www.javaer101.com/en/article/18934547.html    
    // const docs = await Product.aggregate([
    //     {
    //         $match: { },
    //     },
    //     {
    //         $project: {
    //             "_id": 1,
    //             "sizes.sizes": 1
    //         }
    //     },
    //     {
    //         $unwind: {
    //             path: "$sizes",
    //             preserveNullAndEmptyArrays: true
    //         }
    //     },
    //     {
    //         $unwind: {
    //             path: "$sizes.sizes",
    //             preserveNullAndEmptyArrays: true
    //         }
    //     },
    //     {
    //         $group: {
    //             _id: "$sizes.sizes",
    //             total_products: {
    //                 $sum: 1
    //             }
    //         }
    //     },
    //     // {
    //     //     $project: {
    //     //         _id: "$_id",
    //     //         total_products: "$total_products",
    //     //     }
    //     // }
    // ]);

    // console.log(docs);


    // const text = 'sling';

    // const searchProducts = async (text: string[]): Promise<any> => {
    //     console.log("text 2 ", text[0]);
    //     const docs = await Product.find({
    //         $or: [
    //             {
    //                 name: {
    //                     $regex: text[0],
    //                     $options: 'i'
    //                 },
    //             },
    //             {
    //                 description: {
    //                     $regex: text[0],
    //                     $options: 'i'
    //                 },
    //             },
    //             {
    //                 details: {
    //                     $regex: text[0],
    //                     $options: 'i'
    //                 }
    //             }
    //         ]
    //     });
    //     console.log(docs);

    //     return [docs];
    // }

    // // const productsLoader = new DataLoader((text: string[]) => {
    // //     console.log("text ", text);
    // //     searchProducts(text);
    // // });

    // // TypeError: DataLoader must be constructed with a function which accepts Array<key> and returns Promise<Array<value>>, but the function did not return a Promise of an Array of the same length as the Array of keys.    

    // const productsLoader = new DataLoader((text: string[]) => searchProducts(text));
    // const products = await productsLoader.load('sling');

    // console.log("---- result of the dataloader -----");
    // console.log(products);


    //const query = Person.findOne({ 'name.last': 'Ghost' });

    // const doc = await Product.findById('6064bd4a736689093a0f77eb');
    // console.log(doc);

    // const res = await Brand.find({}).limit(20).exec();
    // console.log(res);
    // const idjimmy = res.find((brand: any) => brand.slug === 'jimmychoo')?._id;
    // console.log(idjimmy);

    // const reshoes = await Product.find({brand: idjimmy}).exec();
    // console.log(reshoes);
});

async function updateProducts3() {
    const { MongoClient } = require("mongodb");
    const client = new MongoClient(mongoDB);

    await client.connect();
    // Establish and verify connection
    var cursor = db.collection('products').find();

    cursor.each(async function (_: any, doc: any) {
        // console.log(doc);
        if (doc) {
            const imagesWithOrder = doc.images[0].images.map((img: any, index: number) => {
                return {
                    file: img.id,
                    order: img.order
                }
            });

            console.log(imagesWithOrder);

            const images = [...doc.images];
            images[0].images = [...imagesWithOrder];

            await db.collection('products').updateOne({
                _id: doc._id
            }, {
                $set: {
                    images: images
                }
            });
        }
    });
}


async function updateProducts2() {
    const { MongoClient } = require("mongodb");
    // Create a new MongoClient
    const client = new MongoClient(mongoDB);

    await client.connect();
    // Establish and verify connection
    var cursor = db.collection('products').find();

    cursor.each(async function (_: any, doc: any) {
        // console.log(doc);
        if (doc) {
            const thumbnailId = doc.thumbnail;
            const mainImageId = doc.mainImage;

            await db.collection('products').updateOne({
                _id: doc._id
            }, {
                $set: {
                    thumbnail: {
                        image: thumbnailId,
                        color: doc.colors[0]
                    },
                    mainImage: {
                        image: mainImageId,
                        color: doc.colors[0]
                    }
                }
            });
        }
    });
}

async function updateProducts() {
    const { MongoClient } = require("mongodb");
    // Connection URI
    // const uri =
    //   "mongodb+srv://sample-hostname:27017/?poolSize=20&writeConcern=majority";
    // Create a new MongoClient
    const client = new MongoClient(mongoDB);

    await client.connect();
    // Establish and verify connection
    const r = await client.db("admin").command({ ping: 1 });
    console.log(r);
    // console.log("finished");

    var cursor = db.collection('products').find();

    let count = 0;
    cursor.each(async function (_: any, doc: any) {
        // console.log(doc);
        if (doc && !doc.mainImage) {
            const thumbnailId = doc.images[0].images[0];
            const mainImageId = doc.images[0].images[0];

            // console.log(doc.id)
            console.log(doc._id)
            console.log(thumbnailId);

            const imagesWithOrder = doc.images[0].images.map((imgId: any, index: number) => {
                return {
                    id: imgId,
                    order: index
                }
            });

            console.log(imagesWithOrder);

            const images = [...doc.images];
            images[0].images = [...imagesWithOrder];
            console.log(images);

            // const prod = await db.collection('products').findOne({ _id: doc._id });
            // console.log(prod);
            await db.collection('products').updateOne({
                _id: doc._id
            }, {
                $set: {
                    thumbnail: thumbnailId,
                    mainImage: mainImageId,
                    images: images
                }
            });

            count += 1;
            console.log("Total", count);
        }
    });
}

async function saveData() {
    await Category.deleteMany({});
    await SubCategory.deleteMany({});
    await Brand.deleteMany({});
    await Color.deleteMany({});
    await File.deleteMany({});
    await Product.deleteMany({});
    await Order.deleteMany({});
    await OrderItem.deleteMany({});
    await CartItem.deleteMany({});
    await User.deleteMany({});

    let categoryIds: Ids = await saveCategories(data.categories);
    let subCategoryIds: Ids = await saveSubCategories(data, categoryIds);
    let brandIds: Ids = await saveBrands(data);
    let colorIds: Ids = await saveColors(data);
    let imageProdIds: any = await saveImages(data);
    let userIds: any = await saveUsers(data);

    // console.log(categoryIds);
    // console.log(subCategoryIds);
    // console.log(brandIds);
    // console.log(colorIds);
    // console.log(imageProdIds);

    let productIds: any = {};
    for (let i = 0; i < data.products.length; i++) {
        const product = data.products[i];
        const productModel = new Product({
            ...product,
            colors: product.colors?.map(color => colorIds[color]),
            sizes: product.sizes?.map(size => {
                return {
                    ...size,
                    color: colorIds[size.color] || ''
                }
            }),
            images: product.images?.map(images => { return { color: colorIds[images.color], images: imageProdIds[i] } }),
            prices: product.prices?.map(prices => { return { ...prices, color: colorIds[prices.color] } }),
            brand: product.brand ? brandIds[product.brand] : '',
            category: product.category ? categoryIds[product.category] : '',
            subCategory: product.subCategory ? subCategoryIds[product?.subCategory] : ''
        });
        const res = await productModel.save();
        productIds[product.key] = res._id;
    }
    // console.log("Finished");

    let cartItemsId: any = {};
    for (let i = 0; i < data.cartItems.length; i++) {
        const cartItem = data.cartItems[i];
        const model = new CartItem({
            ...cartItem,
            product: productIds[cartItem.product],
            color: colorIds[cartItem.color],
            user: userIds[0]
        });
        const res = await model.save();
        cartItemsId[cartItem.key] = res._id;
    }

    for (let i = 0; i < data.orders.length; i++) {
        const order = data.orders[0];
        const key = order.key;

        const modelOrder = new Order({
            ...order,
            user: userIds[0]
        });

        const res = await modelOrder.save();
        const orderId = res._id;

        const orderItems = data.orderItems.filter(orderItem => orderItem.order === key);

        for (let j = 0; j < orderItems.length; j++) {
            const orderItem = orderItems[j];
            // console.log(orderItem.color);
            const modelOrderItems = new OrderItem({
                ...orderItem,
                color: colorIds[orderItem.color],
                user: userIds[0],
                product: productIds[orderItem.product],
                order: orderId,
                cartItem: cartItemsId[orderItem.cartItem],
            });

            await modelOrderItems.save();
        }
    }

    console.log("finished");

    // data.products.forEach(product => {
    //     const p = new Product(product);
    //     p.save();
    // });
    // data.categories.forEach(category => {
    //     const p = new Category(category);
    //     p.save();
    // });
    // data.subcategories.forEach(subCategory => {
    //     const p = new SubCategory(subCategory);
    //     p.save();
    // });
    // data.brands.forEach(brand => {
    //     const p = new Brand(brand);
    //     p.save();
    // });
    // data.colors.forEach(color => {
    //     const p = new Color(color);
    //     p.save();
    // });
    // data.images.forEach(image => {
    //     const p = new File(image);
    //     p.save();
    // });
    // data.users.forEach(user => {
    //     const p = new User(user);
    //     p.save();
    // });
}

async function saveCategories(categories: any): Promise<Ids> {
    let categoryIds: Ids = {};

    for (const category of categories) {
        const cat = new Category(category);
        const res = await cat.save();
        categoryIds[category.key] = res._id;
    }

    return categoryIds;
}

async function saveSubCategories(data: any, categoryIds: Ids): Promise<Ids> {
    let subCategoryIds: Ids = {};

    for (const subcategory of data.subcategories) {
        const category = data.categories.find((cat: any) => cat.key === subcategory.categoryKey);
        if (!category) continue;
        const subCat = new SubCategory({
            ...subcategory,
            categoryKey: categoryIds[category.key]
        });
        const res = await subCat.save();
        subCategoryIds[subcategory.key] = res._id;
    }

    return subCategoryIds;
}

async function saveBrands(data: any): Promise<Ids> {
    let brandsIds: Ids = {};

    for (const brand of data.brands) {
        const brandModel = new Brand(brand);
        const res = await brandModel.save();
        brandsIds[brand.key] = res._id;
    }

    return brandsIds;
}

async function saveColors(data: any): Promise<Ids> {
    let colorIds: Ids = {};

    for (const color of data.colors) {
        const colorModel = new Color(color);
        const res = await colorModel.save();
        colorIds[color.key] = res._id;
    }

    return colorIds;
}

function uploadFile(filename: string) {
    return new Promise((resolve, reject) => {
        cloudinary.uploader.upload(filename, { folder: 'products' }, (err: any, url: any) => {
            if (err) return reject(err);
            return resolve(url);
        })
    })
}


async function saveImages(data: any): Promise<any> {
    let imageProdIds: any = [];

    for (const images of data.imagesProduct) {
        let imageIds: String[] = [];
        for (const image of images) {
            // cloudinary.v2.uploader.upload(`../uploadedFile/${image.filename}`, 
            const fileData: any = await uploadFile(`uploadedFiles/${image.filename}`);

            const fileModel = new File({
                ...image,
                ...fileData,
                server: 'cloudinary'
            });
            const res = await fileModel.save();
            //colorIds[color.name] = res._id;
            imageIds.push(res._id);
        }
        imageProdIds.push(imageIds);
    }

    return imageProdIds;
}

async function saveUsers(data: any): Promise<any> {
    let userIds: any = [];

    for (const user of data.users) {
        const userModel = new User(user);
        const res = await userModel.save();
        userIds.push(res._id);
    }

    return userIds;
}

// async function saveCartItems(data: any): Promise<Ids> {
//     //let userIds: any = [];

//     for (const user of data.users) {
//         const userModel = new User(user);
//         const res = await userModel.save();
//         userIds.push(res._id);
//     }

//     return userIds;
// }
