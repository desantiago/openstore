import categoryLoaders from '../dataloaders/categories';
import subCategoryLoaders from '../dataloaders/subCategories';
import brandLoaders from '../dataloaders/brands';
import colorLoaders from '../dataloaders/colors';
import fileLoaders from '../dataloaders/files';
import productLoaders from '../dataloaders/products';
import userLoaders from '../dataloaders/users';
import cartItemLoaders from '../dataloaders/cartItems';
import orderLoaders from '../dataloaders/orders';
import orderItemLoaders from '../dataloaders/orderItems';

//import stripe from '../lib/stripe';
import stripeCharge from '../lib/stripeCharge';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { AuthenticationError, UserInputError } from 'apollo-server-errors';

import {
    Category,
    SubCategory,
    Brand,
    Product,
    Aggregations2,
    AggregationCategory2,
    Color,
    File,
    User,
    Token,
    CartItem,
    Order,
    OrderItem,
    OrderItemInput,
    QueryParams
} from './types';

import {
    IBrand,
    ICategory,
    IColor,
    IFile,
    IProduct,
    ISubCategory,
    IUser,
    ICartItem,
    IOrder,
    IOrderItem,
} from './ProductSchema';

import {
    updateCartItem,
    insertCartItem,
    deleteCartItem,
    insertOrder,
    insertOrderItem,
    insertUser,
    insertFile,
    insertFileFromCloudinary,
    insertProduct,
    updateProduct,
    deleteProduct,
    insertBrand,
    insertColor,
    deleteFile,
} from '../mutations';

interface Agg {
    _id: string,
    count: number;
}
interface SubCatResult {
    subCat: string,
}
interface CatAgg {
    _id: string,
    count: number;
    subCategories: SubCatResult[]
}
interface ColorAgg {
    _id: string[],
    count: number;
}

interface ResultAggregations {
    brands: Agg[],
    categories: CatAgg[],
    subCategories: Agg[],
    colors: ColorAgg[],
    sizes: Agg[],
}

export default class MongoDBData {
    private static instance: MongoDBData;

    private constructor() {
        // this.d = LocalData.getInstance();
    }

    public static getInstance(): MongoDBData {
        if (!MongoDBData.instance) {
            MongoDBData.instance = new MongoDBData();
        }
        return MongoDBData.instance;
    }

    public async getCategories(): Promise<ICategory[]> {
        return await categoryLoaders.categories.load([]);
    }

    public async getCategory(key: string): Promise<ICategory | null> {
        return await categoryLoaders.categoryById.load(key);
    }

    public async getCategoryBySlug(slug: string): Promise<ICategory | null> {
        return await categoryLoaders.categoryBySlug.load(slug);
    }

    // public addCategory(description: string, slug: string): Category {
    //     return this.d.addCategory(description, slug);
    // }

    public async getSubCategories(): Promise<ISubCategory[]> {
        return await subCategoryLoaders.subCategories.load([]);
    }

    public async getSubCategory(key: string): Promise<ISubCategory | null> {
        return await subCategoryLoaders.subCategoryById.load(key);
    }

    public async getSubCategoryBySlug(slug: string): Promise<ISubCategory | null> {
        return await subCategoryLoaders.subCategoryBySlug.load(slug);
    }

    // public addSubCategory(description: string, slug: string, categoryKey: string): SubCategory {
    //     return this.d.addSubCategory(description, slug, categoryKey);
    // }

    public async getSubCategoriesCategory(categoryKey: string): Promise<ISubCategory[]> {
        return await subCategoryLoaders.subCategoriesByCategory.load(categoryKey);
    }

    public async getBrands(): Promise<IBrand[]> {
        return await brandLoaders.brands.load([]);
    }

    public async getBrand(key: string): Promise<IBrand | null> {
        return await brandLoaders.brandById.load(key);
    }

    public async getBrandBySlug(slug: string): Promise<IBrand | null> {
        return await brandLoaders.brandBySlug.load(slug);
        // console.log('getBrandBySlug', brands);
        // return brands;
    }

    public async addBrand(description: string, slug: string): Promise<IBrand | null> {
        brandLoaders.brands.clearAll();
        return await insertBrand(description, slug);
    }

    public async getColors(): Promise<IColor[]> {
        return await colorLoaders.colors.load([]);
    }

    public async getColor(key: string): Promise<IColor> {
        return await colorLoaders.colorById.load(key);
    }

    public async addColor(name: string, hex: string): Promise<IColor | null> {
        colorLoaders.colors.clearAll();
        return await insertColor(name, hex);
    }

    public async getFiles(): Promise<IFile[]> {
        return await fileLoaders.files.load([]);
    }

    public async getFile(key: string): Promise<IFile | null> {
        return await fileLoaders.fileById.load(key);
    }

    public async addFile(filename: string, mimetype: string, encondig: string): Promise<IFile | null> {
        return await insertFile(filename, mimetype, encondig);
    }

    public async addFileFromCloudinary(input: File): Promise<IFile | null> {
        return await insertFileFromCloudinary(input);
    }

    public async deleteFile(key: string): Promise<IFile | null> {
        fileLoaders.fileById.clear(key);
        return await deleteFile(key);
    }


    public async getProducts(category: string[], subCategory: string[], brands: string[], colors: string[], sizes: string[], skip: number, first: number): Promise<IProduct[]> {
        return await productLoaders.products.load({ category, subCategory, brands, colors, sizes, skip, first });
    }

    public async getProductsByParams(where: QueryParams, skip: number, first: number): Promise<IProduct[]> {
        const params: QueryParams = await productLoaders.paramsFromQuery.load(where);

        return await productLoaders.products.load({
            ...params,
            skip,
            first
        });
    }

    private getAggregationFromResults(aggregations: ResultAggregations): Aggregations2 {
        const agg: Aggregations2 = {
            count: aggregations.subCategories.reduce((acc: number, cat: Agg) => acc + cat.count, 0),
            brands: aggregations.brands.map((brand: Agg) => brand._id),
            sizes: aggregations.sizes.map((size: Agg) => size._id),
            colors: aggregations.colors.map((color: ColorAgg) => color._id[0]),
            categories: aggregations.categories.map((cat: CatAgg): AggregationCategory2 => {
                return {
                    category: cat._id,
                    subCategories: cat.subCategories.map((subCat: SubCatResult) => subCat.subCat)
                }
            })
        }
        return agg;
    }

    public async aggregations(category: string[], subCategory: string[], brands: string[], colors: string[], sizes: string[]): Promise<Aggregations2> {
        const aggregations: any = await productLoaders.aggregations.load({ category, subCategory, brands, colors, sizes, skip: 0, first: 0 });
        return this.getAggregationFromResults(aggregations[0]);
    }

    public async aggregationsByParams(where: QueryParams): Promise<Aggregations2> {
        const params: QueryParams = await productLoaders.paramsFromQuery.load(where);

        const aggregations: any = await productLoaders.aggregations.load({
            ...params,
            skip: 0,
            first: 0
        });

        // console.log("------ aggregations --------");
        // console.log(JSON.stringify(aggregations, undefined, 2));
        // console.log(JSON.stringify(aggregations[0], undefined, 2));
        return {
            ...this.getAggregationFromResults(aggregations[0]),
            mode: params.mode
        };
    }

    public async aggregationsNoFilters(where: QueryParams): Promise<Aggregations2> {
        const params: QueryParams = await productLoaders.paramsFromQuery.load(where);

        const aggregations: any = await productLoaders.aggregations.load({
            category: params.mode === 'BRAND' ? [] : params.category,
            subCategory: params.mode === 'CATEGORY' || params.mode === 'BRAND' ? [] : params.subCategory,
            brands: params.mode === 'SUBCATEGORY' ? [] : params.brands,
            skip: 0,
            first: 0
        });

        // console.log("aggregationsNoFilters query");
        // console.log(JSON.stringify(aggregations, undefined, 2));
        return {
            ...this.getAggregationFromResults(aggregations[0]),
            mode: params.mode
        };
    }


    public async getProduct(key: string): Promise<IProduct | null> {
        return await productLoaders.productById.load(key);
    }

    public async getProductByIds(id: string): Promise<IProduct | null> {
        return await productLoaders.productByIds.load(id);
    }

    public async addProduct(productInput: Product): Promise<IProduct | null> {
        return await insertProduct(productInput)
    }

    public async updateProduct(key: string, productInput: Product): Promise<IProduct | null> {
        productLoaders.products.clearAll();
        productLoaders.productById.clear(key);
        return await updateProduct(key, productInput);
    }

    public async deleteProduct(key: string): Promise<IProduct | null> {
        productLoaders.products.clearAll();
        return await deleteProduct(key);
    }

    public async getUser(key: string): Promise<IUser | null> {
        return await userLoaders.userById.load(key);
    }

    public async signUp(name: string, email: string, password: string, secret: string): Promise<Token> {
        const user: IUser | null = await insertUser(name, email, password);
        if (!user) throw new UserInputError('Invalid user');
        let token = this.createToken(user, secret, '240m')

        return {
            token
        }
    }

    private createToken(user: IUser, secret: string, expiresIn: string): string {
        const { id, email, name } = user;
        return jwt.sign({ id, email, name }, secret, { expiresIn });
    }

    private validatePassword(loginPassword: string, password: string): boolean {
        return bcrypt.compareSync(loginPassword, password);
    }

    public async signIn(email: string, password: string, secret: string): Promise<Token | null> {
        const user: IUser | null = await userLoaders.userByEmail.load(email);
        if (!user) return null;
        if (!user) throw new UserInputError('Invalid user');

        const isValid = this.validatePassword(password, user.password);
        if (!isValid) throw new AuthenticationError('Invalid password');

        let token = this.createToken(user, secret, '240m')

        return {
            token
        }
    }

    public async getCartItem(key: string): Promise<ICartItem | null> {
        //return this.d.getCartItem(key);
        return await cartItemLoaders.cartItemById.load(key);
    }

    public async getItemsUser(userKey: string): Promise<ICartItem[]> {
        return await cartItemLoaders.itemsByUser.load(userKey);
    }

    public async addCartItem(quantity: number, color: string, size: string, productKey: string, userKey: string): Promise<ICartItem | null> {
        const product: Product | null = await this.getProduct(productKey);
        if (!product) return null;
        const cartItems: ICartItem[] = await this.getItemsUser(userKey);
        const cartItem: ICartItem | undefined = cartItems.find((cartItem: CartItem) => cartItem.product === productKey);

        if (cartItem && cartItem.color === color && cartItem.size === size) {
            cartItemLoaders.itemsByUser.clear(userKey);
            return await updateCartItem(cartItem.id, cartItem.quantity + quantity);
        }
        else {
            cartItemLoaders.itemsByUser.clear(userKey);
            return await insertCartItem(quantity, color, size, productKey, product.price ? product.price : 0, userKey);
        }
    }

    public async removeCartItem(key: string, user: string): Promise<ICartItem | null> {
        cartItemLoaders.itemsByUser.clear(user);
        return await deleteCartItem(key);
    }

    public searchProducts(terms: string): Promise<IProduct[]> {
        return productLoaders.productsLike.load(terms);
    }

    public async getOrders(): Promise<IOrder[]> {
        return await orderLoaders.orders.load([]);
    }

    public async getOrder(key: string): Promise<IOrder | null> {
        return await orderLoaders.orderById.load(key);
    }

    public async getOrderItem(key: string): Promise<IOrderItem | null> {
        return await orderItemLoaders.orderItemById.load(key);
    }

    public async getOrderItemByOrder(orderKey: string): Promise<IOrderItem[]> {
        return await orderItemLoaders.orderItemByOrder.load(orderKey);
    }

    // private async stripeCharge(stripeId: string, total: number) {
    //     return await stripe.paymentIntents.create({
    //         amount: total,
    //         currency: 'usd',
    //         payment_method: stripeId,
    //         confirm: true,
    //         // receipt_email: 'jenny.rosen@example.com',
    //     }).catch(err => {
    //         console.log(err);
    //         throw new Error(err.message);
    //     });
    // }

    public async addOrder(total: number, user: string, stripeId: string): Promise<IOrder> {
        return await insertOrder(total, user, stripeId);
    }

    public async addOrderItem(input: OrderItemInput): Promise<IOrderItem | null> {
        return await insertOrderItem(input);
    }

    public async checkOut(stripeId: string, user: IUser | null, test = false): Promise<IOrder | null> {
        if (!user) return null;

        const cartItems: ICartItem[] = await this.getItemsUser(user.id);
        const total: number = cartItems.reduce((total, item: ICartItem) => total + item.total, 0);

        const charge = test ? { id: 'mockedId' } : await stripeCharge(stripeId, total);

        const order: IOrder = await this.addOrder(total, user.id, charge.id);
        for (const cartItem of cartItems) {
            const input: OrderItemInput = {
                quantity: cartItem.quantity,
                size: cartItem.size,
                price: cartItem.price,
                color: cartItem.color,
                total: cartItem.total,
                product: cartItem.product,
                order: order.id,
                cartItem: cartItem.key,
                user: user.id
            };

            const o: IOrderItem | null = await this.addOrderItem(input);
        };

        cartItems.forEach(async (cartItem: ICartItem) => {
            await this.removeCartItem(cartItem.id, user.id);
        });

        cartItemLoaders.itemsByUser.clear(user.id);

        return order;
    }
}
