const DataLoader = require('dataloader');

import {
    User,
} from '../models/ProductSchema';

const getUsers = async () => {
    const users = await User.find({}).exec();
    return [users];
}

const getUserByEmail = async (emails: string[]) => {
    // return await User.findOne({ 'email': email });
    return Promise.all(emails.map(email => User.findOne({ 'email': email })))
}

const getUserById = async (ids: string[]) => {
    // return await User.find({ '_id': { $in: id } });
    return Promise.all(ids.map(id => User.findById(id)))
}

const users = new DataLoader(() => getUsers());
const userById = new DataLoader((id: string[]) => getUserById(id));
const userByEmail = new DataLoader((email: string[]) => getUserByEmail(email));

export default {
    users,
    userByEmail,
    userById
};