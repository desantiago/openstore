const DataLoader = require('dataloader');

import {
    Color,
} from '../models/ProductSchema';

const getColors = async () => {
    const colors = await Color.find({}).exec();
    return [colors];
}

const getColorByName = async (name: string[]) => {
    return await Color.find({ 'name': { $in: name } });
    // const color = await Color.findOne({ name: name[0] }).exec();
    // return [color];
}

const getColorById = async (ids: string[]) => {
    // return await Color.find({ '_id': { $in: id } });
    return await Promise.all(
        ids.map(id => Color.findById(id))
    );
}

const colors = new DataLoader(() => getColors());
const colorByName = new DataLoader((name: string[]) => getColorByName(name));
const colorById = new DataLoader((id: string[]) => getColorById(id));

export default {
    colors,
    colorByName,
    colorById
};