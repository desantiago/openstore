const DataLoader = require('dataloader');

import {
    Brand,
} from '../models/ProductSchema';

// class Queries {
//     private promises: any = [];
//     constructor() {
//         return this;
//     }

//     public addQuery(query: any): Queries {
//         this.promises.push(query);
//         return this;
//     }

//     public async results(): Promise<any> {
//         return await Promise.all(this.promises);
//     }
// }


const getBrands = async () => {
    const brands = await Brand.find({}).exec();
    return [brands]
}

const getBrandBySlug = async (slugs: string[]) => {
    return Promise.all(slugs.map(slug => Brand.findOne({ slug })))
    // return await Brand.find({ 'slug': { $in: slug } });
    // const brand = await Brand.findOne({ slug: slug[0] }).exec();
    // return [brand]
}

const getBrandById = async (ids: string[]) => {
    let promises: any = ids.map(id => Brand.findById(id));
    return await Promise.all(promises);

    // const queries: Queries = new Queries();
    // ids.forEach((id: string) => {
    //     queries.addQuery(Brand.findById(id))
    // });
    // const res = queries.results();
}

const brands = new DataLoader(() => getBrands());
const brandBySlug = new DataLoader((slug: string[]) => getBrandBySlug(slug));
const brandById = new DataLoader((id: string[]) => getBrandById(id));

export default {
    brands,
    brandBySlug,
    brandById
};