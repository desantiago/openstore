import { IColor, IProduct, ServerContext } from "../../models/ProductSchema";
import { CartItem, Product, User } from "../../models/types";

const CartItemResolvers = {
    Query: {
        cartItem: (_: void, { key }: CartItem, { data }: ServerContext): Promise<CartItem | null> => data.getCartItem(key),
        itemsUser: (_: void, { user }: CartItem, { data }: ServerContext): Promise<CartItem[]> => data.getItemsUser(user)
    },
    Mutation: {
        addCartItem: (_: void, { quantity, color, size, productKey }: any, { data, me }: ServerContext): Promise<CartItem | null> => {
            //TODO: check if user is logged
            return data.addCartItem(quantity, color, size, productKey, me.id);
        },
        removeCartItem: (_: void, { key }: CartItem, { data, me }: ServerContext): Promise<CartItem | null> => {
            //TODO: check if user is logged
            return data.removeCartItem(key, me.id);
        }
    },
    CartItem: {
        product: (cartItem: CartItem, _: void, { data }: ServerContext): Promise<IProduct | null> => {
            return data.getProduct(cartItem.product);
        },
        color: (cartItem: CartItem, _: void, { data }: ServerContext): Promise<IColor | null> => {
            return data.getColor(cartItem.color);
        },
    }
};

export default CartItemResolvers;