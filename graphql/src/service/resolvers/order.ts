import { IOrder, ServerContext } from "../../models/ProductSchema";
import { Order, OrderItem, User } from "../../models/types";

const OrderResolvers = {
    Query: {
        orders: (_: void, __: void, { data }: ServerContext): Promise<Order[]> => data.getOrders(),
        order: (_: void, { key }: Order, { data }: ServerContext): Promise<Order | null> => data.getOrder(key),
        // ordersByUser: (_: void, { user }: any, { data }: ServerContext) => data.getOrdersByUser(user),
    },
    Mutation: {
        createOrder: (_: void, { total, user, stripeId }: Order, { data }: ServerContext): Promise<Order | null> => {
            return data.addOrder(total, user, stripeId);
        },
        checkOut: (_: void, { stripeId }: Order, { data, me }: ServerContext): Promise<Order | null> => {
            if (!me) return Promise.resolve(null);
            return data.checkOut(stripeId, me);
        }
    },
    Order: {
        orderItems: (order: IOrder, args: void, { data }: ServerContext): Promise<OrderItem[]> => {
            return data.getOrderItemByOrder(order.id)
        },
        user: (order: IOrder, args: void, { data }: ServerContext): Promise<User | null> => {
            return data.getUser(order.user);
        }
    }
};

export default OrderResolvers;