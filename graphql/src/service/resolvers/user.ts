import { ICartItem, IUser, ServerContext } from "../../models/ProductSchema";
import { CartItem, Token, User } from "../../models/types";

const UserResolvers = {
    Query: {
        // subcategories: (_: any, __: any, { data }: any) => data.getSubCategories(),
        user: (_: void, { id }: any, { data }: ServerContext): Promise<IUser | null> => {
            return data.getUser(id)
        },
        authenticatedUser: (_: void, __: void, { data, me }: ServerContext): Promise<IUser | null> => {
            if (me) return data.getUser(me.id)
            return Promise.resolve(null);
        }
        // subcategoryBySlug: (_: any, { slug }: any, { data }: any) => data.getSubCategoryBySlug(slug),
    },
    Mutation: {
        signUp: (_: void, { name, email, password }: User, { data, secret }: ServerContext): Promise<Token> => {
            return data.signUp(name, email, password, secret);
        },
        signIn: (_: void, { email, password }: User, { data, secret }: ServerContext): Promise<Token | null> => {
            return data.signIn(email, password, secret);
        }
    },
    User: {
        cartItems: (user: IUser, args: void, { data }: ServerContext): Promise<CartItem[]> => {
            // console.log("cartItems", user.id);
            return data.getItemsUser(user.id)
        }
    }
    // SubCategory: {
    //     category: (subcategory: any, args: any, { data }: any) => {
    //         return data.getCategory(subcategory.categoryKey);
    //     }
    // }
};

export default UserResolvers;
