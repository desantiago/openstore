import { ICategory, ISubCategory, ServerContext } from "../../models/ProductSchema";
import { SubCategory } from "../../models/types";

const SubCategoryResolvers = {
    Query: {
        subcategories: (_: void, __: void, { data }: ServerContext): Promise<ISubCategory[]> => data.getSubCategories(),
        subcategory: (_: void, { key }: SubCategory, { data }: ServerContext): Promise<ISubCategory | null> => data.getSubCategory(key),
        subcategoryBySlug: (_: void, { slug }: SubCategory, { data }: ServerContext): Promise<ISubCategory | null> => data.getSubCategoryBySlug(slug),
    },
    Mutation: {
        // createSubCategory: (_: void, { description, slug, categoryKey }: any, { data }: ServerContext) => {
        //     return data.addSubCategory(description, slug, categoryKey);
        // }
    },
    SubCategory: {
        category: (subcategory: any, _: void, { data }: ServerContext): Promise<ICategory | null> => {
            return data.getCategory(subcategory.categoryKey);
        }
    }
};

export default SubCategoryResolvers;
