import { IBrand, ICategory, IColor, IFile, IProduct, ISubCategory, ServerContext } from "../../models/ProductSchema";
import { Aggregations2, File, Image, ImageColor, Prices, Product, ProductIds, QueryParams, QueryParamsInput, Sizes } from "../../models/types";

const ProductResolvers = {
    //products(category: string!, subcategory: string, brand: string!)
    Query: {
        products: (_: void, { category, subCategory, brands, colors, sizes, skip, first, }: QueryParams, { data }: ServerContext): Promise<IProduct[]> => {
            return data.getProducts(
                category ? category : [],
                subCategory ? subCategory : [],
                brands ? brands : [],
                colors ? colors : [],
                sizes ? sizes : [],
                skip ? skip : 0,
                first ? first : 0);
        },
        aggregations: (_: void, { category, subCategory, brands, colors, sizes }: QueryParams, { data }: ServerContext): Promise<Aggregations2> => {
            return data.aggregations(
                category ? category : [],
                subCategory ? subCategory : [],
                brands ? brands : [],
                colors ? colors : [],
                sizes ? sizes : []);
        },
        search: (_: void, { term }: ProductIds, { data }: ServerContext): Promise<IProduct[]> => data.searchProducts(term ? term : ''),
        product: (_: void, { key }: ProductIds, { data }: ServerContext): Promise<IProduct | null> => data.getProduct(key ? key : ''),
        productByIds: (_: void, { id }: ProductIds, { data }: ServerContext): Promise<IProduct | null> => data.getProductByIds(id ? id : ''),
        productsByParams: (_: void, { where, skip, first }: QueryParamsInput, { data }: ServerContext): Promise<IProduct[]> => {
            return data.getProductsByParams(
                where ? where : {},
                skip ? skip : 0,
                first ? first : 0)
        },
    },
    Mutation: {
        createProduct: (_: void, { input }: any, { data }: ServerContext) => {
            return data.addProduct(input);
        },
        updateProduct: (_: void, { key, input }: any, { data }: ServerContext) => {
            // console.log(JSON.stringify(input, null, 2));
            return data.updateProduct(key, input);
        },
        deleteProduct: (_: void, { key }: ProductIds, { data }: ServerContext) => {
            return data.deleteProduct(key ? key : '');
        }
    },
    Product: {
        // brand: (product: any, args: any, { data }: any) => {
        //     return data.getBrand(product.brand)
        // },
        brand: (product: Product, _: void, { data }: ServerContext): Promise<IBrand | null> => {
            return data.getBrand(product.brand ? product.brand : '');
        },
        category: (product: Product, _: void, { data }: ServerContext): Promise<ICategory | null> => {
            return data.getCategory(product.category ? product.category : '');
        },
        subCategory: (product: Product, _: void, { data }: ServerContext): Promise<ISubCategory | null> => {
            return data.getSubCategory(product.subCategory ? product.subCategory : '');
        },
        colors: (product: Product, _: void, { data }: ServerContext) => {
            if (!product.colors) return [];
            return product.colors.map((color: string) => data.getColor(color));
        },
    },
    Images: {
        color: (image: ImageColor, _: void, { data }: ServerContext): Promise<IColor | null> => {
            return data.getColor(image.color);
        },
        // images: async ({ images }: any, args: any, { data }: any) => {
        //     //return data.getColor(image.color);
        //     console.log("-------");
        //     console.log(images);
        //     return images.map(async (imageKey: any) => {
        //         return {
        //             id: await data.getFile(imageKey.id),
        //             order: imageKey.order
        //         }
        //     });
        // }
    },
    Image: {
        file: (image: any, _: void, { data }: ServerContext): Promise<IFile | null> => {
            return data.getFile(image._doc.file);
        },
    },
    Prices: {
        color: (price: Prices, _: void, { data }: ServerContext): Promise<IColor | null> => {
            return data.getColor(price.color);
        }
    },
    Sizes: {
        color: (size: Sizes, _: void, { data }: ServerContext): Promise<IColor | null> => {
            return data.getColor(size.color);
        }
    },
    ImageColor: {
        color: (imageColor: ImageColor, _: void, { data }: ServerContext): Promise<IColor | null> => {
            return data.getColor(imageColor.color);
        },
        image: (imageColor: ImageColor, _: void, { data }: ServerContext): Promise<IFile | null> => {
            return data.getFile(imageColor.image);
        },
    },
    RelatedProduct: {
        product: (relatedProduct: any, _: void, { data }: ServerContext): Promise<IProduct | null> => {
            return data.getProduct(relatedProduct.product);
        },
    }
    // SubCategory: {
    //     category: (subcategory: any, args: any, { data }: any) => {
    //         return data.getCategory(subcategory.categoryKey);
    //     }
    // }
};

export default ProductResolvers;
