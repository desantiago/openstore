import { makeExecutableSchema } from "graphql-tools";
// import { ServiceTypeDefs } from "./service/serviceSchema";
// import ServiceResolvers from "./service/serviceResolver";

import serviceTypeDefs from './service/schemas'
import ServiceResolvers from "./service/resolvers";

export const schema = makeExecutableSchema({
    // typeDefs: ServiceTypeDefs,
    typeDefs: serviceTypeDefs,
    resolvers: ServiceResolvers,
});
