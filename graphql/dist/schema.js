"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.schema = void 0;
var graphql_tools_1 = require("graphql-tools");
var schemas_1 = __importDefault(require("./service/schemas"));
var resolvers_1 = __importDefault(require("./service/resolvers"));
exports.schema = graphql_tools_1.makeExecutableSchema({
    typeDefs: schemas_1.default,
    resolvers: resolvers_1.default,
});
