"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("uuid");
var moment_1 = __importDefault(require("moment"));
var bcrypt_1 = __importDefault(require("bcrypt"));
function createSlug(brand, name) {
    var words = __spreadArrays(brand.toLocaleLowerCase().split(' '), name.toLocaleLowerCase().split(' ')).flat();
    return words.join('-');
}
function generatePasswordHash(password) {
    var saltRounds = 10;
    var hash = bcrypt_1.default.hashSync(password, saltRounds);
    return hash;
}
function testData() {
    var idCategoryShoes = uuid_1.v4();
    var idCategoryBags = uuid_1.v4();
    var userId = uuid_1.v4();
    console.log(userId);
    var users = [];
    users.push({
        id: userId,
        name: 'desantiago',
        email: 'desantiagod@gmail.com',
        password: generatePasswordHash('password'),
        timeStamp: moment_1.default().valueOf().toString()
    });
    var categories = [];
    categories.push({
        key: idCategoryShoes,
        description: 'Shoes',
        slug: 'shoes',
        timeStamp: moment_1.default().valueOf().toString()
    });
    categories.push({
        key: idCategoryBags,
        description: 'Bags',
        slug: 'bags',
        timeStamp: moment_1.default().valueOf().toString()
    });
    var idSubCategoryPumps = uuid_1.v4();
    var subcategories = [];
    subcategories.push({
        key: idSubCategoryPumps,
        description: 'Pumps',
        slug: 'pumps',
        timeStamp: '1',
        categoryKey: idCategoryShoes
    });
    var idSubCategoryFlats = uuid_1.v4();
    subcategories.push({
        key: idSubCategoryFlats,
        description: 'Flats',
        slug: 'flats',
        timeStamp: '1',
        categoryKey: idCategoryShoes
    });
    var idSubCategoryBoots = uuid_1.v4();
    subcategories.push({
        key: idSubCategoryBoots,
        description: 'Boots & Booties',
        slug: 'boots',
        timeStamp: '1',
        categoryKey: idCategoryShoes
    });
    var idSubCategorySandals = uuid_1.v4();
    subcategories.push({
        key: idSubCategorySandals,
        description: 'Sandals',
        slug: 'sandals',
        timeStamp: '1',
        categoryKey: idCategoryShoes
    });
    var idSubCategoryBeltBags = uuid_1.v4();
    subcategories.push({
        key: idSubCategoryBeltBags,
        description: 'Belt Bags',
        slug: 'beltbags',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    var idSubCategoryBucketBags = uuid_1.v4();
    subcategories.push({
        key: idSubCategoryBucketBags,
        description: 'Bucket Bags',
        slug: 'bucketbags',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    var idSubCategoryClutches = uuid_1.v4();
    subcategories.push({
        key: idSubCategoryClutches,
        description: 'Clutches',
        slug: 'clutches',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    var idSubCategoryCrossbody = uuid_1.v4();
    subcategories.push({
        key: idSubCategoryCrossbody,
        description: 'Crossbody',
        slug: 'crossbody',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    var idSubCategoryMiniBags = uuid_1.v4();
    subcategories.push({
        key: idSubCategoryMiniBags,
        description: 'Mini Bags',
        slug: 'minibags',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    var idSubCategoryShoulder = uuid_1.v4();
    subcategories.push({
        key: idSubCategoryShoulder,
        description: 'Shoulder',
        slug: 'shoulder',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    var idSubCategoryTotes = uuid_1.v4();
    subcategories.push({
        key: idSubCategoryTotes,
        description: 'Totes',
        slug: 'totes',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    var brands = [];
    var idBrandAeyde = uuid_1.v4();
    brands.push({
        key: idBrandAeyde,
        description: 'AEYDE',
        slug: 'aeyde',
        timeStamp: '1'
    });
    var idBrandAlexandreBirman = uuid_1.v4();
    brands.push({
        key: idBrandAlexandreBirman,
        description: 'Alexandre Birman',
        slug: 'alexandrebirman',
        timeStamp: '1'
    });
    var idBrandAlexander = uuid_1.v4();
    brands.push({
        key: idBrandAlexander,
        description: 'Alexander McQueen',
        slug: 'alexandermcqueen',
        timeStamp: '1'
    });
    var idBrandAlexandreVauthier = uuid_1.v4();
    brands.push({
        key: idBrandAlexandreVauthier,
        description: 'Alexandre Vauthier',
        slug: 'alexandrevauthier',
        timeStamp: '1'
    });
    var idBrandAlexanderWang = uuid_1.v4();
    brands.push({
        key: idBrandAlexanderWang,
        description: 'Alexander Wang',
        slug: 'alexanderwang',
        timeStamp: '1'
    });
    var idBrandAmina = uuid_1.v4();
    brands.push({
        key: idBrandAmina,
        description: 'Ammina Muaddi',
        slug: 'amminamuaddi',
        timeStamp: '1'
    });
    var idBrandAquazzura = uuid_1.v4();
    brands.push({
        key: idBrandAquazzura,
        description: 'Aquazzura',
        slug: 'aquazzura',
        timeStamp: '1'
    });
    var idBrandBalmain = uuid_1.v4();
    brands.push({
        key: idBrandBalmain,
        description: 'Balmain',
        slug: 'balmain',
        timeStamp: '1'
    });
    var idBrandByFar = uuid_1.v4();
    brands.push({
        key: idBrandByFar,
        description: 'By Far',
        slug: 'byfar',
        timeStamp: '1'
    });
    var idBrandCultGaia = uuid_1.v4();
    brands.push({
        key: idBrandCultGaia,
        description: 'Cult Gaia',
        slug: 'cultgaia',
        timeStamp: '1'
    });
    var idBrandFrancesco = uuid_1.v4();
    brands.push({
        key: idBrandFrancesco,
        description: 'Francesco Russo',
        slug: 'francescorusso',
        timeStamp: '1'
    });
    var idBrandGia = uuid_1.v4();
    brands.push({
        key: idBrandGia,
        description: 'Gia',
        slug: 'gia',
        timeStamp: '1'
    });
    var idBrandIsabelMarant = uuid_1.v4();
    brands.push({
        key: idBrandIsabelMarant,
        description: 'Isabel Marant',
        slug: 'isabelmarant',
        timeStamp: '1'
    });
    var idBrandJimmyChoo = uuid_1.v4();
    brands.push({
        key: idBrandJimmyChoo,
        description: 'Jimmy Choo',
        slug: 'jimmychoo',
        timeStamp: '1'
    });
    var idBrandManolo = uuid_1.v4();
    brands.push({
        key: idBrandManolo,
        description: 'Manolo Blahnik',
        slug: 'manoloblahnik',
        timeStamp: '1'
    });
    var idBrandMarkCross = uuid_1.v4();
    brands.push({
        key: idBrandMarkCross,
        description: 'Mark Cross',
        slug: 'markcross',
        timeStamp: '1'
    });
    var idBrandMarni = uuid_1.v4();
    brands.push({
        key: idBrandMarni,
        description: 'Marni',
        slug: 'marni',
        timeStamp: '1'
    });
    var idBrandPacoRabanne = uuid_1.v4();
    brands.push({
        key: idBrandPacoRabanne,
        description: 'Paco Rabanne',
        slug: 'pacorabanne',
        timeStamp: '1'
    });
    var idBrandProenza = uuid_1.v4();
    brands.push({
        key: idBrandProenza,
        description: 'Proenza Schouler',
        slug: 'proenzaschouler',
        timeStamp: '1'
    });
    var idBrandStaud = uuid_1.v4();
    brands.push({
        key: idBrandStaud,
        description: 'Staud',
        slug: 'staud',
        timeStamp: '1'
    });
    var idBrandValentino = uuid_1.v4();
    brands.push({
        key: idBrandValentino,
        description: 'Valentino Garavani',
        slug: 'valentinogaravani',
        timeStamp: '1'
    });
    var idBrandWandler = uuid_1.v4();
    brands.push({
        key: idBrandWandler,
        description: 'Wandler',
        slug: 'wandler',
        timeStamp: '1'
    });
    var colors = [];
    var idColorBlack = uuid_1.v4();
    colors.push({
        key: idColorBlack,
        name: 'Black',
        hex: '#000'
    });
    var idHotPinkColor = uuid_1.v4();
    colors.push({
        key: idHotPinkColor,
        name: 'Hot Pink',
        hex: '#000'
    });
    var idWhiteColor = uuid_1.v4();
    colors.push({
        key: idWhiteColor,
        name: 'White',
        hex: '#fff'
    });
    var idBeigeColor = uuid_1.v4();
    colors.push({
        key: idBeigeColor,
        name: 'Beige',
        hex: '#fff'
    });
    var idBrownColor = uuid_1.v4();
    colors.push({
        key: idBrownColor,
        name: 'Brown',
        hex: '#fff'
    });
    var idGoldColor = uuid_1.v4();
    colors.push({
        key: idGoldColor,
        name: 'Gold',
        hex: '#fff'
    });
    var idPinkColor = uuid_1.v4();
    colors.push({
        key: idPinkColor,
        name: 'Pink',
        hex: '#fff'
    });
    var idOrangeColor = uuid_1.v4();
    colors.push({
        key: idOrangeColor,
        name: 'Orange',
        hex: '#fff'
    });
    var idMultiColor = uuid_1.v4();
    colors.push({
        key: idMultiColor,
        name: 'Multi',
        hex: '#fff'
    });
    var idSilverColor = uuid_1.v4();
    colors.push({
        key: idSilverColor,
        name: 'Silver',
        hex: '#fff'
    });
    var defaultImage = {
        mimetype: 'image/jpeg',
        encoding: '7bit',
        timeStamp: moment_1.default().milliseconds() + ''
    };
    var images1 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ROMY85PAT_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ROMY85PAT_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ROMY85PAT_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ROMY85PAT_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ROMY85PAT_5.jpg' }),
    ];
    var keyProduct1 = uuid_1.v4();
    var products = [];
    var product1 = {
        key: keyProduct1,
        name: 'Romy 85 Patent Leather Pumps',
        description: "Crafted in Italy, the label's pointed toe Romy pumps are timeless in black patent leather. This sleek pair works equally well with cocktail dresses and denim. 3.3\" stiletto heels. 100% leather. In black. Made in Italy.",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: '',
        id: '',
        slug: createSlug('Jimmy Choo', 'Romy 85 Patent Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 65000,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images1.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 65000 },
                    { size: '5.5', price: 65000 },
                    { size: '6', price: 65000 },
                    { size: '6.5', price: 65000 },
                    { size: '7', price: 65000 },
                    { size: '8', price: 65000 },
                    { size: '9', price: 65000 },
                    { size: '10', price: 65000 },
                    { size: '11', price: 65000 },
                    { size: '12', price: 65000 },
                ],
            }],
        brand: idBrandJimmyChoo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product1);
    var images2 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '9XX03186703_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '9XX03186703_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '9XX03186703_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '9XX03186703_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '9XX03186703_5.jpeg' }),
    ];
    var keyProduct2 = uuid_1.v4();
    var product2 = {
        key: keyProduct2,
        name: 'Hangisi Crystal Satin Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Detailed with their signature crystal buckles, Manolo Blahnik updates their cult-favorite Hangisi pointed toe heels in high shine satin.' +
            'Uppers: 68% viscose, 32% silk' +
            'Lining: 100% kid leather' +
            'Soles: 100% cow leather' +
            'Heel height: 4.1"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Manolo Blahnik', 'Hangisi Crystal Satin Pumps'),
        startSize: 5,
        endSize: 12,
        price: 99500,
        colors: [idHotPinkColor],
        sizes: [{
                color: idHotPinkColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idHotPinkColor,
                images: images2.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idHotPinkColor,
                prices: [
                    { size: '5', price: 99500 },
                    { size: '5.5', price: 99500 },
                    { size: '6', price: 99500 },
                    { size: '6.5', price: 99500 },
                    { size: '7', price: 99500 },
                    { size: '8', price: 99500 },
                    { size: '9', price: 99500 },
                    { size: '10', price: 99500 },
                    { size: '11', price: 99500 },
                    { size: '12', price: 99500 },
                ],
            }],
        brand: idBrandManolo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product2);
    var images3 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '21CRTIFPWHNAP_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '21CRTIFPWHNAP_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '21CRTIFPWHNAP_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '21CRTIFPWHNAP_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '21CRTIFPWHNAP_5.jpeg' }),
    ];
    var product3 = {
        key: uuid_1.v4(),
        name: 'Tiffany Slingback Leather Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Marrying classic Mary Janes with your favorite slingbacks, the label\'s Tiffany stiletto pumps are an elegant essential with knife sharp pointed toes. Buckle straps.' +
            'Composition: leather' +
            'Heel height: 3.3"' +
            'Made in Italy',
        id: '',
        slug: createSlug('By Far', 'Tiffany Slingback Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 48000,
        colors: [idWhiteColor],
        sizes: [{
                color: idWhiteColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idWhiteColor,
                images: images3.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idWhiteColor,
                prices: [
                    { size: '5', price: 48000 },
                    { size: '5.5', price: 48000 },
                    { size: '6', price: 48000 },
                    { size: '6.5', price: 48000 },
                    { size: '7', price: 48000 },
                    { size: '8', price: 48000 },
                    { size: '9', price: 48000 },
                    { size: '10', price: 48000 },
                    { size: '11', price: 48000 },
                    { size: '12', price: 48000 },
                ],
            }],
        brand: idBrandByFar,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product3);
    var images4 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '633501WHQVA_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '633501WHQVA_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '633501WHQVA_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '633501WHQVA_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '633501WHQVA_5.jpeg' }),
    ];
    var product4 = {
        key: uuid_1.v4(),
        name: 'Embellished Suede Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'McQueen revamps your go-to suede stilettos with sparkling crystal embellishments along pointed toes for a strong signature finish.' +
            'Composition: leather, brass' +
            'Wedge height: 3.5"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Alexander McQueen', 'Embellished Suede Pumps'),
        startSize: 5,
        endSize: 12,
        price: 99500,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images4.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 99500 },
                    { size: '5.5', price: 99500 },
                    { size: '6', price: 99500 },
                    { size: '6.5', price: 99500 },
                    { size: '7', price: 99500 },
                    { size: '8', price: 99500 },
                    { size: '9', price: 99500 },
                    { size: '10', price: 99500 },
                    { size: '11', price: 99500 },
                    { size: '12', price: 99500 },
                ],
            }],
        brand: idBrandAlexander,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product4);
    var images5 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '633501WHR721081_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '633501WHR721081_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '633501WHR721081_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '633501WHR721081_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '633501WHR721081_5.jpg' }),
    ];
    var product5 = {
        key: uuid_1.v4(),
        name: 'Spiked Suede Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'McQueen revamps your go-to suede stilettos with silver-tone spikes along pointed toes for a strong signature finish.' +
            'Composition: leather, brass' +
            'Heel height: 3.5"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Alexander McQueen', 'Spiked Suede Pumps'),
        startSize: 5,
        endSize: 12,
        price: 95000,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images5.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 95000 },
                    { size: '5.5', price: 95000 },
                    { size: '6', price: 95000 },
                    { size: '6.5', price: 95000 },
                    { size: '7', price: 95000 },
                    { size: '8', price: 95000 },
                    { size: '9', price: 95000 },
                    { size: '10', price: 95000 },
                    { size: '11', price: 95000 },
                    { size: '12', price: 95000 },
                ],
            }],
        brand: idBrandAlexander,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product5);
    var images6 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'B3512301710001_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'B3512301710001_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'B3512301710001_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'B3512301710001_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'B3512301710001_5.jpg' }),
    ];
    var product6 = {
        key: uuid_1.v4(),
        name: 'Clarita 100 Suede Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'The label\'s signature Clarita 100 pumps are a go-to style in soft suede. Suede ankle ties. 3.9" stiletto heels. Leather uppers, lining and soles. Imported.',
        id: '',
        slug: createSlug('Alexander McQueen', 'Clarita 100 Suede Pumps'),
        startSize: 5,
        endSize: 12,
        price: 69500,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images6.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 69500 },
                    { size: '5.5', price: 69500 },
                    { size: '6', price: 69500 },
                    { size: '6.5', price: 69500 },
                    { size: '7', price: 69500 },
                    { size: '8', price: 69500 },
                    { size: '9', price: 69500 },
                    { size: '10', price: 69500 },
                    { size: '11', price: 69500 },
                    { size: '12', price: 69500 },
                ],
            }],
        brand: idBrandAlexander,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product6);
    var images7 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BBBLK_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BBBLK_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BBBLK_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BBBLK_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BBBLK_5.jpg' }),
    ];
    var product7 = {
        key: uuid_1.v4(),
        name: 'BB Snakeskin Embossed Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Manolo Blahnik\'s timeless BB pumps updated for the new season in a glossy snakeskin-embossed leather. Styled with pointed toes and 4" stiletto heels. Leather lining and soles. In black.  Made in Italy.',
        id: '',
        slug: createSlug('Manolo Blahnik', 'BB Snakeskin Embossed Pumps'),
        startSize: 5,
        endSize: 12,
        price: 93500,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images7.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 93500 },
                    { size: '5.5', price: 93500 },
                    { size: '6', price: 93500 },
                    { size: '6.5', price: 93500 },
                    { size: '7', price: 93500 },
                    { size: '8', price: 93500 },
                    { size: '9', price: 93500 },
                    { size: '10', price: 93500 },
                    { size: '11', price: 93500 },
                    { size: '12', price: 93500 },
                ],
            }],
        brand: idBrandManolo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product7);
    var images8 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BING100PATPINK_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BING100PATPINK_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BING100PATPINK_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BING100PATPINK_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BING100PATPINK_5.jpg' }),
    ];
    var product8 = {
        key: uuid_1.v4(),
        name: 'Bing Patent Crystal-Embellished Mules',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'A classic party style, Jimmy Choo\'s Bing pumps in patent leather feature crystal-embellished ankle straps.' +
            'Composition: leather, glass ' +
            'Heel height: 4"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Jimmy Choo', 'Bing Patent Crystal-Embellished Mules'),
        startSize: 5,
        endSize: 12,
        price: 95000,
        colors: [idBeigeColor],
        sizes: [{
                color: idBeigeColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idBeigeColor,
                images: images8.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idBeigeColor,
                prices: [
                    { size: '5', price: 95000 },
                    { size: '5.5', price: 95000 },
                    { size: '6', price: 95000 },
                    { size: '6.5', price: 95000 },
                    { size: '7', price: 95000 },
                    { size: '8', price: 95000 },
                    { size: '9', price: 95000 },
                    { size: '10', price: 95000 },
                    { size: '11', price: 95000 },
                    { size: '12', price: 95000 },
                ],
            }],
        brand: idBrandJimmyChoo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product8);
    var images9 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'CCAMPARINUDE_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'CCAMPARINUDE_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'CCAMPARINUDE_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'CCAMPARINUDE_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'CCAMPARINUDE_5.jpg' }),
    ];
    var product9 = {
        key: uuid_1.v4(),
        name: 'Campari Patent Mary Jane Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Trimmed in tonal grosgrain, the label\'s Campari pumps are a patent leather re-imagining of the classic Mary Jane. Pointed toes. Side button closures.' +
            'Uppers: 100% calf leather' +
            'Lining: 100% kid leather' +
            'Soles: 100% cow leather' +
            'Heel height: 4"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Manolo Blahnik', 'Campari Patent Mary Jane Pumps'),
        startSize: 5,
        endSize: 12,
        price: 74500,
        colors: [idBeigeColor],
        sizes: [{
                color: idBeigeColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idBeigeColor,
                images: images9.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idBeigeColor,
                prices: [
                    { size: '5', price: 74500 },
                    { size: '5.5', price: 74500 },
                    { size: '6', price: 74500 },
                    { size: '6.5', price: 74500 },
                    { size: '7', price: 74500 },
                    { size: '8', price: 74500 },
                    { size: '9', price: 74500 },
                    { size: '10', price: 74500 },
                    { size: '11', price: 74500 },
                    { size: '12', price: 74500 },
                ],
            }],
        brand: idBrandManolo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product9);
    var images10 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'FEXHIGP0SUELEP_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'FEXHIGP0SUELEP_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'FEXHIGP0SUELEP_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'FEXHIGP0SUELEP_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'FEXHIGP0SUELEP_5.jpeg' }),
    ];
    var product10 = {
        key: uuid_1.v4(),
        name: 'Fenix 105 Suede Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Tailored along classic pointed toes, Aquazzura\'s Fenix pumps sweep back into thin stilettos along cut-out panels of colorblocked suede.' +
            'Uppers: kid suede' +
            'Lining: goat leather' +
            'Soles: calf leather' +
            'Heel height: 4.1"' +
            'Made in Italy ',
        id: '',
        slug: createSlug('Aquazzura', 'Fenix 105 Suede Pumps'),
        startSize: 5,
        endSize: 12,
        price: 76500,
        colors: [idMultiColor],
        sizes: [{
                color: idMultiColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idMultiColor,
                images: images10.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idMultiColor,
                prices: [
                    { size: '5', price: 76500 },
                    { size: '5.5', price: 76500 },
                    { size: '6', price: 76500 },
                    { size: '6.5', price: 76500 },
                    { size: '7', price: 76500 },
                    { size: '8', price: 76500 },
                    { size: '9', price: 76500 },
                    { size: '10', price: 76500 },
                    { size: '11', price: 76500 },
                    { size: '12', price: 76500 },
                ],
            }],
        brand: idBrandAquazzura,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product10);
    var images11 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'FR35121A12026110999_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'FR35121A12026110999_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'FR35121A12026110999_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'FR35121A12026110999_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'FR35121A12026110999_5.jpg' }),
    ];
    var product11 = {
        key: uuid_1.v4(),
        name: 'Wave 105 Ankle Wrap Pump',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Francesco Russo\'s Wave pumps wrap around the ankles in two-tones of smooth leather for a trend-right staple. Ankle tie closures.' +
            'Composition: leather' +
            'Heel height: 4.1"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Francesco Russo', 'Wave 105 Ankle Wrap Pump'),
        startSize: 5,
        endSize: 12,
        price: 83000,
        colors: [idBeigeColor],
        sizes: [{
                color: idBeigeColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idBeigeColor,
                images: images11.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idBeigeColor,
                prices: [
                    { size: '5', price: 83000 },
                    { size: '5.5', price: 83000 },
                    { size: '6', price: 83000 },
                    { size: '6.5', price: 83000 },
                    { size: '7', price: 83000 },
                    { size: '8', price: 83000 },
                    { size: '9', price: 83000 },
                    { size: '10', price: 83000 },
                    { size: '11', price: 83000 },
                    { size: '12', price: 83000 },
                ],
            }],
        brand: idBrandFrancesco,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product11);
    var images12 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'HANGISIBRONZE_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'HANGISIBRONZE_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'HANGISIBRONZE_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'HANGISIBRONZE_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'HANGISIBRONZE_5.jpg' }),
    ];
    var product12 = {
        key: uuid_1.v4(),
        name: 'Hangisi Crystal Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Glistening lurex mesh pumps from Manolo Blahnik with signature Hangisi crystal buckles. Closed pointed toe. Slip-on style. 3" stiletto heel. Fabric upper. Leather lining and soles. In silver. Made in Italy.',
        id: '',
        slug: createSlug('Manolo Blahnik', 'Hangisi Crystal Pumps'),
        startSize: 5,
        endSize: 12,
        price: 102500,
        colors: [idSilverColor],
        sizes: [{
                color: idSilverColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idSilverColor,
                images: images12.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idSilverColor,
                prices: [
                    { size: '5', price: 102500 },
                    { size: '5.5', price: 102500 },
                    { size: '6', price: 102500 },
                    { size: '6.5', price: 102500 },
                    { size: '7', price: 102500 },
                    { size: '8', price: 102500 },
                    { size: '9', price: 102500 },
                    { size: '10', price: 102500 },
                    { size: '11', price: 102500 },
                    { size: '12', price: 102500 },
                ],
            }],
        brand: idBrandManolo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product12);
    var images13 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'LURUMBLK_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'LURUMBLK_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'LURUMBLK_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'LURUMBLK_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'LURUMBLK_5.jpg' }),
    ];
    var product13 = {
        key: uuid_1.v4(),
        name: 'Lurum Crystal-Embellished Satin Mules',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Event-ready stunners, Manolo Blahnik\'s sleek satin Lurum pumps dazzle with crystal-embellished uppers and ankle straps. ' +
            'Composition: polyester satin, leather, glass crystals' +
            'Heel height: 4"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Manolo Blahnik', 'Lurum Crystal-Embellished Satin Mules'),
        startSize: 5,
        endSize: 12,
        price: 129500,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images13.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 129500 },
                    { size: '5.5', price: 129500 },
                    { size: '6', price: 129500 },
                    { size: '6.5', price: 129500 },
                    { size: '7', price: 129500 },
                    { size: '8', price: 129500 },
                    { size: '9', price: 129500 },
                    { size: '10', price: 129500 },
                    { size: '11', price: 129500 },
                    { size: '12', price: 129500 },
                ],
            }],
        brand: idBrandManolo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product13);
    var images14 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'MITHIGP0NAP000_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'MITHIGP0NAP000_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'MITHIGP0NAP000_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'MITHIGP0NAP000_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'MITHIGP0NAP000_5.jpeg' }),
    ];
    var product14 = {
        key: uuid_1.v4(),
        name: 'Minute 105 Leather Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Doubling down on re-imagining the classic Mary Jane, the Italian label\'s Minute stiletto pumps are an elegant essential in smooth leather. Back zip closure.' +
            'Composition: leather' +
            'Heel height: 4.1"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Aquazzura', 'Minute 105 Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 79500,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images14.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 79500 },
                    { size: '5.5', price: 79500 },
                    { size: '6', price: 79500 },
                    { size: '6.5', price: 79500 },
                    { size: '7', price: 79500 },
                    { size: '8', price: 79500 },
                    { size: '9', price: 79500 },
                    { size: '10', price: 79500 },
                    { size: '11', price: 79500 },
                    { size: '12', price: 79500 },
                ],
            }],
        brand: idBrandAquazzura,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product14);
    var images15 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'NREHIGM0SKOTPG_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'NREHIGM0SKOTPG_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'NREHIGM0SKOTPG_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'NREHIGM0SKOTPG_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'NREHIGM0SKOTPG_5.jpg' }),
    ];
    var product15 = {
        key: uuid_1.v4(),
        name: 'Rendez Vous 105 Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Night out-ready in snakeskin-embossed leather, the label\'s signature Rendez Vous pumps are secured with metallic gold-tone leather cording.' +
            'Composition: leather' +
            'Heel height: 4.1"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Aquazzura', 'Rendez Vous 105 Pumps'),
        startSize: 5,
        endSize: 12,
        price: 75000,
        colors: [idBeigeColor],
        sizes: [{
                color: idBeigeColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idBeigeColor,
                images: images15.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idBeigeColor,
                prices: [
                    { size: '5', price: 75000 },
                    { size: '5.5', price: 75000 },
                    { size: '6', price: 75000 },
                    { size: '6.5', price: 75000 },
                    { size: '7', price: 75000 },
                    { size: '8', price: 75000 },
                    { size: '9', price: 75000 },
                    { size: '10', price: 75000 },
                    { size: '11', price: 75000 },
                    { size: '12', price: 75000 },
                ],
            }],
        brand: idBrandAquazzura,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product15);
    var images16 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'QW1S0393VB8P45_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'QW1S0393VB8P45_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'QW1S0393VB8P45_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'QW1S0393VB8P45_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'QW1S0393VB8P45_5.jpeg' }),
    ];
    var product16 = {
        key: uuid_1.v4(),
        name: 'Rockstud Leather Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'The ideal fashion-insider pump, Valentino crafts a caged pointed toe silhouette. The Italian label punctuates these beige stilettos with coordinating Rockstuds for a tone-on-tone finish. Ankle buckle closures.' +
            'Composition: leather, brass' +
            'Heel height: 3.9"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Valentino Garavani', 'Rockstud Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 109500,
        colors: [idBeigeColor],
        sizes: [{
                color: idBeigeColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idBeigeColor,
                images: images16.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idBeigeColor,
                prices: [
                    { size: '5', price: 109500 },
                    { size: '5.5', price: 109500 },
                    { size: '6', price: 109500 },
                    { size: '6.5', price: 109500 },
                    { size: '7', price: 109500 },
                    { size: '8', price: 109500 },
                    { size: '9', price: 109500 },
                    { size: '10', price: 109500 },
                    { size: '11', price: 109500 },
                    { size: '12', price: 109500 },
                ],
            }],
        brand: idBrandValentino,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product16);
    var images17 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1P270202201_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1P270202201_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1P270202201_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1P270202201_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1P270202201_5.jpg' }),
    ];
    var product17 = {
        key: uuid_1.v4(),
        name: 'Patent Leather Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Crafted in Italy from sleek patent leather, the label\'s take on the classic pump features dynamic angled vamps. Pointed toes. Slips on.' +
            'Composition: 100% leather' +
            'Heel height: 4"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Francesco Russo', 'Patent Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 64000,
        colors: [idWhiteColor],
        sizes: [{
                color: idWhiteColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idWhiteColor,
                images: images17.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idWhiteColor,
                prices: [
                    { size: '5', price: 64000 },
                    { size: '5.5', price: 64000 },
                    { size: '6', price: 64000 },
                    { size: '6.5', price: 64000 },
                    { size: '7', price: 64000 },
                    { size: '8', price: 64000 },
                    { size: '9', price: 64000 },
                    { size: '10', price: 64000 },
                    { size: '11', price: 64000 },
                    { size: '12', price: 64000 },
                ],
            }],
        brand: idBrandFrancesco,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product17);
    var images18 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'RAY100OPO_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'RAY100OPO_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'RAY100OPO_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'RAY100OPO_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'RAY100OPO_5.jpeg' }),
    ];
    var product18 = {
        key: uuid_1.v4(),
        name: 'Ray 100 Patent Leather Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Set a top their signature arched heels, the label\'s Ray pumps combine the quintessential slingback with the classic Mary Jane for a truly iconic closet staple in tortoiseshell-effect patent leather. Ankle buckle closures.' +
            'Composition: leather ' +
            'Heel height: 3.9"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Jimmy Choo', 'Ray 100 Patent Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 75000,
        colors: [idOrangeColor],
        sizes: [{
                color: idOrangeColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idOrangeColor,
                images: images18.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idOrangeColor,
                prices: [
                    { size: '5', price: 75000 },
                    { size: '5.5', price: 75000 },
                    { size: '6', price: 75000 },
                    { size: '6.5', price: 75000 },
                    { size: '7', price: 75000 },
                    { size: '8', price: 75000 },
                    { size: '9', price: 75000 },
                    { size: '10', price: 75000 },
                    { size: '11', price: 75000 },
                    { size: '12', price: 75000 },
                ],
            }],
        brand: idBrandJimmyChoo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product18);
    var images19 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ROMYPUMPPINK_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ROMYPUMPPINK_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ROMYPUMPPINK_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ROMYPUMPPINK_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ROMYPUMPPINK_5.jpeg' }),
    ];
    var product19 = {
        key: uuid_1.v4(),
        name: 'Romy Wavy Satin Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Speckled with glistening rhinestones along wavy vamps, the label\'s Romy pumps are a luxe upgrade to a pointed toe staple.' +
            'Uppers: 72% viscose, 28% silk' +
            'Lining: 100% goat leather' +
            'Soles: 100% cow leather' +
            'Heel height: 3.7"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Ammina Muaddi', 'Romy Wavy Satin Pumps'),
        startSize: 5,
        endSize: 12,
        price: 108000,
        colors: [idPinkColor],
        sizes: [{
                color: idPinkColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idPinkColor,
                images: images19.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idPinkColor,
                prices: [
                    { size: '5', price: 108000 },
                    { size: '5.5', price: 108000 },
                    { size: '6', price: 108000 },
                    { size: '6.5', price: 108000 },
                    { size: '7', price: 108000 },
                    { size: '8', price: 108000 },
                    { size: '9', price: 108000 },
                    { size: '10', price: 108000 },
                    { size: '11', price: 108000 },
                    { size: '12', price: 108000 },
                ],
            }],
        brand: idBrandAmina,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product19);
    var images20 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BING100PAT_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BING100PAT_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BING100PAT_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BING100PAT_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'BING100PAT_5.jpg' }),
    ];
    var product20 = {
        key: uuid_1.v4(),
        name: 'Bing Patent Crystal-Embellished Mule',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'A classic party style, Jimmy Choo\'s Bing pumps in patent leather feature crystal-embellished ankle straps.' +
            'Composition: leather, glass' +
            'Heel height: 4"' +
            'Made in Italy' +
            'Item is listed in Italian',
        id: '',
        slug: createSlug('Jimmy Choo', 'Bing Patent Crystal-Embellished Mule'),
        startSize: 5,
        endSize: 12,
        price: 95000,
        colors: [idWhiteColor],
        sizes: [{
                color: idWhiteColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idWhiteColor,
                images: images20.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idWhiteColor,
                prices: [
                    { size: '5', price: 95000 },
                    { size: '5.5', price: 95000 },
                    { size: '6', price: 95000 },
                    { size: '6.5', price: 95000 },
                    { size: '7', price: 95000 },
                    { size: '8', price: 95000 },
                    { size: '9', price: 95000 },
                    { size: '10', price: 95000 },
                    { size: '11', price: 95000 },
                    { size: '12', price: 95000 },
                ],
            }],
        brand: idBrandJimmyChoo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    };
    products.push(product20);
    var images21 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ALEXTOELOWBLK_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ALEXTOELOWBLK_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ALEXTOELOWBLK_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ALEXTOELOWBLK_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'ALEXTOELOWBLK_5.jpg' }),
    ];
    var product21 = {
        key: uuid_1.v4(),
        name: 'Alex Patent Leather Booties',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Take a sleek step in these black patent leather boots from Alexandre Vauthier. Asymmetric top hem. Pull-on style.' +
            'Compositon:' +
            'Heel height: 4.3"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Alexandre Vauthier', 'Alex Patent Leather Booties'),
        startSize: 5,
        endSize: 12,
        price: 103500,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images21.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 103500 },
                    { size: '5.5', price: 103500 },
                    { size: '6', price: 103500 },
                    { size: '6.5', price: 103500 },
                    { size: '7', price: 103500 },
                    { size: '8', price: 103500 },
                    { size: '9', price: 103500 },
                    { size: '10', price: 103500 },
                    { size: '11', price: 103500 },
                    { size: '12', price: 103500 },
                ],
            }],
        brand: idBrandAlexandreVauthier,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    };
    products.push(product21);
    var images22 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'GIORGIABOOTWHT_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'GIORGIABOOTWHT_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'GIORGIABOOTWHT_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'GIORGIABOOTWHT_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'GIORGIABOOTWHT_5.jpeg' }),
    ];
    var product22 = {
        key: uuid_1.v4(),
        name: 'Giorgia Leather Ankle Boots',
        description: "",
        sizeAndFit: "PLEASE NOTE: This style runs small. We recommend taking one size up from your usual size. Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Elevated a top their signature sculpted heels, the label\'s Giorgia ankle boots are pointed along the toes in smooth leather. Side zip closures.' +
            'Composition: leather' +
            'Heel height: 3.7"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Ammina Muaddi', 'Giorgia Leather Ankle Boots'),
        startSize: 5,
        endSize: 12,
        price: 104500,
        colors: [idWhiteColor],
        sizes: [{
                color: idWhiteColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idWhiteColor,
                images: images22.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idWhiteColor,
                prices: [
                    { size: '5', price: 104500 },
                    { size: '5.5', price: 104500 },
                    { size: '6', price: 104500 },
                    { size: '6.5', price: 104500 },
                    { size: '7', price: 104500 },
                    { size: '8', price: 104500 },
                    { size: '9', price: 104500 },
                    { size: '10', price: 104500 },
                    { size: '11', price: 104500 },
                    { size: '12', price: 104500 },
                ],
            }],
        brand: idBrandAmina,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    };
    products.push(product22);
    var images23 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'MALVINABOOTBLK_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'MALVINABOOTBLK_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'MALVINABOOTBLK_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'MALVINABOOTBLK_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'MALVINABOOTBLK_5.jpeg' }),
    ];
    var product23 = {
        key: uuid_1.v4(),
        name: 'Malvina Knee-High Patent Leather Boot',
        description: "",
        sizeAndFit: "PLEASE NOTE: This style runs small. We recommend taking one size up from your usual size. Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Contouring along the calf in their rigid patent leather, the label\'s Malvina 95 boots are an elegant knee-high staple. Side zip closures.' +
            'Composition: leather' +
            'Heel height: 3.7"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Ammina Muaddi', 'Malvina Knee-High Patent Leather Boot'),
        startSize: 5,
        endSize: 12,
        price: 134500,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images23.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 134500 },
                    { size: '5.5', price: 134500 },
                    { size: '6', price: 134500 },
                    { size: '6.5', price: 134500 },
                    { size: '7', price: 134500 },
                    { size: '8', price: 134500 },
                    { size: '9', price: 134500 },
                    { size: '10', price: 134500 },
                    { size: '11', price: 134500 },
                    { size: '12', price: 134500 },
                ],
            }],
        brand: idBrandAmina,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    };
    products.push(product23);
    var images24 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'VN1C602LGDT_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'VN1C602LGDT_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'VN1C602LGDT_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'VN1C602LGDT_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'VN1C602LGDT_5.jpeg' }),
    ];
    var product24 = {
        key: uuid_1.v4(),
        name: 'Salome Logo Leather Ankle Boots',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Secured with their signature logo \'B\' buckle plaques, the label\'s Salome ankle boots update your staple Chelseas in luxurious calfskin leather. Elasticized gore shaft. Back pull tabs. Pull-on style.' +
            'Composition: calfskin leather' +
            'Heel height: 2.2"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Balmain', 'Salome Logo Leather Ankle Boots'),
        startSize: 5,
        endSize: 12,
        price: 129500,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images24.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 129500 },
                    { size: '5.5', price: 129500 },
                    { size: '6', price: 129500 },
                    { size: '6.5', price: 129500 },
                    { size: '7', price: 129500 },
                    { size: '8', price: 129500 },
                    { size: '9', price: 129500 },
                    { size: '10', price: 129500 },
                    { size: '11', price: 129500 },
                    { size: '12', price: 129500 },
                ],
            }],
        brand: idBrandBalmain,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    };
    products.push(product24);
    var images25 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'GIORGIABOOTGLASSBLK_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'GIORGIABOOTGLASSBLK_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'GIORGIABOOTGLASSBLK_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'GIORGIABOOTGLASSBLK_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'GIORGIABOOTGLASSBLK_5.jpeg' }),
    ];
    var product25 = {
        key: uuid_1.v4(),
        name: 'Giorgia Leather Ankle Boots',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Elevated a top their signature sculpted plexiglass heels, the label\'s Giorgia ankle boots are pointed along the toes in smooth leather. Side zip closures.' +
            'Composition: leather, resin' +
            'Heel height" 3.7"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Ammina Muaddi', 'Giorgia Leather Ankle Boots'),
        startSize: 5,
        endSize: 12,
        price: 104500,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images25.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 104500 },
                    { size: '5.5', price: 104500 },
                    { size: '6', price: 104500 },
                    { size: '6.5', price: 104500 },
                    { size: '7', price: 104500 },
                    { size: '8', price: 104500 },
                    { size: '9', price: 104500 },
                    { size: '10', price: 104500 },
                    { size: '11', price: 104500 },
                    { size: '12', price: 104500 },
                ],
            }],
        brand: idBrandAmina,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    };
    products.push(product25);
    var images26 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'PERNI05B102_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'PERNI05B102_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'PERNI05B102_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'PERNI05B102_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'PERNI05B102_5.jpg' }),
    ];
    var product26 = {
        key: uuid_1.v4(),
        name: 'Pointed Toe Leather Booties',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Pernille Teisbaek lends her styling expertise with these minimal leather booties cut with sharp pointed toes. Pull-on style.' +
            'Composition: leather' +
            'Heel height: 3.3"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Gia', 'Pointed Toe Leather Booties'),
        startSize: 5,
        endSize: 12,
        price: 67500,
        colors: [idWhiteColor],
        sizes: [{
                color: idWhiteColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idWhiteColor,
                images: images26.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idWhiteColor,
                prices: [
                    { size: '5', price: 67500 },
                    { size: '5.5', price: 67500 },
                    { size: '6', price: 67500 },
                    { size: '6.5', price: 67500 },
                    { size: '7', price: 67500 },
                    { size: '8', price: 67500 },
                    { size: '9', price: 67500 },
                    { size: '10', price: 67500 },
                    { size: '11', price: 67500 },
                    { size: '12', price: 67500 },
                ],
            }],
        brand: idBrandGia,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    };
    products.push(product26);
    var images27 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'B350390013009_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'B350390013009_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'B350390013009_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'B350390013009_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'B350390013009_5.jpg' }),
    ];
    var product27 = {
        key: uuid_1.v4(),
        name: 'Clarita 90 Block Sandals',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'The label\'s must-have Clarita block heel sandals are a leg-lengthening style with self-tie ankle wraps. Finished with knotted toe straps.' +
            'Composition: 100% leather' +
            'Heel height: 3.5"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Alxeandre Birman', 'Clarita 90 Block Sandals'),
        startSize: 5,
        endSize: 12,
        price: 59500,
        colors: [idBeigeColor],
        sizes: [{
                color: idBeigeColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idBeigeColor,
                images: images27.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idBeigeColor,
                prices: [
                    { size: '5', price: 59500 },
                    { size: '5.5', price: 59500 },
                    { size: '6', price: 59500 },
                    { size: '6.5', price: 59500 },
                    { size: '7', price: 59500 },
                    { size: '8', price: 59500 },
                    { size: '9', price: 59500 },
                    { size: '10', price: 59500 },
                    { size: '11', price: 59500 },
                    { size: '12', price: 59500 },
                ],
            }],
        brand: idBrandAlexandreBirman,
        category: idCategoryShoes,
        subCategory: idSubCategorySandals
    };
    products.push(product27);
    var images28 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '01458000WHITE_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '01458000WHITE_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '01458000WHITE_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '01458000WHITE_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: '01458000WHITE_5.jpg' }),
    ];
    var product28 = {
        key: uuid_1.v4(),
        name: 'Clarita 100 Leather Sandals',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'The label\'s must-have Clarita sandals are a leg-lengthening style with a self-tie ankle wraps. Finished with knotted toe straps.' +
            'Composition: leather' +
            'Heel height: 3.9"' +
            'Imported',
        id: '',
        slug: createSlug('Alxeandre Birman', 'Clarita Leather Block Sandals'),
        startSize: 5,
        endSize: 12,
        price: 59500,
        colors: [idWhiteColor],
        sizes: [{
                color: idWhiteColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idWhiteColor,
                images: images28.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idWhiteColor,
                prices: [
                    { size: '5', price: 59500 },
                    { size: '5.5', price: 59500 },
                    { size: '6', price: 59500 },
                    { size: '6.5', price: 59500 },
                    { size: '7', price: 59500 },
                    { size: '8', price: 59500 },
                    { size: '9', price: 59500 },
                    { size: '10', price: 59500 },
                    { size: '11', price: 59500 },
                    { size: '12', price: 59500 },
                ],
            }],
        brand: idBrandAlexandreBirman,
        category: idCategoryShoes,
        subCategory: idSubCategorySandals
    };
    products.push(product28);
    var images29 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1S502213286_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1S502213286_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1S502213286_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1S502213286_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1S502213286_5.jpg' }),
    ];
    var product29 = {
        key: uuid_1.v4(),
        name: 'T-Strap Patent Leather Cage Sandal',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Set atop leg-lengthening stilettos, Francesco Russo updates their signature T-strap cage sandal with mirrored gold patent leather. Ankle buckle closures.' +
            'Composition: leather' +
            'Heel height: 4.1"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Francesco Russo', 'T-Strap Patent Leather Cage Sanda'),
        startSize: 5,
        endSize: 12,
        price: 89000,
        colors: [idGoldColor],
        sizes: [{
                color: idGoldColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idGoldColor,
                images: images29.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idGoldColor,
                prices: [
                    { size: '5', price: 89000 },
                    { size: '5.5', price: 89000 },
                    { size: '6', price: 89000 },
                    { size: '6.5', price: 89000 },
                    { size: '7', price: 89000 },
                    { size: '8', price: 89000 },
                    { size: '9', price: 89000 },
                    { size: '10', price: 89000 },
                    { size: '11', price: 89000 },
                    { size: '12', price: 89000 },
                ],
            }],
        brand: idBrandFrancesco,
        category: idCategoryShoes,
        subCategory: idSubCategorySandals
    };
    products.push(product29);
    var images30 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1S502202200_1.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1S502202200_2.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1S502202200_3.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1S502202200_4.jpg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'R1S502202200_5.jpg' }),
    ];
    var product30 = {
        key: uuid_1.v4(),
        name: 'T-Strap Leather Stiletto Sandals',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Francesco Russo\'s patent leather t-strap sandal in classic black. Leg-lengthening 4" stiletto heels. Contrasting patent blush leather midsoles. Composition: 100% leather. In black. Made in Italy.',
        id: '',
        slug: createSlug('Francesco Russo', 'T-Strap Leather Stiletto Sandals'),
        startSize: 5,
        endSize: 12,
        price: 89000,
        colors: [idColorBlack],
        sizes: [{
                color: idColorBlack,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idColorBlack,
                images: images30.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idColorBlack,
                prices: [
                    { size: '5', price: 89000 },
                    { size: '5.5', price: 89000 },
                    { size: '6', price: 89000 },
                    { size: '6.5', price: 89000 },
                    { size: '7', price: 89000 },
                    { size: '8', price: 89000 },
                    { size: '9', price: 89000 },
                    { size: '10', price: 89000 },
                    { size: '11', price: 89000 },
                    { size: '12', price: 89000 },
                ],
            }],
        brand: idBrandFrancesco,
        category: idCategoryShoes,
        subCategory: idSubCategorySandals
    };
    products.push(product30);
    var images31 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'AURORABROWN_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'AURORABROWN_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'AURORABROWN_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'AURORABROWN_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'AURORABROWN_5.jpeg' }),
    ];
    var product31 = {
        key: uuid_1.v4(),
        name: 'Aurora Pointed Leather Flats',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Elegantly crafted in smooth brown calf leather, the label\'s Aurora flats feature sleek pointed square toes. Slip-on style.' +
            'Composition: leather' +
            'Made in Italy',
        id: '',
        slug: createSlug('AEYDE', 'Aurora Pointed Leather Flats'),
        startSize: 5,
        endSize: 12,
        price: 89000,
        colors: [idBrownColor],
        sizes: [{
                color: idBrownColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idBrownColor,
                images: images31.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idBrownColor,
                prices: [
                    { size: '5', price: 25800 },
                    { size: '5.5', price: 25800 },
                    { size: '6', price: 25800 },
                    { size: '6.5', price: 25800 },
                    { size: '7', price: 25800 },
                    { size: '8', price: 25800 },
                    { size: '9', price: 25800 },
                    { size: '10', price: 25800 },
                    { size: '11', price: 25800 },
                    { size: '12', price: 25800 },
                ],
            }],
        brand: idBrandAeyde,
        category: idCategoryShoes,
        subCategory: idSubCategoryFlats
    };
    products.push(product31);
    var images32 = [
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'TWIFLAA0NPGNN0_1.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'TWIFLAA0NPGNN0_2.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'TWIFLAA0NPGNN0_3.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'TWIFLAA0NPGNN0_4.jpeg' }),
        __assign(__assign({}, defaultImage), { key: uuid_1.v4(), filename: 'TWIFLAA0NPGNN0_5.jpeg' }),
    ];
    var product32 = {
        key: uuid_1.v4(),
        name: 'Mondaine Twisted Leather Flat Slides',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Aquazzura twists their signature Mondaine flats in smooth leather with pointed faille cap toes. ' +
            'Composition: leather, polyester' +
            'Made in Italy',
        id: '',
        slug: createSlug('Aquazzura', 'Mondaine Twisted Leather Flat Slides'),
        startSize: 5,
        endSize: 12,
        price: 69500,
        colors: [idBeigeColor],
        sizes: [{
                color: idBeigeColor,
                sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
            }],
        images: [{
                color: idBeigeColor,
                images: images32.map(function (image) { return image.key; })
            }],
        prices: [{
                color: idBeigeColor,
                prices: [
                    { size: '5', price: 69500 },
                    { size: '5.5', price: 69500 },
                    { size: '6', price: 69500 },
                    { size: '6.5', price: 69500 },
                    { size: '7', price: 69500 },
                    { size: '8', price: 69500 },
                    { size: '9', price: 69500 },
                    { size: '10', price: 69500 },
                    { size: '11', price: 69500 },
                    { size: '12', price: 69500 },
                ],
            }],
        brand: idBrandAquazzura,
        category: idCategoryShoes,
        subCategory: idSubCategoryFlats
    };
    products.push(product32);
    var images = __spreadArrays(images1, images2, images3, images4, images5, images6, images7, images8, images9, images10, images11, images12, images13, images14, images15, images16, images17, images18, images19, images20, images21, images22, images23, images24, images25, images26, images27, images28, images29, images30, images31, images32);
    var cartItems = [];
    var cartItemId1 = uuid_1.v4();
    cartItems.push({
        key: cartItemId1,
        quantity: 1,
        product: keyProduct1,
        color: idColorBlack,
        size: '11',
        price: 65000,
        total: 65000,
        user: userId
    });
    var cartItemId2 = uuid_1.v4();
    cartItems.push({
        key: cartItemId2,
        quantity: 1,
        product: keyProduct2,
        color: idHotPinkColor,
        size: '11',
        price: 99500,
        total: 99500,
        user: userId
    });
    var orders = [];
    var orderId = uuid_1.v4();
    var order = {
        key: orderId,
        id: 1,
        user: userId,
        total: 65000 + 99500,
        stripeId: "123131321231321232",
        date: moment_1.default().valueOf().toString(),
        timeStamp: moment_1.default().valueOf().toString()
    };
    orders.push(order);
    var orderItems = [];
    var orderItem1 = {
        key: uuid_1.v4(),
        quantity: 1,
        color: idColorBlack,
        size: '11',
        price: 65000,
        total: 65000,
        user: userId,
        product: keyProduct1,
        productName: 'Romy 85 Patent Leather Pumps',
        order: orderId,
        cartItem: cartItemId1,
        timeStamp: moment_1.default().valueOf().toString()
    };
    orderItems.push(orderItem1);
    var orderItem2 = {
        key: uuid_1.v4(),
        quantity: 1,
        color: idHotPinkColor,
        size: '11',
        price: 99500,
        total: 99500,
        user: userId,
        product: keyProduct2,
        productName: 'Hangisi Crystal Satin Pumps',
        order: orderId,
        cartItem: cartItemId2,
        timeStamp: moment_1.default().valueOf().toString()
    };
    orderItems.push(orderItem2);
    return {
        categories: categories,
        subcategories: subcategories,
        brands: brands,
        products: products,
        colors: colors,
        images: images,
        users: users,
        cartItems: cartItems,
        orders: orders,
        orderItems: orderItems
    };
}
exports.default = testData;
