"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("uuid");
var moment_1 = __importDefault(require("moment"));
var TestData_1 = __importDefault(require("./TestData"));
var fs_1 = __importDefault(require("fs"));
var bcrypt_1 = __importDefault(require("bcrypt"));
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var apollo_server_errors_1 = require("apollo-server-errors");
var stripe_1 = __importDefault(require("../lib/stripe"));
var LocalData = (function () {
    function LocalData() {
        if (fs_1.default.existsSync("./testfile.json")) {
            var fileContents = fs_1.default.readFileSync("./testfile.json", { encoding: 'utf8', flag: 'r' });
            var data = JSON.parse(fileContents);
            this.categories = data.categories;
            this.subcategories = data.subcategories;
            this.brands = data.brands;
            this.products = data.products;
            this.colors = data.colors;
            this.files = data.files;
            this.users = data.users;
            this.cartItems = data.cartItems;
            this.orders = data.orders;
            this.orderItems = data.orderItems;
            console.log(this.categories.length);
            console.log(this.subcategories.length);
            console.log(this.brands.length);
            console.log(this.products.length);
            console.log(this.colors.length);
            console.log(this.files.length);
            console.log(this.cartItems.length);
        }
        else {
            var data = TestData_1.default();
            this.categories = data.categories;
            this.subcategories = data.subcategories;
            this.brands = data.brands;
            this.products = data.products;
            this.colors = data.colors;
            this.files = data.images;
            this.users = data.users;
            this.cartItems = data.cartItems;
            this.orders = data.orders;
            this.orderItems = data.orderItems;
        }
    }
    LocalData.getInstance = function () {
        if (!LocalData.instance) {
            LocalData.instance = new LocalData();
        }
        return LocalData.instance;
    };
    LocalData.prototype.emptyCategory = function () {
        return {
            key: '',
            description: '',
            timeStamp: '',
            slug: '',
        };
    };
    LocalData.prototype.getCategories = function () {
        return this.categories;
    };
    LocalData.prototype.getCategory = function (key) {
        return this.categories.find(function (category) { return category.key === key; }) || this.emptyCategory();
    };
    LocalData.prototype.getCategoryBySlug = function (slug) {
        return this.categories.find(function (category) { return category.slug === slug; }) || this.emptyCategory();
    };
    LocalData.prototype.addCategory = function (description, slug) {
        var category = {
            key: uuid_1.v4(),
            description: description,
            slug: slug,
            timeStamp: moment_1.default().millisecond() + ''
        };
        this.categories.push(category);
        return category;
    };
    LocalData.prototype.emptySubCategory = function () {
        return {
            key: '',
            description: '',
            slug: '',
            timeStamp: '',
            categoryKey: ''
        };
    };
    LocalData.prototype.getSubCategories = function () {
        return this.subcategories;
    };
    LocalData.prototype.getSubCategory = function (key) {
        return this.subcategories.find(function (subcategory) { return subcategory.key === key; }) || this.emptySubCategory();
    };
    LocalData.prototype.getSubCategoryBySlug = function (slug) {
        return this.subcategories.find(function (subcategory) { return subcategory.slug === slug; }) || this.emptySubCategory();
    };
    LocalData.prototype.addSubCategory = function (description, slug, categoryKey) {
        var subcategory = {
            key: uuid_1.v4(),
            description: description,
            slug: slug,
            timeStamp: moment_1.default().milliseconds() + '',
            categoryKey: categoryKey
        };
        this.subcategories.push(subcategory);
        return subcategory;
    };
    LocalData.prototype.getSubCategoriesCategory = function (categoryKey) {
        return this.subcategories.filter(function (subcategory) { return subcategory.categoryKey === categoryKey; }) || this.emptySubCategory();
    };
    LocalData.prototype.emptyBrand = function () {
        return {
            key: '',
            description: '',
            slug: '',
            timeStamp: '',
        };
    };
    LocalData.prototype.getBrands = function () {
        return this.brands;
    };
    LocalData.prototype.getBrand = function (key) {
        return this.brands.find(function (brand) { return brand.key === key; }) || this.emptyBrand();
    };
    LocalData.prototype.getBrandBySlug = function (slug) {
        return this.brands.find(function (brand) { return brand.slug === slug; }) || this.emptyBrand();
    };
    LocalData.prototype.addBrand = function (description, slug) {
        var brand = {
            key: uuid_1.v4(),
            description: description,
            slug: slug,
            timeStamp: moment_1.default().milliseconds() + '',
        };
        this.brands.push(brand);
        return brand;
    };
    LocalData.prototype.emptyColor = function () {
        return {
            key: '',
            name: '',
            hex: '',
        };
    };
    LocalData.prototype.getColors = function () {
        return this.colors;
    };
    LocalData.prototype.getColor = function (key) {
        return this.colors.find(function (color) { return color.key === key; }) || this.emptyColor();
    };
    LocalData.prototype.addColor = function (name, hex) {
        var color = {
            key: uuid_1.v4(),
            name: name,
            hex: hex
        };
        this.colors.push(color);
        return color;
    };
    LocalData.prototype.emptyFile = function () {
        return {
            key: '',
            mimetype: '',
            encoding: '',
            timeStamp: moment_1.default().milliseconds() + '',
            filename: ''
        };
    };
    LocalData.prototype.getFiles = function () {
        return this.files;
    };
    LocalData.prototype.getFile = function (key) {
        return this.files.find(function (file) { return file.key === key; }) || this.emptyFile();
    };
    LocalData.prototype.addFile = function (filename, mimetype, encoding) {
        var file = {
            key: uuid_1.v4(),
            filename: filename,
            mimetype: mimetype,
            encoding: encoding,
            timeStamp: moment_1.default().milliseconds() + '',
        };
        this.files.push(file);
        return file;
    };
    LocalData.prototype.emptyProduct = function () {
        return {
            key: uuid_1.v4(),
            name: '',
            description: '',
            sizeAndFit: '',
            details: '',
            colors: [''],
            images: [{ color: '', images: [] }],
            timeStamp: '',
            brand: '',
            category: '',
            subCategory: '',
            id: '',
            slug: '',
            startSize: 0,
            endSize: 0,
            price: 0,
        };
    };
    LocalData.prototype.getProducts = function (category, subCategory, brands, colors, sizes, skip, first) {
        var _this = this;
        console.log("parameters ", category, subCategory, brands, colors, sizes, skip, first);
        var result = this.products;
        if (subCategory && Array.isArray(subCategory) && subCategory.length) {
            var subCategoryKeys_1 = subCategory.map(function (subCat) { return _this.getSubCategoryBySlug(subCat).key; });
            result = result.filter(function (product) {
                return subCategoryKeys_1.includes(product.subCategory);
            });
        }
        else if (category && Array.isArray(category) && category.length) {
            var categoryKeys_1 = category.map(function (cat) { return _this.getCategoryBySlug(cat).key; });
            result = result.filter(function (product) {
                return categoryKeys_1.includes(product.category);
            });
        }
        if (brands && Array.isArray(brands) && brands.length) {
            var brandKeys_1 = brands.map(function (brand) { return _this.getBrandBySlug(brand).key; });
            result = result.filter(function (product) { return brandKeys_1.includes(product.brand); });
        }
        if (colors && Array.isArray(colors) && colors.length) {
            result = result.filter(function (product) {
                var intersection = product.colors.filter(function (color) { return colors.includes(color); });
                return intersection.length;
            });
        }
        if (sizes && Array.isArray(sizes) && sizes.length) {
            result = result.filter(function (product) {
                var _a;
                var s = (_a = product.sizes) === null || _a === void 0 ? void 0 : _a.filter(function (size) {
                    var intersection = sizes.filter(function (sizeTofind) { return size.sizes.includes(sizeTofind); });
                    return intersection.length;
                });
                return s === null || s === void 0 ? void 0 : s.length;
            });
        }
        if (first !== undefined && skip !== undefined && skip >= 0 && first >= 1) {
            result = result.slice(skip, skip + first);
        }
        console.log('length', result.length);
        return result;
    };
    LocalData.prototype.aggregations = function (category, subCategory, brands, colors, sizes) {
        var _this = this;
        var results = this.getProducts(category, subCategory, brands, colors, sizes, -1, -1);
        var temp = results.reduce(function (aggs, product) {
            aggs.count += 1;
            var aggCat = aggs.categories.find(function (cat) { return cat.category === product.category; });
            if (!aggCat) {
                aggs.categories.push({
                    category: product.category,
                    subCategories: [product.subCategory]
                });
            }
            else {
                if (!aggCat.subCategories.includes(product.subCategory)) {
                    aggCat.subCategories.push(product.subCategory);
                }
            }
            if (!aggs.brands.includes(product.brand))
                aggs.brands.push(product.brand);
            product.colors.forEach(function (color) {
                if (!aggs.colors.includes(color)) {
                    aggs.colors.push(color);
                }
            });
            product.sizes.forEach(function (size) {
                size.sizes.forEach(function (sizeProduct) {
                    if (!aggs.sizes.includes(sizeProduct)) {
                        aggs.sizes.push(sizeProduct);
                    }
                });
            });
            return aggs;
        }, { count: 0, colors: [], brands: [], sizes: [], categories: [] });
        var categories = [];
        temp.categories.forEach(function (aggCat) {
            categories.push({
                category: _this.categories.find(function (cat) { return cat.key === aggCat.category; }) || null,
                subCategories: aggCat.subCategories.map(function (aggSubCat) { return _this.subcategories.find(function (subCat) { return subCat.key === aggSubCat; }); })
            });
        });
        var aggregations = {
            count: temp.count,
            colors: temp.colors.map(function (colorKey) { return _this.colors.find(function (color) { return color.key === colorKey; }); }),
            brands: temp.brands.map(function (brandKey) { return _this.brands.find(function (brand) { return brand.key === brandKey; }); }),
            categories: categories,
            sizes: temp.sizes,
        };
        return aggregations;
    };
    LocalData.prototype.getProduct = function (key) {
        return this.products.find(function (product) { return product.key === key; }) || this.emptyProduct();
    };
    LocalData.prototype.addProduct = function (productInput) {
        var _a;
        var categoryKey = ((_a = this.subcategories.find(function (subcategory) { return subcategory.key === productInput.subCategory; })) === null || _a === void 0 ? void 0 : _a.categoryKey) || '';
        var product = __assign(__assign({}, productInput), { key: uuid_1.v4(), category: categoryKey, timeStamp: moment_1.default().milliseconds() + '' });
        this.products.push(product);
        this.saveTestFile();
        return product;
    };
    LocalData.prototype.updateProduct = function (key, productInput) {
        var _a;
        var categoryKey = ((_a = this.subcategories.find(function (subcategory) { return subcategory.key === productInput.subCategory; })) === null || _a === void 0 ? void 0 : _a.categoryKey) || '';
        var product = this.getProduct(key);
        var updatedProduct = __assign(__assign(__assign({}, product), productInput), { key: key, category: categoryKey, timeStamp: moment_1.default().milliseconds() + '' });
        var index = this.products.findIndex(function (prod) { return prod.key === key; });
        this.products[index] = updatedProduct;
        this.saveTestFile();
        return updatedProduct;
    };
    LocalData.prototype.deleteProduct = function (key) {
        var index = this.products.findIndex(function (prod) { return prod.key === key; });
        var product = this.products[index];
        this.products.splice(index, 1);
        this.saveTestFile();
        return product;
    };
    LocalData.prototype.saveTestFile = function () {
        var testData = {
            categories: this.categories,
            subcategories: this.subcategories,
            brands: this.brands,
            products: this.products,
            colors: this.colors,
            files: this.files
        };
        fs_1.default.writeFileSync('testfile.json', JSON.stringify(testData));
    };
    LocalData.prototype.emptyUser = function () {
        return {
            id: '',
            name: '',
            email: '',
            password: '',
            timeStamp: moment_1.default().milliseconds() + ''
        };
    };
    LocalData.prototype.getUser = function (id) {
        return this.users.find(function (user) { return user.id === id; }) || this.emptyUser();
    };
    LocalData.prototype.getUserByUserName = function (name) {
        return this.users.find(function (user) { return user.name === name; }) || this.emptyUser();
    };
    LocalData.prototype.getUserByEmail = function (email) {
        return this.users.find(function (user) { return user.email === email; });
    };
    LocalData.prototype.createToken = function (user, secret, expiresIn) {
        var id = user.id, email = user.email, name = user.name;
        return jsonwebtoken_1.default.sign({ id: id, email: email, name: name }, secret, { expiresIn: expiresIn });
    };
    LocalData.prototype.generatePasswordHash = function (password) {
        var saltRounds = 10;
        var hash = bcrypt_1.default.hashSync(password, saltRounds);
        return hash;
    };
    LocalData.prototype.validatePassword = function (loginPassword, password) {
        return bcrypt_1.default.compareSync(loginPassword, password);
    };
    LocalData.prototype.signUp = function (name, email, password, secret) {
        var user = {
            id: uuid_1.v4(),
            name: name,
            email: email,
            password: this.generatePasswordHash(password),
            timeStamp: moment_1.default().milliseconds() + ''
        };
        this.users.push(user);
        return {
            token: this.createToken(user, secret, '30m')
        };
    };
    LocalData.prototype.signIn = function (email, password, secret) {
        var user = this.getUserByEmail(email);
        console.log(user);
        if (!user)
            throw new apollo_server_errors_1.UserInputError('Invalid user');
        var isValid = this.validatePassword(password, user.password);
        console.log(isValid);
        if (!isValid)
            throw new apollo_server_errors_1.AuthenticationError('Invalid password');
        var token = this.createToken(user, secret, '240m');
        return {
            token: token
        };
    };
    LocalData.prototype.getCartItem = function (key) {
        return this.cartItems.find(function (cartItem) { return cartItem.key === key; });
    };
    LocalData.prototype.getItemsUser = function (userKey) {
        console.log("getItemsUser ", userKey);
        return this.cartItems.filter(function (cartItem) { return cartItem.user === userKey; });
    };
    LocalData.prototype.addCartItem = function (quantity, color, size, productKey, userKey) {
        var product = this.getProduct(productKey);
        var cartItem = this.cartItems.find(function (cartItem) { return cartItem.product === productKey; });
        if (cartItem && cartItem.color === color && cartItem.size === size) {
            cartItem.quantity = cartItem.quantity + quantity;
            return cartItem;
        }
        else {
            var newCartItem = {
                key: uuid_1.v4(),
                quantity: quantity,
                color: color,
                size: size,
                price: product.price,
                total: quantity * product.price,
                user: userKey,
                product: productKey
            };
            this.cartItems.push(newCartItem);
            return newCartItem;
        }
    };
    LocalData.prototype.removeCartItem = function (key) {
        var index = this.cartItems.findIndex(function (cartItem) { return cartItem.key === key; });
        if (index !== -1) {
            var cartItem = this.cartItems[index];
            this.cartItems.splice(index, 1);
            return cartItem;
        }
        return null;
    };
    LocalData.prototype.searchProducts = function (term) {
        return this.products.filter(function (product) { return product.name.toLowerCase().includes(term) ||
            product.description.toLowerCase().includes(term) ||
            product.details.toLowerCase().includes(term); });
    };
    LocalData.prototype.getOrders = function () {
        return this.orders;
    };
    LocalData.prototype.getOrder = function (key) {
        return this.orders.find(function (order) { return order.key === key; });
    };
    LocalData.prototype.getOrdersByUser = function (user) {
        return this.orders.filter(function (order) { return order.user === user; });
    };
    LocalData.prototype.nextOrderId = function () {
        return this.orders.reduce(function (nextId, v) {
            return (nextId > v.id ? nextId : v.id);
        }, 0);
    };
    LocalData.prototype.addOrder = function (total, user, stripeId) {
        var order = {
            key: uuid_1.v4(),
            id: this.nextOrderId() + 1,
            total: total,
            user: user,
            stripeId: stripeId,
            date: moment_1.default().valueOf().toString(),
            timeStamp: moment_1.default().valueOf().toString(),
        };
        this.orders.push(order);
        return order;
    };
    LocalData.prototype.getOrderItem = function (key) {
        return this.orderItems.find(function (orderItem) { return orderItem.key === key; });
    };
    LocalData.prototype.getOrderItemByOrder = function (orderKey) {
        return this.orderItems.filter(function (orderItem) { return orderItem.order === orderKey; });
    };
    LocalData.prototype.addOrderItem = function (input) {
        var product = this.getProduct(input.product);
        var orderItem = __assign(__assign({}, input), { productName: product.name, key: uuid_1.v4(), timeStamp: moment_1.default().valueOf().toString() });
        this.orderItems.push(orderItem);
        return orderItem;
    };
    LocalData.prototype.checkOut = function (stripeId, user) {
        return __awaiter(this, void 0, void 0, function () {
            var cartItems, total, charge, order;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        cartItems = this.getItemsUser(user.id);
                        total = cartItems.reduce(function (total, item) { return total + item.total; }, 0);
                        return [4, stripe_1.default.paymentIntents.create({
                                amount: total,
                                currency: 'usd',
                                payment_method: stripeId,
                                confirm: true,
                            }).catch(function (err) {
                                console.log(err);
                                throw new Error(err.message);
                            })];
                    case 1:
                        charge = _a.sent();
                        console.log(charge);
                        order = this.addOrder(total, user.id, charge.id);
                        cartItems.forEach(function (cartItem) {
                            var input = {
                                quantity: cartItem.quantity,
                                size: cartItem.size,
                                price: cartItem.price,
                                color: cartItem.color,
                                total: cartItem.total,
                                product: cartItem.product,
                                order: order.key,
                                cartItem: cartItem.key,
                                user: user.id
                            };
                            _this.addOrderItem(input);
                        });
                        cartItems.forEach(function (cartItem) {
                            _this.removeCartItem(cartItem.key);
                        });
                        return [2, order];
                }
            });
        });
    };
    return LocalData;
}());
exports.default = LocalData;
