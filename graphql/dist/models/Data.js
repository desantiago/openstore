"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var LocalData_1 = __importDefault(require("./LocalData"));
var Data = (function () {
    function Data() {
        this.d = LocalData_1.default.getInstance();
    }
    Data.getInstance = function () {
        if (!Data.instance) {
            Data.instance = new Data();
        }
        return Data.instance;
    };
    Data.prototype.getCategories = function () {
        return this.d.getCategories();
    };
    Data.prototype.getCategory = function (key) {
        return this.d.getCategory(key);
    };
    Data.prototype.getCategoryBySlug = function (slug) {
        return this.d.getCategoryBySlug(slug);
    };
    Data.prototype.addCategory = function (description, slug) {
        return this.d.addCategory(description, slug);
    };
    Data.prototype.getSubCategories = function () {
        console.log('getSubCategories');
        return this.d.getSubCategories();
    };
    Data.prototype.getSubCategory = function (key) {
        return this.d.getSubCategory(key);
    };
    Data.prototype.getSubCategoryBySlug = function (slug) {
        return this.d.getSubCategoryBySlug(slug);
    };
    Data.prototype.addSubCategory = function (description, slug, categoryKey) {
        return this.d.addSubCategory(description, slug, categoryKey);
    };
    Data.prototype.getSubCategoriesCategory = function (categoryKey) {
        return this.d.getSubCategoriesCategory(categoryKey);
    };
    Data.prototype.getBrands = function () {
        return this.d.getBrands();
    };
    Data.prototype.getBrand = function (key) {
        return this.d.getBrand(key);
    };
    Data.prototype.getBrandBySlug = function (slug) {
        return this.d.getBrandBySlug(slug);
    };
    Data.prototype.addBrand = function (description, slug) {
        return this.d.addBrand(description, slug);
    };
    Data.prototype.getColors = function () {
        return this.d.getColors();
    };
    Data.prototype.getColor = function (key) {
        return this.d.getColor(key);
    };
    Data.prototype.addColor = function (name, hex) {
        return this.d.addColor(name, hex);
    };
    Data.prototype.getFiles = function () {
        return this.d.getFiles();
    };
    Data.prototype.getFile = function (key) {
        return this.d.getFile(key);
    };
    Data.prototype.addFile = function (filename, mimetype, encondig) {
        return this.d.addFile(filename, mimetype, encondig);
    };
    Data.prototype.getProducts = function (category, subcategory, brands, colors, sizes, skip, first) {
        return this.d.getProducts(category, subcategory, brands, colors, sizes, skip, first);
    };
    Data.prototype.aggregations = function (category, subcategory, brands, colors, sizes) {
        return this.d.aggregations(category, subcategory, brands, colors, sizes);
    };
    Data.prototype.getProduct = function (key) {
        return this.d.getProduct(key);
    };
    Data.prototype.addProduct = function (productInput) {
        return this.d.addProduct(productInput);
    };
    Data.prototype.updateProduct = function (key, productInput) {
        return this.d.updateProduct(key, productInput);
    };
    Data.prototype.deleteProduct = function (key) {
        return this.d.deleteProduct(key);
    };
    Data.prototype.getUser = function (id) {
        return this.d.getUser(id);
    };
    Data.prototype.signUp = function (name, email, password, secret) {
        return this.d.signUp(name, email, password, secret);
    };
    Data.prototype.signIn = function (email, password, secret, cookies) {
        return this.d.signIn(email, password, secret);
    };
    Data.prototype.getCartItem = function (key) {
        return this.d.getCartItem(key);
    };
    Data.prototype.getItemsUser = function (userKey) {
        return this.d.getItemsUser(userKey);
    };
    Data.prototype.addCartItem = function (quantity, color, size, productKey, userKey) {
        return this.d.addCartItem(quantity, color, size, productKey, userKey);
    };
    Data.prototype.removeCartItem = function (key) {
        return this.d.removeCartItem(key);
    };
    Data.prototype.searchProducts = function (key) {
        return this.d.searchProducts(key);
    };
    Data.prototype.getOrders = function () {
        return this.d.getOrders();
    };
    Data.prototype.getOrder = function (key) {
        return this.d.getOrder(key);
    };
    Data.prototype.addOrder = function (total, user, stripeId) {
        return this.d.addOrder(total, user, stripeId);
    };
    Data.prototype.getOrderItem = function (key) {
        return this.d.getOrderItem(key);
    };
    Data.prototype.getOrderItemByOrder = function (orderKey) {
        return this.d.getOrderItemByOrder(orderKey);
    };
    Data.prototype.addOrderItem = function (input) {
        return this.d.addOrderItem(input);
    };
    Data.prototype.checkOut = function (stripeId, user) {
        return this.d.checkOut(stripeId, user);
    };
    return Data;
}());
exports.default = Data;
