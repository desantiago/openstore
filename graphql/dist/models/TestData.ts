import {
    Category,
    SubCategory,
    Brand,
    Product,
    CartItem,
    Order,
    OrderItem
} from './types';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';
import bcrypt from 'bcrypt';

function createSlug(brand: string, name: string): string {
    const words = [...brand.toLocaleLowerCase().split(' '), ...name.toLocaleLowerCase().split(' ')].flat();
    return words.join('-');
}

function generatePasswordHash(password: string): string {
    const saltRounds = 10;
    const hash = bcrypt.hashSync(password, saltRounds);
    return hash;
}

export default function testData() {
    const idCategoryShoes = uuidv4();
    const idCategoryBags = uuidv4();

    const userId = uuidv4();
    console.log(userId);
    let users = [];
    users.push({
        id: userId,
        name: 'desantiago',
        email: 'desantiagod@gmail.com',
        password: generatePasswordHash('password'),
        timeStamp: moment().valueOf().toString()
    });

    let categories = [];
    categories.push({
        key: idCategoryShoes,
        description: 'Shoes',
        slug: 'shoes',
        timeStamp: moment().valueOf().toString()
    });
    categories.push({
        key: idCategoryBags,
        description: 'Bags',
        slug: 'bags',
        timeStamp: moment().valueOf().toString()
    });

    const idSubCategoryPumps = uuidv4();
    let subcategories = [];
    subcategories.push({
        key: idSubCategoryPumps,
        description: 'Pumps',
        slug: 'pumps',
        timeStamp: '1',
        categoryKey: idCategoryShoes
    });
    const idSubCategoryFlats = uuidv4();
    subcategories.push({
        key: idSubCategoryFlats,
        description: 'Flats',
        slug: 'flats',
        timeStamp: '1',
        categoryKey: idCategoryShoes
    });
    const idSubCategoryBoots = uuidv4();
    subcategories.push({
        key: idSubCategoryBoots,
        description: 'Boots & Booties',
        slug: 'boots',
        timeStamp: '1',
        categoryKey: idCategoryShoes
    });
    const idSubCategorySandals = uuidv4();
    subcategories.push({
        key: idSubCategorySandals,
        description: 'Sandals',
        slug: 'sandals',
        timeStamp: '1',
        categoryKey: idCategoryShoes
    });

    const idSubCategoryBeltBags = uuidv4();
    subcategories.push({
        key: idSubCategoryBeltBags,
        description: 'Belt Bags',
        slug: 'beltbags',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    const idSubCategoryBucketBags = uuidv4();
    subcategories.push({
        key: idSubCategoryBucketBags,
        description: 'Bucket Bags',
        slug: 'bucketbags',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    const idSubCategoryClutches = uuidv4();
    subcategories.push({
        key: idSubCategoryClutches,
        description: 'Clutches',
        slug: 'clutches',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    const idSubCategoryCrossbody = uuidv4();
    subcategories.push({
        key: idSubCategoryCrossbody,
        description: 'Crossbody',
        slug: 'crossbody',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    const idSubCategoryMiniBags = uuidv4();
    subcategories.push({
        key: idSubCategoryMiniBags,
        description: 'Mini Bags',
        slug: 'minibags',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    const idSubCategoryShoulder = uuidv4();
    subcategories.push({
        key: idSubCategoryShoulder,
        description: 'Shoulder',
        slug: 'shoulder',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });
    const idSubCategoryTotes = uuidv4();
    subcategories.push({
        key: idSubCategoryTotes,
        description: 'Totes',
        slug: 'totes',
        timeStamp: '1',
        categoryKey: idCategoryBags
    });

    let brands = [];
    const idBrandAeyde = uuidv4();
    brands.push({
        key: idBrandAeyde,
        description: 'AEYDE',
        slug: 'aeyde',
        timeStamp: '1'
    });
    const idBrandAlexandreBirman = uuidv4();
    brands.push({
        key: idBrandAlexandreBirman,
        description: 'Alexandre Birman',
        slug: 'alexandrebirman',
        timeStamp: '1'
    });
    const idBrandAlexander = uuidv4();
    brands.push({
        key: idBrandAlexander,
        description: 'Alexander McQueen',
        slug: 'alexandermcqueen',
        timeStamp: '1'
    });
    const idBrandAlexandreVauthier = uuidv4();
    brands.push({
        key: idBrandAlexandreVauthier,
        description: 'Alexandre Vauthier',
        slug: 'alexandrevauthier',
        timeStamp: '1'
    });
    const idBrandAlexanderWang = uuidv4();
    brands.push({
        key: idBrandAlexanderWang,
        description: 'Alexander Wang',
        slug: 'alexanderwang',
        timeStamp: '1'
    });
    const idBrandAmina = uuidv4();
    brands.push({
        key: idBrandAmina,
        description: 'Ammina Muaddi',
        slug: 'amminamuaddi',
        timeStamp: '1'
    });
    const idBrandAquazzura = uuidv4();
    brands.push({
        key: idBrandAquazzura,
        description: 'Aquazzura',
        slug: 'aquazzura',
        timeStamp: '1'
    });
    const idBrandBalmain = uuidv4();
    brands.push({
        key: idBrandBalmain,
        description: 'Balmain',
        slug: 'balmain',
        timeStamp: '1'
    });
    const idBrandByFar = uuidv4();
    brands.push({
        key: idBrandByFar,
        description: 'By Far',
        slug: 'byfar',
        timeStamp: '1'
    });
    const idBrandCultGaia = uuidv4();
    brands.push({
        key: idBrandCultGaia,
        description: 'Cult Gaia',
        slug: 'cultgaia',
        timeStamp: '1'
    });
    const idBrandFrancesco = uuidv4();
    brands.push({
        key: idBrandFrancesco,
        description: 'Francesco Russo',
        slug: 'francescorusso',
        timeStamp: '1'
    });
    const idBrandGia = uuidv4();
    brands.push({
        key: idBrandGia,
        description: 'Gia',
        slug: 'gia',
        timeStamp: '1'
    });
    const idBrandIsabelMarant = uuidv4();
    brands.push({
        key: idBrandIsabelMarant,
        description: 'Isabel Marant',
        slug: 'isabelmarant',
        timeStamp: '1'
    });
    const idBrandJimmyChoo = uuidv4();
    brands.push({
        key: idBrandJimmyChoo,
        description: 'Jimmy Choo',
        slug: 'jimmychoo',
        timeStamp: '1'
    });
    const idBrandManolo = uuidv4();
    brands.push({
        key: idBrandManolo,
        description: 'Manolo Blahnik',
        slug: 'manoloblahnik',
        timeStamp: '1'
    });
    const idBrandMarkCross = uuidv4();
    brands.push({
        key: idBrandMarkCross,
        description: 'Mark Cross',
        slug: 'markcross',
        timeStamp: '1'
    });
    const idBrandMarni = uuidv4();
    brands.push({
        key: idBrandMarni,
        description: 'Marni',
        slug: 'marni',
        timeStamp: '1'
    });
    const idBrandPacoRabanne = uuidv4();
    brands.push({
        key: idBrandPacoRabanne,
        description: 'Paco Rabanne',
        slug: 'pacorabanne',
        timeStamp: '1'
    });
    const idBrandProenza = uuidv4();
    brands.push({
        key: idBrandProenza,
        description: 'Proenza Schouler',
        slug: 'proenzaschouler',
        timeStamp: '1'
    });
    const idBrandStaud = uuidv4();
    brands.push({
        key: idBrandStaud,
        description: 'Staud',
        slug: 'staud',
        timeStamp: '1'
    });
    const idBrandValentino = uuidv4();
    brands.push({
        key: idBrandValentino,
        description: 'Valentino Garavani',
        slug: 'valentinogaravani',
        timeStamp: '1'
    });
    const idBrandWandler = uuidv4();
    brands.push({
        key: idBrandWandler,
        description: 'Wandler',
        slug: 'wandler',
        timeStamp: '1'
    });

    let colors = [];
    const idColorBlack = uuidv4();
    colors.push({
        key: idColorBlack,
        name: 'Black',
        hex: '#000'
    });
    const idHotPinkColor = uuidv4();
    colors.push({
        key: idHotPinkColor,
        name: 'Hot Pink',
        hex: '#000'
    });
    const idWhiteColor = uuidv4();
    colors.push({
        key: idWhiteColor,
        name: 'White',
        hex: '#fff'
    });
    const idBeigeColor = uuidv4();
    colors.push({
        key: idBeigeColor,
        name: 'Beige',
        hex: '#fff'
    });
    const idBrownColor = uuidv4();
    colors.push({
        key: idBrownColor,
        name: 'Brown',
        hex: '#fff'
    });
    const idGoldColor = uuidv4();
    colors.push({
        key: idGoldColor,
        name: 'Gold',
        hex: '#fff'
    });
    const idPinkColor = uuidv4();
    colors.push({
        key: idPinkColor,
        name: 'Pink',
        hex: '#fff'
    });
    const idOrangeColor = uuidv4();
    colors.push({
        key: idOrangeColor,
        name: 'Orange',
        hex: '#fff'
    });
    const idMultiColor = uuidv4();
    colors.push({
        key: idMultiColor,
        name: 'Multi',
        hex: '#fff'
    });
    const idSilverColor = uuidv4();
    colors.push({
        key: idSilverColor,
        name: 'Silver',
        hex: '#fff'
    });

    //const idSilverColor = uuidv4();

    const defaultImage = {
        mimetype: 'image/jpeg',
        encoding: '7bit',
        timeStamp: moment().milliseconds() + ''
    }

    const images1 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ROMY85PAT_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ROMY85PAT_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ROMY85PAT_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ROMY85PAT_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ROMY85PAT_5.jpg',
        },
    ];

    const keyProduct1 = uuidv4()
    let products = [];
    const product1: Product = {
        key: keyProduct1,
        name: 'Romy 85 Patent Leather Pumps',
        description: "Crafted in Italy, the label's pointed toe Romy pumps are timeless in black patent leather. This sleek pair works equally well with cocktail dresses and denim. 3.3\" stiletto heels. 100% leather. In black. Made in Italy.",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: '',
        id: '',
        slug: createSlug('Jimmy Choo', 'Romy 85 Patent Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 65000,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images1.map(image => image.key)
            // images: ['intermix/ROMY85PAT_1.jpg', 'intermix/ROMY85PAT_2.jpg', 'intermix/ROMY85PAT_3.jpg', 'intermix/ROMY85PAT_4.jpg', 'intermix/ROMY85PAT_5.jpg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 65000 },
                { size: '5.5', price: 65000 },
                { size: '6', price: 65000 },
                { size: '6.5', price: 65000 },
                { size: '7', price: 65000 },
                { size: '8', price: 65000 },
                { size: '9', price: 65000 },
                { size: '10', price: 65000 },
                { size: '11', price: 65000 },
                { size: '12', price: 65000 },
            ],
        }],
        brand: idBrandJimmyChoo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product1);

    const images2 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '9XX03186703_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '9XX03186703_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '9XX03186703_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '9XX03186703_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '9XX03186703_5.jpeg',
        },
    ];

    const keyProduct2 = uuidv4();
    const product2: Product = {
        key: keyProduct2,
        name: 'Hangisi Crystal Satin Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Detailed with their signature crystal buckles, Manolo Blahnik updates their cult-favorite Hangisi pointed toe heels in high shine satin.' +
            'Uppers: 68% viscose, 32% silk' +
            'Lining: 100% kid leather' +
            'Soles: 100% cow leather' +
            'Heel height: 4.1"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Manolo Blahnik', 'Hangisi Crystal Satin Pumps'),
        startSize: 5,
        endSize: 12,
        price: 99500,
        colors: [idHotPinkColor],
        sizes: [{
            color: idHotPinkColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idHotPinkColor,
            images: images2.map(image => image.key)
            //images: ['intermix/9XX03186703_1.jpeg', 'intermix/9XX03186703_2.jpeg', 'intermix/9XX03186703_3.jpeg', 'intermix/9XX03186703_4.jpeg', 'intermix/9XX03186703_5.jpeg']
        }],
        prices: [{
            color: idHotPinkColor,
            prices: [
                { size: '5', price: 99500 },
                { size: '5.5', price: 99500 },
                { size: '6', price: 99500 },
                { size: '6.5', price: 99500 },
                { size: '7', price: 99500 },
                { size: '8', price: 99500 },
                { size: '9', price: 99500 },
                { size: '10', price: 99500 },
                { size: '11', price: 99500 },
                { size: '12', price: 99500 },
            ],
        }],
        brand: idBrandManolo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product2);

    const images3 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '21CRTIFPWHNAP_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '21CRTIFPWHNAP_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '21CRTIFPWHNAP_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '21CRTIFPWHNAP_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '21CRTIFPWHNAP_5.jpeg',
        },
    ];

    const product3: Product = {
        key: uuidv4(),
        name: 'Tiffany Slingback Leather Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Marrying classic Mary Janes with your favorite slingbacks, the label\'s Tiffany stiletto pumps are an elegant essential with knife sharp pointed toes. Buckle straps.' +
            'Composition: leather' +
            'Heel height: 3.3"' +
            'Made in Italy',
        id: '',
        slug: createSlug('By Far', 'Tiffany Slingback Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 48000,
        colors: [idWhiteColor],
        sizes: [{
            color: idWhiteColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idWhiteColor,
            images: images3.map(image => image.key)
            // images: ['intermix/21CRTIFPWHNAP_1.jpeg', 'intermix/21CRTIFPWHNAP_2.jpeg', 'intermix/21CRTIFPWHNAP_3.jpeg', 'intermix/21CRTIFPWHNAP_4.jpeg', 'intermix/21CRTIFPWHNAP_5.jpeg']
        }],
        prices: [{
            color: idWhiteColor,
            prices: [
                { size: '5', price: 48000 },
                { size: '5.5', price: 48000 },
                { size: '6', price: 48000 },
                { size: '6.5', price: 48000 },
                { size: '7', price: 48000 },
                { size: '8', price: 48000 },
                { size: '9', price: 48000 },
                { size: '10', price: 48000 },
                { size: '11', price: 48000 },
                { size: '12', price: 48000 },
            ],
        }],
        brand: idBrandByFar,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product3);

    const images4 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '633501WHQVA_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '633501WHQVA_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '633501WHQVA_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '633501WHQVA_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '633501WHQVA_5.jpeg',
        },
    ];

    const product4: Product = {
        key: uuidv4(),
        name: 'Embellished Suede Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'McQueen revamps your go-to suede stilettos with sparkling crystal embellishments along pointed toes for a strong signature finish.' +
            'Composition: leather, brass' +
            'Wedge height: 3.5"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Alexander McQueen', 'Embellished Suede Pumps'),
        startSize: 5,
        endSize: 12,
        price: 99500,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images4.map(image => image.key)
            // images: ['intermix/633501WHQVA_1.jpeg', 'intermix/633501WHQVA_2.jpeg', 'intermix/633501WHQVA_3.jpeg', 'intermix/633501WHQVA_4.jpeg', 'intermix/633501WHQVA_5.jpeg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 99500 },
                { size: '5.5', price: 99500 },
                { size: '6', price: 99500 },
                { size: '6.5', price: 99500 },
                { size: '7', price: 99500 },
                { size: '8', price: 99500 },
                { size: '9', price: 99500 },
                { size: '10', price: 99500 },
                { size: '11', price: 99500 },
                { size: '12', price: 99500 },
            ],
        }],
        brand: idBrandAlexander,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product4);

    const images5 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '633501WHR721081_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '633501WHR721081_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '633501WHR721081_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '633501WHR721081_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '633501WHR721081_5.jpg',
        },
    ];

    const product5: Product = {
        key: uuidv4(),
        name: 'Spiked Suede Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'McQueen revamps your go-to suede stilettos with silver-tone spikes along pointed toes for a strong signature finish.' +
            'Composition: leather, brass' +
            'Heel height: 3.5"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Alexander McQueen', 'Spiked Suede Pumps'),
        startSize: 5,
        endSize: 12,
        price: 95000,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images5.map(image => image.key)
            // images: ['intermix/633501WHR721081_1.jpg', 'intermix/633501WHR721081_2.jpg', 'intermix/633501WHR721081_3.jpg', 'intermix/633501WHR721081_4.jpg', 'intermix/633501WHR721081_5.jpg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 95000 },
                { size: '5.5', price: 95000 },
                { size: '6', price: 95000 },
                { size: '6.5', price: 95000 },
                { size: '7', price: 95000 },
                { size: '8', price: 95000 },
                { size: '9', price: 95000 },
                { size: '10', price: 95000 },
                { size: '11', price: 95000 },
                { size: '12', price: 95000 },
            ],
        }],
        brand: idBrandAlexander,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product5);

    const images6 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'B3512301710001_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'B3512301710001_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'B3512301710001_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'B3512301710001_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'B3512301710001_5.jpg',
        },
    ];

    const product6: Product = {
        key: uuidv4(),
        name: 'Clarita 100 Suede Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'The label\'s signature Clarita 100 pumps are a go-to style in soft suede. Suede ankle ties. 3.9" stiletto heels. Leather uppers, lining and soles. Imported.',
        id: '',
        slug: createSlug('Alexander McQueen', 'Clarita 100 Suede Pumps'),
        startSize: 5,
        endSize: 12,
        price: 69500,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images6.map(image => image.key)
            // images: ['intermix/B3512301710001_1.jpg', 'intermix/B3512301710001_2.jpg', 'intermix/B3512301710001_3.jpg', 'intermix/B3512301710001_4.jpg', 'intermix/B3512301710001_5.jpg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 69500 },
                { size: '5.5', price: 69500 },
                { size: '6', price: 69500 },
                { size: '6.5', price: 69500 },
                { size: '7', price: 69500 },
                { size: '8', price: 69500 },
                { size: '9', price: 69500 },
                { size: '10', price: 69500 },
                { size: '11', price: 69500 },
                { size: '12', price: 69500 },
            ],
        }],
        brand: idBrandAlexander,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product6);

    const images7 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BBBLK_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BBBLK_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BBBLK_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BBBLK_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BBBLK_5.jpg',
        },
    ];

    const product7: Product = {
        key: uuidv4(),
        name: 'BB Snakeskin Embossed Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Manolo Blahnik\'s timeless BB pumps updated for the new season in a glossy snakeskin-embossed leather. Styled with pointed toes and 4" stiletto heels. Leather lining and soles. In black.  Made in Italy.',
        id: '',
        slug: createSlug('Manolo Blahnik', 'BB Snakeskin Embossed Pumps'),
        startSize: 5,
        endSize: 12,
        price: 93500,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images7.map(image => image.key)
            // images: ['intermix/BBBLK_1.jpg', 'intermix/BBBLK_2.jpg', 'intermix/BBBLK_3.jpg', 'intermix/BBBLK_4.jpg', 'intermix/BBBLK_5.jpg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 93500 },
                { size: '5.5', price: 93500 },
                { size: '6', price: 93500 },
                { size: '6.5', price: 93500 },
                { size: '7', price: 93500 },
                { size: '8', price: 93500 },
                { size: '9', price: 93500 },
                { size: '10', price: 93500 },
                { size: '11', price: 93500 },
                { size: '12', price: 93500 },
            ],
        }],
        brand: idBrandManolo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product7);

    const images8 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BING100PATPINK_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BING100PATPINK_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BING100PATPINK_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BING100PATPINK_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BING100PATPINK_5.jpg',
        },
    ];

    const product8: Product = {
        key: uuidv4(),
        name: 'Bing Patent Crystal-Embellished Mules',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'A classic party style, Jimmy Choo\'s Bing pumps in patent leather feature crystal-embellished ankle straps.' +
            'Composition: leather, glass ' +
            'Heel height: 4"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Jimmy Choo', 'Bing Patent Crystal-Embellished Mules'),
        startSize: 5,
        endSize: 12,
        price: 95000,
        colors: [idBeigeColor],
        sizes: [{
            color: idBeigeColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idBeigeColor,
            images: images8.map(image => image.key)
            // images: ['intermix/BING100PATPINK_1.jpg', 'intermix/BING100PATPINK_2.jpg', 'intermix/BING100PATPINK_3.jpg', 'intermix/BING100PATPINK_4.jpg', 'intermix/BING100PATPINK_5.jpg']
        }],
        prices: [{
            color: idBeigeColor,
            prices: [
                { size: '5', price: 95000 },
                { size: '5.5', price: 95000 },
                { size: '6', price: 95000 },
                { size: '6.5', price: 95000 },
                { size: '7', price: 95000 },
                { size: '8', price: 95000 },
                { size: '9', price: 95000 },
                { size: '10', price: 95000 },
                { size: '11', price: 95000 },
                { size: '12', price: 95000 },
            ],
        }],
        brand: idBrandJimmyChoo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product8);

    const images9 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'CCAMPARINUDE_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'CCAMPARINUDE_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'CCAMPARINUDE_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'CCAMPARINUDE_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'CCAMPARINUDE_5.jpg',
        },
    ];

    const product9: Product = {
        key: uuidv4(),
        name: 'Campari Patent Mary Jane Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Trimmed in tonal grosgrain, the label\'s Campari pumps are a patent leather re-imagining of the classic Mary Jane. Pointed toes. Side button closures.' +
            'Uppers: 100% calf leather' +
            'Lining: 100% kid leather' +
            'Soles: 100% cow leather' +
            'Heel height: 4"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Manolo Blahnik', 'Campari Patent Mary Jane Pumps'),
        startSize: 5,
        endSize: 12,
        price: 74500,
        colors: [idBeigeColor],
        sizes: [{
            color: idBeigeColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idBeigeColor,
            images: images9.map(image => image.key)
            // images: ['intermix/CCAMPARINUDE_1.jpg', 'intermix/CCAMPARINUDE_2.jpg', 'intermix/CCAMPARINUDE_3.jpg', 'intermix/CCAMPARINUDE_4.jpg', 'intermix/CCAMPARINUDE_5.jpg']
        }],
        prices: [{
            color: idBeigeColor,
            prices: [
                { size: '5', price: 74500 },
                { size: '5.5', price: 74500 },
                { size: '6', price: 74500 },
                { size: '6.5', price: 74500 },
                { size: '7', price: 74500 },
                { size: '8', price: 74500 },
                { size: '9', price: 74500 },
                { size: '10', price: 74500 },
                { size: '11', price: 74500 },
                { size: '12', price: 74500 },
            ],
        }],
        brand: idBrandManolo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product9);

    const images10 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'FEXHIGP0SUELEP_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'FEXHIGP0SUELEP_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'FEXHIGP0SUELEP_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'FEXHIGP0SUELEP_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'FEXHIGP0SUELEP_5.jpeg',
        },
    ];

    const product10: Product = {
        key: uuidv4(),
        name: 'Fenix 105 Suede Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Tailored along classic pointed toes, Aquazzura\'s Fenix pumps sweep back into thin stilettos along cut-out panels of colorblocked suede.' +
            'Uppers: kid suede' +
            'Lining: goat leather' +
            'Soles: calf leather' +
            'Heel height: 4.1"' +
            'Made in Italy ',
        id: '',
        slug: createSlug('Aquazzura', 'Fenix 105 Suede Pumps'),
        startSize: 5,
        endSize: 12,
        price: 76500,
        colors: [idMultiColor],
        sizes: [{
            color: idMultiColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idMultiColor,
            images: images10.map(image => image.key)
            // images: ['intermix/FEXHIGP0SUELEP_1.jpeg', 'intermix/FEXHIGP0SUELEP_2.jpeg', 'intermix/FEXHIGP0SUELEP_3.jpeg', 'intermix/FEXHIGP0SUELEP_4.jpeg', 'intermix/FEXHIGP0SUELEP_5.jpeg']
        }],
        prices: [{
            color: idMultiColor,
            prices: [
                { size: '5', price: 76500 },
                { size: '5.5', price: 76500 },
                { size: '6', price: 76500 },
                { size: '6.5', price: 76500 },
                { size: '7', price: 76500 },
                { size: '8', price: 76500 },
                { size: '9', price: 76500 },
                { size: '10', price: 76500 },
                { size: '11', price: 76500 },
                { size: '12', price: 76500 },
            ],
        }],
        brand: idBrandAquazzura,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product10);

    const images11 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'FR35121A12026110999_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'FR35121A12026110999_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'FR35121A12026110999_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'FR35121A12026110999_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'FR35121A12026110999_5.jpg',
        },
    ];

    const product11: Product = {
        key: uuidv4(),
        name: 'Wave 105 Ankle Wrap Pump',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Francesco Russo\'s Wave pumps wrap around the ankles in two-tones of smooth leather for a trend-right staple. Ankle tie closures.' +
            'Composition: leather' +
            'Heel height: 4.1"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Francesco Russo', 'Wave 105 Ankle Wrap Pump'),
        startSize: 5,
        endSize: 12,
        price: 83000,
        colors: [idBeigeColor],
        sizes: [{
            color: idBeigeColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idBeigeColor,
            images: images11.map(image => image.key)
            // images: ['intermix/FR35121A12026110999_1.jpg', 'intermix/FR35121A12026110999_2.jpg', 'intermix/FR35121A12026110999_3.jpg', 'intermix/FR35121A12026110999_4.jpg', 'intermix/FR35121A12026110999_5.jpg']
        }],
        prices: [{
            color: idBeigeColor,
            prices: [
                { size: '5', price: 83000 },
                { size: '5.5', price: 83000 },
                { size: '6', price: 83000 },
                { size: '6.5', price: 83000 },
                { size: '7', price: 83000 },
                { size: '8', price: 83000 },
                { size: '9', price: 83000 },
                { size: '10', price: 83000 },
                { size: '11', price: 83000 },
                { size: '12', price: 83000 },
            ],
        }],
        brand: idBrandFrancesco,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product11);

    const images12 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'HANGISIBRONZE_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'HANGISIBRONZE_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'HANGISIBRONZE_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'HANGISIBRONZE_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'HANGISIBRONZE_5.jpg',
        },
    ];

    const product12: Product = {
        key: uuidv4(),
        name: 'Hangisi Crystal Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Glistening lurex mesh pumps from Manolo Blahnik with signature Hangisi crystal buckles. Closed pointed toe. Slip-on style. 3" stiletto heel. Fabric upper. Leather lining and soles. In silver. Made in Italy.',
        id: '',
        slug: createSlug('Manolo Blahnik', 'Hangisi Crystal Pumps'),
        startSize: 5,
        endSize: 12,
        price: 102500,
        colors: [idSilverColor],
        sizes: [{
            color: idSilverColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idSilverColor,
            images: images12.map(image => image.key)
            // images: ['intermix/HANGISIBRONZE_1.jpg', 'intermix/HANGISIBRONZE_2.jpg', 'intermix/HANGISIBRONZE_3.jpg', 'intermix/HANGISIBRONZE_4.jpg', 'intermix/HANGISIBRONZE_5.jpg']
        }],
        prices: [{
            color: idSilverColor,
            prices: [
                { size: '5', price: 102500 },
                { size: '5.5', price: 102500 },
                { size: '6', price: 102500 },
                { size: '6.5', price: 102500 },
                { size: '7', price: 102500 },
                { size: '8', price: 102500 },
                { size: '9', price: 102500 },
                { size: '10', price: 102500 },
                { size: '11', price: 102500 },
                { size: '12', price: 102500 },
            ],
        }],
        brand: idBrandManolo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product12);

    const images13 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'LURUMBLK_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'LURUMBLK_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'LURUMBLK_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'LURUMBLK_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'LURUMBLK_5.jpg',
        },
    ];

    const product13: Product = {
        key: uuidv4(),
        name: 'Lurum Crystal-Embellished Satin Mules',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Event-ready stunners, Manolo Blahnik\'s sleek satin Lurum pumps dazzle with crystal-embellished uppers and ankle straps. ' +
            'Composition: polyester satin, leather, glass crystals' +
            'Heel height: 4"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Manolo Blahnik', 'Lurum Crystal-Embellished Satin Mules'),
        startSize: 5,
        endSize: 12,
        price: 129500,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images13.map(image => image.key)
            // images: ['intermix/LURUMBLK_1.jpg', 'intermix/LURUMBLK_2.jpg', 'intermix/LURUMBLK_3.jpg', 'intermix/LURUMBLK_4.jpg', 'intermix/LURUMBLK_5.jpg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 129500 },
                { size: '5.5', price: 129500 },
                { size: '6', price: 129500 },
                { size: '6.5', price: 129500 },
                { size: '7', price: 129500 },
                { size: '8', price: 129500 },
                { size: '9', price: 129500 },
                { size: '10', price: 129500 },
                { size: '11', price: 129500 },
                { size: '12', price: 129500 },
            ],
        }],
        brand: idBrandManolo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product13);

    const images14 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'MITHIGP0NAP000_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'MITHIGP0NAP000_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'MITHIGP0NAP000_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'MITHIGP0NAP000_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'MITHIGP0NAP000_5.jpeg',
        },
    ];

    const product14: Product = {
        key: uuidv4(),
        name: 'Minute 105 Leather Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Doubling down on re-imagining the classic Mary Jane, the Italian label\'s Minute stiletto pumps are an elegant essential in smooth leather. Back zip closure.' +
            'Composition: leather' +
            'Heel height: 4.1"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Aquazzura', 'Minute 105 Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 79500,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images14.map(image => image.key)
            // images: ['intermix/MITHIGP0NAP000_1.jpeg', 'intermix/MITHIGP0NAP000_2.jpeg', 'intermix/MITHIGP0NAP000_3.jpeg', 'intermix/MITHIGP0NAP000_4.jpeg', 'intermix/MITHIGP0NAP000_5.jpeg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 79500 },
                { size: '5.5', price: 79500 },
                { size: '6', price: 79500 },
                { size: '6.5', price: 79500 },
                { size: '7', price: 79500 },
                { size: '8', price: 79500 },
                { size: '9', price: 79500 },
                { size: '10', price: 79500 },
                { size: '11', price: 79500 },
                { size: '12', price: 79500 },
            ],
        }],
        brand: idBrandAquazzura,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product14);

    const images15 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'NREHIGM0SKOTPG_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'NREHIGM0SKOTPG_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'NREHIGM0SKOTPG_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'NREHIGM0SKOTPG_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'NREHIGM0SKOTPG_5.jpg',
        },
    ];

    const product15: Product = {
        key: uuidv4(),
        name: 'Rendez Vous 105 Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Night out-ready in snakeskin-embossed leather, the label\'s signature Rendez Vous pumps are secured with metallic gold-tone leather cording.' +
            'Composition: leather' +
            'Heel height: 4.1"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Aquazzura', 'Rendez Vous 105 Pumps'),
        startSize: 5,
        endSize: 12,
        price: 75000,
        colors: [idBeigeColor],
        sizes: [{
            color: idBeigeColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idBeigeColor,
            images: images15.map(image => image.key)
            // images: ['intermix/NREHIGM0SKOTPG_1.jpg', 'intermix/NREHIGM0SKOTPG_2.jpg', 'intermix/NREHIGM0SKOTPG_3.jpg', 'intermix/NREHIGM0SKOTPG_4.jpg', 'intermix/NREHIGM0SKOTPG_5.jpg']
        }],
        prices: [{
            color: idBeigeColor,
            prices: [
                { size: '5', price: 75000 },
                { size: '5.5', price: 75000 },
                { size: '6', price: 75000 },
                { size: '6.5', price: 75000 },
                { size: '7', price: 75000 },
                { size: '8', price: 75000 },
                { size: '9', price: 75000 },
                { size: '10', price: 75000 },
                { size: '11', price: 75000 },
                { size: '12', price: 75000 },
            ],
        }],
        brand: idBrandAquazzura,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product15);

    const images16 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'QW1S0393VB8P45_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'QW1S0393VB8P45_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'QW1S0393VB8P45_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'QW1S0393VB8P45_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'QW1S0393VB8P45_5.jpeg',
        },
    ];

    const product16: Product = {
        key: uuidv4(),
        name: 'Rockstud Leather Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'The ideal fashion-insider pump, Valentino crafts a caged pointed toe silhouette. The Italian label punctuates these beige stilettos with coordinating Rockstuds for a tone-on-tone finish. Ankle buckle closures.' +
            'Composition: leather, brass' +
            'Heel height: 3.9"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Valentino Garavani', 'Rockstud Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 109500,
        colors: [idBeigeColor],
        sizes: [{
            color: idBeigeColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idBeigeColor,
            images: images16.map(image => image.key)
            // images: ['intermix/QW1S0393VB8P45_1.jpeg', 'intermix/QW1S0393VB8P45_2.jpeg', 'intermix/QW1S0393VB8P45_3.jpeg', 'intermix/QW1S0393VB8P45_4.jpeg', 'intermix/QW1S0393VB8P45_5.jpeg']
        }],
        prices: [{
            color: idBeigeColor,
            prices: [
                { size: '5', price: 109500 },
                { size: '5.5', price: 109500 },
                { size: '6', price: 109500 },
                { size: '6.5', price: 109500 },
                { size: '7', price: 109500 },
                { size: '8', price: 109500 },
                { size: '9', price: 109500 },
                { size: '10', price: 109500 },
                { size: '11', price: 109500 },
                { size: '12', price: 109500 },
            ],
        }],
        brand: idBrandValentino,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product16);

    const images17 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1P270202201_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1P270202201_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1P270202201_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1P270202201_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1P270202201_5.jpg',
        },
    ];

    const product17: Product = {
        key: uuidv4(),
        name: 'Patent Leather Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Crafted in Italy from sleek patent leather, the label\'s take on the classic pump features dynamic angled vamps. Pointed toes. Slips on.' +
            'Composition: 100% leather' +
            'Heel height: 4"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Francesco Russo', 'Patent Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 64000,
        colors: [idWhiteColor],
        sizes: [{
            color: idWhiteColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idWhiteColor,
            images: images17.map(image => image.key)
            // images: ['intermix/R1P270202201_1.jpg', 'intermix/R1P270202201_2.jpg', 'intermix/R1P270202201_3.jpg', 'intermix/R1P270202201_4.jpg', 'intermix/R1P270202201_5.jpg']
        }],
        prices: [{
            color: idWhiteColor,
            prices: [
                { size: '5', price: 64000 },
                { size: '5.5', price: 64000 },
                { size: '6', price: 64000 },
                { size: '6.5', price: 64000 },
                { size: '7', price: 64000 },
                { size: '8', price: 64000 },
                { size: '9', price: 64000 },
                { size: '10', price: 64000 },
                { size: '11', price: 64000 },
                { size: '12', price: 64000 },
            ],
        }],
        brand: idBrandFrancesco,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product17);

    const images18 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'RAY100OPO_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'RAY100OPO_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'RAY100OPO_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'RAY100OPO_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'RAY100OPO_5.jpeg',
        },
    ];

    const product18: Product = {
        key: uuidv4(),
        name: 'Ray 100 Patent Leather Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Set a top their signature arched heels, the label\'s Ray pumps combine the quintessential slingback with the classic Mary Jane for a truly iconic closet staple in tortoiseshell-effect patent leather. Ankle buckle closures.' +
            'Composition: leather ' +
            'Heel height: 3.9"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Jimmy Choo', 'Ray 100 Patent Leather Pumps'),
        startSize: 5,
        endSize: 12,
        price: 75000,
        colors: [idOrangeColor],
        sizes: [{
            color: idOrangeColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idOrangeColor,
            images: images18.map(image => image.key)
            // images: ['intermix/RAY100OPO_1.jpeg', 'intermix/RAY100OPO_2.jpeg', 'intermix/RAY100OPO_3.jpeg', 'intermix/RAY100OPO_4.jpeg', 'intermix/RAY100OPO_5.jpeg']
        }],
        prices: [{
            color: idOrangeColor,
            prices: [
                { size: '5', price: 75000 },
                { size: '5.5', price: 75000 },
                { size: '6', price: 75000 },
                { size: '6.5', price: 75000 },
                { size: '7', price: 75000 },
                { size: '8', price: 75000 },
                { size: '9', price: 75000 },
                { size: '10', price: 75000 },
                { size: '11', price: 75000 },
                { size: '12', price: 75000 },
            ],
        }],
        brand: idBrandJimmyChoo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product18);

    const images19 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ROMYPUMPPINK_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ROMYPUMPPINK_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ROMYPUMPPINK_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ROMYPUMPPINK_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ROMYPUMPPINK_5.jpeg',
        },
    ];

    const product19: Product = {
        key: uuidv4(),
        name: 'Romy Wavy Satin Pumps',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Speckled with glistening rhinestones along wavy vamps, the label\'s Romy pumps are a luxe upgrade to a pointed toe staple.' +
            'Uppers: 72% viscose, 28% silk' +
            'Lining: 100% goat leather' +
            'Soles: 100% cow leather' +
            'Heel height: 3.7"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Ammina Muaddi', 'Romy Wavy Satin Pumps'),
        startSize: 5,
        endSize: 12,
        price: 108000,
        colors: [idPinkColor],
        sizes: [{
            color: idPinkColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idPinkColor,
            images: images19.map(image => image.key)
            // images: ['intermix/ROMYPUMPPINK_1.jpeg', 'intermix/ROMYPUMPPINK_2.jpeg', 'intermix/ROMYPUMPPINK_3.jpeg', 'intermix/ROMYPUMPPINK_4.jpeg', 'intermix/ROMYPUMPPINK_5.jpeg']
        }],
        prices: [{
            color: idPinkColor,
            prices: [
                { size: '5', price: 108000 },
                { size: '5.5', price: 108000 },
                { size: '6', price: 108000 },
                { size: '6.5', price: 108000 },
                { size: '7', price: 108000 },
                { size: '8', price: 108000 },
                { size: '9', price: 108000 },
                { size: '10', price: 108000 },
                { size: '11', price: 108000 },
                { size: '12', price: 108000 },
            ],
        }],
        brand: idBrandAmina,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product19);

    const images20 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BING100PAT_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BING100PAT_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BING100PAT_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BING100PAT_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'BING100PAT_5.jpg',
        },
    ];

    const product20: Product = {
        key: uuidv4(),
        name: 'Bing Patent Crystal-Embellished Mule',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'A classic party style, Jimmy Choo\'s Bing pumps in patent leather feature crystal-embellished ankle straps.' +
            'Composition: leather, glass' +
            'Heel height: 4"' +
            'Made in Italy' +
            'Item is listed in Italian',
        id: '',
        slug: createSlug('Jimmy Choo', 'Bing Patent Crystal-Embellished Mule'),
        startSize: 5,
        endSize: 12,
        price: 95000,
        colors: [idWhiteColor],
        sizes: [{
            color: idWhiteColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idWhiteColor,
            images: images20.map(image => image.key)
            // images: ['intermix/BING100PAT_1.jpg', 'intermix/BING100PAT_2.jpg', 'intermix/BING100PAT_3.jpg', 'intermix/BING100PAT_4.jpg', 'intermix/BING100PAT_5.jpg']
        }],
        prices: [{
            color: idWhiteColor,
            prices: [
                { size: '5', price: 95000 },
                { size: '5.5', price: 95000 },
                { size: '6', price: 95000 },
                { size: '6.5', price: 95000 },
                { size: '7', price: 95000 },
                { size: '8', price: 95000 },
                { size: '9', price: 95000 },
                { size: '10', price: 95000 },
                { size: '11', price: 95000 },
                { size: '12', price: 95000 },
            ],
        }],
        brand: idBrandJimmyChoo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps
    }
    products.push(product20);

    const images21 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ALEXTOELOWBLK_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ALEXTOELOWBLK_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ALEXTOELOWBLK_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ALEXTOELOWBLK_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'ALEXTOELOWBLK_5.jpg',
        },
    ];

    const product21: Product = {
        key: uuidv4(),
        name: 'Alex Patent Leather Booties',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Take a sleek step in these black patent leather boots from Alexandre Vauthier. Asymmetric top hem. Pull-on style.' +
            'Compositon:' +
            'Heel height: 4.3"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Alexandre Vauthier', 'Alex Patent Leather Booties'),
        startSize: 5,
        endSize: 12,
        price: 103500,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images21.map(image => image.key)
            // images: ['intermix/ALEXTOELOWBLK_1.jpg', 'intermix/ALEXTOELOWBLK_2.jpg', 'intermix/ALEXTOELOWBLK_3.jpg', 'intermix/ALEXTOELOWBLK_4.jpg', 'intermix/ALEXTOELOWBLK_5.jpg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 103500 },
                { size: '5.5', price: 103500 },
                { size: '6', price: 103500 },
                { size: '6.5', price: 103500 },
                { size: '7', price: 103500 },
                { size: '8', price: 103500 },
                { size: '9', price: 103500 },
                { size: '10', price: 103500 },
                { size: '11', price: 103500 },
                { size: '12', price: 103500 },
            ],
        }],
        brand: idBrandAlexandreVauthier,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    }
    products.push(product21);

    const images22 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'GIORGIABOOTWHT_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'GIORGIABOOTWHT_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'GIORGIABOOTWHT_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'GIORGIABOOTWHT_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'GIORGIABOOTWHT_5.jpeg',
        },
    ];

    const product22: Product = {
        key: uuidv4(),
        name: 'Giorgia Leather Ankle Boots',
        description: "",
        sizeAndFit: "PLEASE NOTE: This style runs small. We recommend taking one size up from your usual size. Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Elevated a top their signature sculpted heels, the label\'s Giorgia ankle boots are pointed along the toes in smooth leather. Side zip closures.' +
            'Composition: leather' +
            'Heel height: 3.7"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Ammina Muaddi', 'Giorgia Leather Ankle Boots'),
        startSize: 5,
        endSize: 12,
        price: 104500,
        colors: [idWhiteColor],
        sizes: [{
            color: idWhiteColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idWhiteColor,
            images: images22.map(image => image.key)
            // images: ['intermix/GIORGIABOOTWHT_1.jpeg', 'intermix/GIORGIABOOTWHT_2.jpeg', 'intermix/GIORGIABOOTWHT_3.jpeg', 'intermix/GIORGIABOOTWHT_4.jpeg', 'intermix/GIORGIABOOTWHT_5.jpeg']
        }],
        prices: [{
            color: idWhiteColor,
            prices: [
                { size: '5', price: 104500 },
                { size: '5.5', price: 104500 },
                { size: '6', price: 104500 },
                { size: '6.5', price: 104500 },
                { size: '7', price: 104500 },
                { size: '8', price: 104500 },
                { size: '9', price: 104500 },
                { size: '10', price: 104500 },
                { size: '11', price: 104500 },
                { size: '12', price: 104500 },
            ],
        }],
        brand: idBrandAmina,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    }
    products.push(product22);

    const images23 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'MALVINABOOTBLK_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'MALVINABOOTBLK_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'MALVINABOOTBLK_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'MALVINABOOTBLK_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'MALVINABOOTBLK_5.jpeg',
        },
    ];

    const product23: Product = {
        key: uuidv4(),
        name: 'Malvina Knee-High Patent Leather Boot',
        description: "",
        sizeAndFit: "PLEASE NOTE: This style runs small. We recommend taking one size up from your usual size. Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Contouring along the calf in their rigid patent leather, the label\'s Malvina 95 boots are an elegant knee-high staple. Side zip closures.' +
            'Composition: leather' +
            'Heel height: 3.7"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Ammina Muaddi', 'Malvina Knee-High Patent Leather Boot'),
        startSize: 5,
        endSize: 12,
        price: 134500,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images23.map(image => image.key)
            // images: ['intermix/MALVINABOOTBLK_1.jpeg', 'intermix/MALVINABOOTBLK_2.jpeg', 'intermix/MALVINABOOTBLK_3.jpeg', 'intermix/MALVINABOOTBLK_4.jpeg', 'intermix/MALVINABOOTBLK_5.jpeg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 134500 },
                { size: '5.5', price: 134500 },
                { size: '6', price: 134500 },
                { size: '6.5', price: 134500 },
                { size: '7', price: 134500 },
                { size: '8', price: 134500 },
                { size: '9', price: 134500 },
                { size: '10', price: 134500 },
                { size: '11', price: 134500 },
                { size: '12', price: 134500 },
            ],
        }],
        brand: idBrandAmina,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    }
    products.push(product23);

    const images24 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'VN1C602LGDT_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'VN1C602LGDT_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'VN1C602LGDT_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'VN1C602LGDT_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'VN1C602LGDT_5.jpeg',
        },
    ];

    const product24: Product = {
        key: uuidv4(),
        name: 'Salome Logo Leather Ankle Boots',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Secured with their signature logo \'B\' buckle plaques, the label\'s Salome ankle boots update your staple Chelseas in luxurious calfskin leather. Elasticized gore shaft. Back pull tabs. Pull-on style.' +
            'Composition: calfskin leather' +
            'Heel height: 2.2"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Balmain', 'Salome Logo Leather Ankle Boots'),
        startSize: 5,
        endSize: 12,
        price: 129500,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images24.map(image => image.key)
            // images: ['intermix/VN1C602LGDT_1.jpeg', 'intermix/VN1C602LGDT_2.jpeg', 'intermix/VN1C602LGDT_3.jpeg', 'intermix/VN1C602LGDT_4.jpeg', 'intermix/VN1C602LGDT_5.jpeg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 129500 },
                { size: '5.5', price: 129500 },
                { size: '6', price: 129500 },
                { size: '6.5', price: 129500 },
                { size: '7', price: 129500 },
                { size: '8', price: 129500 },
                { size: '9', price: 129500 },
                { size: '10', price: 129500 },
                { size: '11', price: 129500 },
                { size: '12', price: 129500 },
            ],
        }],
        brand: idBrandBalmain,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    }
    products.push(product24);

    const images25 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'GIORGIABOOTGLASSBLK_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'GIORGIABOOTGLASSBLK_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'GIORGIABOOTGLASSBLK_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'GIORGIABOOTGLASSBLK_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'GIORGIABOOTGLASSBLK_5.jpeg',
        },
    ];

    const product25: Product = {
        key: uuidv4(),
        name: 'Giorgia Leather Ankle Boots',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Elevated a top their signature sculpted plexiglass heels, the label\'s Giorgia ankle boots are pointed along the toes in smooth leather. Side zip closures.' +
            'Composition: leather, resin' +
            'Heel height" 3.7"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Ammina Muaddi', 'Giorgia Leather Ankle Boots'),
        startSize: 5,
        endSize: 12,
        price: 104500,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images25.map(image => image.key)
            // images: ['intermix/GIORGIABOOTGLASSBLK_1.jpeg', 'intermix/GIORGIABOOTGLASSBLK_2.jpeg', 'intermix/GIORGIABOOTGLASSBLK_3.jpeg', 'intermix/GIORGIABOOTGLASSBLK_4.jpeg', 'intermix/GIORGIABOOTGLASSBLK_5.jpeg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 104500 },
                { size: '5.5', price: 104500 },
                { size: '6', price: 104500 },
                { size: '6.5', price: 104500 },
                { size: '7', price: 104500 },
                { size: '8', price: 104500 },
                { size: '9', price: 104500 },
                { size: '10', price: 104500 },
                { size: '11', price: 104500 },
                { size: '12', price: 104500 },
            ],
        }],
        brand: idBrandAmina,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    }
    products.push(product25);

    const images26 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'PERNI05B102_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'PERNI05B102_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'PERNI05B102_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'PERNI05B102_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'PERNI05B102_5.jpg',
        },
    ];

    const product26: Product = {
        key: uuidv4(),
        name: 'Pointed Toe Leather Booties',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Pernille Teisbaek lends her styling expertise with these minimal leather booties cut with sharp pointed toes. Pull-on style.' +
            'Composition: leather' +
            'Heel height: 3.3"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Gia', 'Pointed Toe Leather Booties'),
        startSize: 5,
        endSize: 12,
        price: 67500,
        colors: [idWhiteColor],
        sizes: [{
            color: idWhiteColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idWhiteColor,
            images: images26.map(image => image.key)
            // images: ['intermix/PERNI05B102_1.jpg', 'intermix/PERNI05B102_2.jpg', 'intermix/PERNI05B102_3.jpg', 'intermix/PERNI05B102_4.jpg', 'intermix/PERNI05B102_5.jpg']
        }],
        prices: [{
            color: idWhiteColor,
            prices: [
                { size: '5', price: 67500 },
                { size: '5.5', price: 67500 },
                { size: '6', price: 67500 },
                { size: '6.5', price: 67500 },
                { size: '7', price: 67500 },
                { size: '8', price: 67500 },
                { size: '9', price: 67500 },
                { size: '10', price: 67500 },
                { size: '11', price: 67500 },
                { size: '12', price: 67500 },
            ],
        }],
        brand: idBrandGia,
        category: idCategoryShoes,
        subCategory: idSubCategoryBoots
    }
    products.push(product26);

    const images27 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'B350390013009_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'B350390013009_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'B350390013009_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'B350390013009_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'B350390013009_5.jpg',
        },
    ];

    const product27: Product = {
        key: uuidv4(),
        name: 'Clarita 90 Block Sandals',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'The label\'s must-have Clarita block heel sandals are a leg-lengthening style with self-tie ankle wraps. Finished with knotted toe straps.' +
            'Composition: 100% leather' +
            'Heel height: 3.5"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Alxeandre Birman', 'Clarita 90 Block Sandals'),
        startSize: 5,
        endSize: 12,
        price: 59500,
        colors: [idBeigeColor],
        sizes: [{
            color: idBeigeColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idBeigeColor,
            images: images27.map(image => image.key)
            // images: ['intermix/B350390013009_1.jpg', 'intermix/B350390013009_2.jpg', 'intermix/B350390013009_3.jpg', 'intermix/B350390013009_4.jpg', 'intermix/B350390013009_5.jpg']
        }],
        prices: [{
            color: idBeigeColor,
            prices: [
                { size: '5', price: 59500 },
                { size: '5.5', price: 59500 },
                { size: '6', price: 59500 },
                { size: '6.5', price: 59500 },
                { size: '7', price: 59500 },
                { size: '8', price: 59500 },
                { size: '9', price: 59500 },
                { size: '10', price: 59500 },
                { size: '11', price: 59500 },
                { size: '12', price: 59500 },
            ],
        }],
        brand: idBrandAlexandreBirman,
        category: idCategoryShoes,
        subCategory: idSubCategorySandals
    }
    products.push(product27);

    const images28 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '01458000WHITE_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '01458000WHITE_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '01458000WHITE_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '01458000WHITE_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: '01458000WHITE_5.jpg',
        },
    ];

    const product28: Product = {
        key: uuidv4(),
        name: 'Clarita 100 Leather Sandals',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'The label\'s must-have Clarita sandals are a leg-lengthening style with a self-tie ankle wraps. Finished with knotted toe straps.' +
            'Composition: leather' +
            'Heel height: 3.9"' +
            'Imported',
        id: '',
        slug: createSlug('Alxeandre Birman', 'Clarita Leather Block Sandals'),
        startSize: 5,
        endSize: 12,
        price: 59500,
        colors: [idWhiteColor],
        sizes: [{
            color: idWhiteColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idWhiteColor,
            images: images28.map(image => image.key)
            // images: ['intermix/01458000WHITE_1.jpg', 'intermix/01458000WHITE_2.jpg', 'intermix/01458000WHITE_3.jpg', 'intermix/01458000WHITE_4.jpg', 'intermix/01458000WHITE_5.jpg']
        }],
        prices: [{
            color: idWhiteColor,
            prices: [
                { size: '5', price: 59500 },
                { size: '5.5', price: 59500 },
                { size: '6', price: 59500 },
                { size: '6.5', price: 59500 },
                { size: '7', price: 59500 },
                { size: '8', price: 59500 },
                { size: '9', price: 59500 },
                { size: '10', price: 59500 },
                { size: '11', price: 59500 },
                { size: '12', price: 59500 },
            ],
        }],
        brand: idBrandAlexandreBirman,
        category: idCategoryShoes,
        subCategory: idSubCategorySandals
    }
    products.push(product28);

    const images29 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1S502213286_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1S502213286_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1S502213286_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1S502213286_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1S502213286_5.jpg',
        },
    ];

    const product29: Product = {
        key: uuidv4(),
        name: 'T-Strap Patent Leather Cage Sandal',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Set atop leg-lengthening stilettos, Francesco Russo updates their signature T-strap cage sandal with mirrored gold patent leather. Ankle buckle closures.' +
            'Composition: leather' +
            'Heel height: 4.1"' +
            'Made in Italy',
        id: '',
        slug: createSlug('Francesco Russo', 'T-Strap Patent Leather Cage Sanda'),
        startSize: 5,
        endSize: 12,
        price: 89000,
        colors: [idGoldColor],
        sizes: [{
            color: idGoldColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idGoldColor,
            images: images29.map(image => image.key)
            // images: ['intermix/R1S502213286_1.jpg', 'intermix/R1S502213286_2.jpg', 'intermix/R1S502213286_3.jpg', 'intermix/R1S502213286_4.jpg', 'intermix/R1S502213286_5.jpg']
        }],
        prices: [{
            color: idGoldColor,
            prices: [
                { size: '5', price: 89000 },
                { size: '5.5', price: 89000 },
                { size: '6', price: 89000 },
                { size: '6.5', price: 89000 },
                { size: '7', price: 89000 },
                { size: '8', price: 89000 },
                { size: '9', price: 89000 },
                { size: '10', price: 89000 },
                { size: '11', price: 89000 },
                { size: '12', price: 89000 },
            ],
        }],
        brand: idBrandFrancesco,
        category: idCategoryShoes,
        subCategory: idSubCategorySandals
    }
    products.push(product29);

    const images30 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1S502202200_1.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1S502202200_2.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1S502202200_3.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1S502202200_4.jpg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'R1S502202200_5.jpg',
        },
    ];

    const product30: Product = {
        key: uuidv4(),
        name: 'T-Strap Leather Stiletto Sandals',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Francesco Russo\'s patent leather t-strap sandal in classic black. Leg-lengthening 4" stiletto heels. Contrasting patent blush leather midsoles. Composition: 100% leather. In black. Made in Italy.',
        id: '',
        slug: createSlug('Francesco Russo', 'T-Strap Leather Stiletto Sandals'),
        startSize: 5,
        endSize: 12,
        price: 89000,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: images30.map(image => image.key)
            // images: ['intermix/R1S502202200_1.jpg', 'intermix/R1S502202200_2.jpg', 'intermix/R1S502202200_3.jpg', 'intermix/R1S502202200_4.jpg', 'intermix/R1S502202200_5.jpg']
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 89000 },
                { size: '5.5', price: 89000 },
                { size: '6', price: 89000 },
                { size: '6.5', price: 89000 },
                { size: '7', price: 89000 },
                { size: '8', price: 89000 },
                { size: '9', price: 89000 },
                { size: '10', price: 89000 },
                { size: '11', price: 89000 },
                { size: '12', price: 89000 },
            ],
        }],
        brand: idBrandFrancesco,
        category: idCategoryShoes,
        subCategory: idSubCategorySandals
    }
    products.push(product30);

    const images31 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'AURORABROWN_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'AURORABROWN_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'AURORABROWN_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'AURORABROWN_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'AURORABROWN_5.jpeg',
        },
    ];

    const product31: Product = {
        key: uuidv4(),
        name: 'Aurora Pointed Leather Flats',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Elegantly crafted in smooth brown calf leather, the label\'s Aurora flats feature sleek pointed square toes. Slip-on style.' +
            'Composition: leather' +
            'Made in Italy',
        id: '',
        slug: createSlug('AEYDE', 'Aurora Pointed Leather Flats'),
        startSize: 5,
        endSize: 12,
        price: 89000,
        colors: [idBrownColor],
        sizes: [{
            color: idBrownColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idBrownColor,
            images: images31.map(image => image.key)
            // images: ['intermix/AURORABROWN_1.jpeg', 'intermix/AURORABROWN_2.jpeg', 'intermix/AURORABROWN_3.jpeg', 'intermix/AURORABROWN_4.jpeg', 'intermix/AURORABROWN_5.jpeg']
        }],
        prices: [{
            color: idBrownColor,
            prices: [
                { size: '5', price: 25800 },
                { size: '5.5', price: 25800 },
                { size: '6', price: 25800 },
                { size: '6.5', price: 25800 },
                { size: '7', price: 25800 },
                { size: '8', price: 25800 },
                { size: '9', price: 25800 },
                { size: '10', price: 25800 },
                { size: '11', price: 25800 },
                { size: '12', price: 25800 },
            ],
        }],
        brand: idBrandAeyde,
        category: idCategoryShoes,
        subCategory: idSubCategoryFlats
    }
    products.push(product31);

    const images32 = [
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'TWIFLAA0NPGNN0_1.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'TWIFLAA0NPGNN0_2.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'TWIFLAA0NPGNN0_3.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'TWIFLAA0NPGNN0_4.jpeg',
        },
        {
            ...defaultImage,
            key: uuidv4(),
            filename: 'TWIFLAA0NPGNN0_5.jpeg',
        },
    ];

    const product32: Product = {
        key: uuidv4(),
        name: 'Mondaine Twisted Leather Flat Slides',
        description: "",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: 'Aquazzura twists their signature Mondaine flats in smooth leather with pointed faille cap toes. ' +
            'Composition: leather, polyester' +
            'Made in Italy',
        id: '',
        slug: createSlug('Aquazzura', 'Mondaine Twisted Leather Flat Slides'),
        startSize: 5,
        endSize: 12,
        price: 69500,
        colors: [idBeigeColor],
        sizes: [{
            color: idBeigeColor,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idBeigeColor,
            images: images32.map(image => image.key)
            // images: ['intermix/TWIFLAA0NPGNN0_1.jpeg', 'intermix/TWIFLAA0NPGNN0_2.jpeg', 'intermix/TWIFLAA0NPGNN0_3.jpeg', 'intermix/TWIFLAA0NPGNN0_4.jpeg', 'intermix/TWIFLAA0NPGNN0_5.jpeg']
        }],
        prices: [{
            color: idBeigeColor,
            prices: [
                { size: '5', price: 69500 },
                { size: '5.5', price: 69500 },
                { size: '6', price: 69500 },
                { size: '6.5', price: 69500 },
                { size: '7', price: 69500 },
                { size: '8', price: 69500 },
                { size: '9', price: 69500 },
                { size: '10', price: 69500 },
                { size: '11', price: 69500 },
                { size: '12', price: 69500 },
            ],
        }],
        brand: idBrandAquazzura,
        category: idCategoryShoes,
        subCategory: idSubCategoryFlats
    }
    products.push(product32);

    const images = [
        ...images1,
        ...images2,
        ...images3,
        ...images4,
        ...images5,
        ...images6,
        ...images7,
        ...images8,
        ...images9,
        ...images10,
        ...images11,
        ...images12,
        ...images13,
        ...images14,
        ...images15,
        ...images16,
        ...images17,
        ...images18,
        ...images19,
        ...images20,
        ...images21,
        ...images22,
        ...images23,
        ...images24,
        ...images25,
        ...images26,
        ...images27,
        ...images28,
        ...images29,
        ...images30,
        ...images31,
        ...images32,
    ];

    let cartItems: CartItem[] = [];
    const cartItemId1 = uuidv4();
    cartItems.push({
        key: cartItemId1,
        quantity: 1,
        product: keyProduct1,
        color: idColorBlack,
        size: '11',
        price: 65000,
        total: 65000,
        user: userId
    });

    const cartItemId2 = uuidv4();
    cartItems.push({
        key: cartItemId2,
        quantity: 1,
        product: keyProduct2,
        color: idHotPinkColor,
        size: '11',
        price: 99500,
        total: 99500,
        user: userId
    });


    let orders = [];

    const orderId = uuidv4();
    const order: Order = {
        key: orderId,
        id: 1,
        user: userId,
        total: 65000 + 99500,
        stripeId: "123131321231321232",
        date: moment().valueOf().toString(),
        timeStamp: moment().valueOf().toString()
    }
    orders.push(order);

    let orderItems = [];
    const orderItem1: OrderItem = {
        key: uuidv4(),
        quantity: 1,
        color: idColorBlack,
        size: '11',
        price: 65000,
        total: 65000,
        user: userId,
        product: keyProduct1,
        productName: 'Romy 85 Patent Leather Pumps',
        order: orderId,
        cartItem: cartItemId1,
        timeStamp: moment().valueOf().toString()
    }
    orderItems.push(orderItem1);

    const orderItem2: OrderItem = {
        key: uuidv4(),
        quantity: 1,
        color: idHotPinkColor,
        size: '11',
        price: 99500,
        total: 99500,
        user: userId,
        product: keyProduct2,
        productName: 'Hangisi Crystal Satin Pumps',
        order: orderId,
        cartItem: cartItemId2,
        timeStamp: moment().valueOf().toString()
    }
    orderItems.push(orderItem2);

    return {
        categories,
        subcategories,
        brands,
        products,
        colors,
        images,
        users,
        cartItems,
        orders,
        orderItems
    }
}
