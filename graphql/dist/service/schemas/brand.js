"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
var apollo_server_express_1 = require("apollo-server-express");
exports.default = apollo_server_express_1.gql(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    type Brand {\n        key: ID!\n        description: String\n        slug: String\n        timeStamp: String\n    }\n\n    extend type Query {\n        brands: [Brand]\n        brand(key: ID!): Brand\n        brandBySlug(slug: String!): Brand\n    }\n\n    extend type Mutation {\n        createBrand(description: String, slug: String): Brand\n    }\n"], ["\n    type Brand {\n        key: ID!\n        description: String\n        slug: String\n        timeStamp: String\n    }\n\n    extend type Query {\n        brands: [Brand]\n        brand(key: ID!): Brand\n        brandBySlug(slug: String!): Brand\n    }\n\n    extend type Mutation {\n        createBrand(description: String, slug: String): Brand\n    }\n"])));
var templateObject_1;
