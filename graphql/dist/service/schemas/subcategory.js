"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
var apollo_server_express_1 = require("apollo-server-express");
exports.default = apollo_server_express_1.gql(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    type SubCategory {\n        key: ID!\n        description: String\n        slug: String\n        timeStamp: String\n        categoryKey: String\n        category: Category!\n    }\n\n    extend type Query {\n        subcategories: [SubCategory]\n        subcategory(key: ID!): SubCategory\n        subcategoryBySlug(slug: String!): SubCategory\n    }\n\n    extend type Mutation {\n        createSubCategory(description: String, slug: String, categoryKey: String): SubCategory\n    }\n"], ["\n    type SubCategory {\n        key: ID!\n        description: String\n        slug: String\n        timeStamp: String\n        categoryKey: String\n        category: Category!\n    }\n\n    extend type Query {\n        subcategories: [SubCategory]\n        subcategory(key: ID!): SubCategory\n        subcategoryBySlug(slug: String!): SubCategory\n    }\n\n    extend type Mutation {\n        createSubCategory(description: String, slug: String, categoryKey: String): SubCategory\n    }\n"])));
var templateObject_1;
