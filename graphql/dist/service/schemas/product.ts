import { gql } from "apollo-server-express";

export default gql`
    type Images {
        color: Color
        images: [File]
    }

    type Sizes {
        color: Color
        sizes: [String]
    }

    type Price {
        size: String
        price: Float
    }

    type Prices {
        color: Color
        prices: [Price]
    }

    type Product {
        key: ID!
        name: String
        description: String
        sizeAndFit: String
        details: String
        timeStamp: String
        id: String
        slug: String
        startSize: Int,
        endSize: Int,
        price: Float,

        colors: [Color]
        images: [Images]
        sizes: [Sizes]
        prices: [Prices]

        category: Category
        subCategory: SubCategory
        brand: Brand
    }

    type AggregationCategory {
        category: Category
        subCategories: [SubCategory]
    }

    type Aggregations {
        count: Int
        brands: [Brand]
        sizes: [String]
        colors: [Color]
        categories: [AggregationCategory]
    }

    input ColorInput {
        name: String
        hex: String
    }

    input ImagesInput {
        color: String
        images: [String]
    }

    input SizesInput {
        color: String
        sizes: [String]
    }

    input PriceInput {
        size: String
        price: Float
    }

    input PricesInput {
        color: String
        prices: [PriceInput]
    }

    input ProductInput {
        key: ID!
        name: String!
        description: String!
        sizeAndFit: String
        details: String
        timeStamp: String
        id: String
        slug: String
        startSize: Int,
        endSize: Int,
        price: Float,

        colors: [String]
        images: [ImagesInput]
        sizes: [SizesInput]
        prices: [PricesInput]

        brand: String!
        category: String!
        subCategory: String!
    }

    extend type Query {
        products(category: [String], subCategory: [String], brands: [String], colors: [String], sizes: [String], skip: Int, first: Int): [Product]
        search(term: String!): [Product]
        aggregations(category: [String], subCategory: [String], brands: [String], colors: [String], sizes: [String]): Aggregations
        product(key: ID!): Product
    }

    extend type Mutation {
        createProduct(input: ProductInput): Product
        updateProduct(key: ID!, input: ProductInput): Product
        deleteProduct(key: ID!): Product
    }
`;
