import { gql } from "apollo-server-express";

export default gql`
    type Category {
        key: ID!
        description: String
        slug: String,
        timeStamp: String
        subcategories: [SubCategory]
    }

    extend type Query {
        categories: [Category]
        category(key: ID!): Category
        categoryBySlug(slug: String!): Category
    }

    extend type Mutation {
        createCategory(description: String, slug: String): Category
    }
`;
