import { gql } from "apollo-server-express";

export default gql`
    type CartItem {
        key: ID!
        quantity: Int
        color: Color
        size: String
        price: Float
        total: Float
        user: User
        product: Product
    }

    extend type Query {
        cartItem(key: ID!): CartItem
        itemsUser(user: ID!): [CartItem]
    }

    extend type Mutation {
        addCartItem(quantity: Int!, color: ID!, size: String!, productKey: ID!): CartItem
        removeCartItem(key: String!): CartItem
    }
`;
