import { gql } from "apollo-server-express";

export default gql`
    input OrderItemInput {
        quantity: Float
        color: String
        size: String
        price: Float
        total: Float
        user: String
        product: String
        order: String
        cartItem: String
    }

    type OrderItem {
        key: ID!
        quantity: Float
        color: Color
        size: String
        price: Float
        total: Float
        user: User
        product: Product
        productName: String
        order: Order
        cartItem: CartItem
        timeStamp: String
    }

    extend type Query {
        orderItemByOrder(order: ID!): [OrderItem]
        orderItem(key: ID!): OrderItem
    }

    extend type Mutation {
        createOrderItem(input: OrderItemInput): OrderItem
    }
`;
