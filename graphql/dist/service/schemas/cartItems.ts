import { gql } from "apollo-server-express";

export default gql`
    type CartItem {
        key: ID!
        quantity: Int
        price: Float
        total: Float
        user: User
        product: Product
    }

    extend type Query {
        cartItem(key: ID!): CartItem
        itemsUser(user: ID!): [CartItem]
    }

    extend type Mutation {
        addCartItem(quantity: Int!, productKey: ID!, userKey: ID!): CartItem
    }
`;
