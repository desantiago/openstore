const OrderItemResolvers = {
    Query: {
        orderItem: (_: any, { key }: any, { data }: any) => data.getOrderItem(key),
        orderItemByOrder: (_: any, { order }: any, { data }: any) => data.getOrderItemByOrder(order),
    },
    Mutation: {
        createOrderItem: (_: any, { input }: any, { data }: any) => {
            return data.addOrderItem(input);
        }
    },
    OrderItem: {
        color: (orderItem: any, args: any, { data }: any) => {
            return data.getColor(orderItem.color)
        },
        user: (orderItem: any, args: any, { data }: any) => {
            return data.getUser(orderItem.user);
        },
        product: (orderItem: any, args: any, { data }: any) => {
            return data.getProduct(orderItem.product);
        },
        order: (orderItem: any, args: any, { data }: any) => {
            return data.getOrder(orderItem.order);
        },
        cartItem: (orderItem: any, args: any, { data }: any) => {
            return data.getCartItem(orderItem.cartItem);
        }
    }
};

export default OrderItemResolvers;