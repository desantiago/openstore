"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrderItemResolvers = {
    Query: {
        orderItem: function (_, _a, _b) {
            var key = _a.key;
            var data = _b.data;
            return data.getOrderItem(key);
        },
        orderItemByOrder: function (_, _a, _b) {
            var order = _a.order;
            var data = _b.data;
            return data.getOrderItemByOrder(order);
        },
    },
    Mutation: {
        createOrderItem: function (_, _a, _b) {
            var input = _a.input;
            var data = _b.data;
            return data.addOrderItem(input);
        }
    },
    OrderItem: {
        color: function (orderItem, args, _a) {
            var data = _a.data;
            return data.getColor(orderItem.color);
        },
        user: function (orderItem, args, _a) {
            var data = _a.data;
            return data.getUser(orderItem.user);
        },
        product: function (orderItem, args, _a) {
            var data = _a.data;
            return data.getProduct(orderItem.product);
        },
        order: function (orderItem, args, _a) {
            var data = _a.data;
            return data.getOrder(orderItem.order);
        },
        cartItem: function (orderItem, args, _a) {
            var data = _a.data;
            return data.getCartItem(orderItem.cartItem);
        }
    }
};
exports.default = OrderItemResolvers;
