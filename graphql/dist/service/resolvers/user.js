"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserResolvers = {
    Query: {
        user: function (_, _a, _b) {
            var id = _a.id;
            var data = _b.data, me = _b.me;
            return data.getUser(id);
        },
        authenticatedUser: function (_, _a, _b) {
            var data = _b.data, me = _b.me;
            if (me)
                return data.getUser(me.id);
        }
    },
    Mutation: {
        signUp: function (_, _a, _b) {
            var name = _a.name, email = _a.email, password = _a.password;
            var data = _b.data, secret = _b.secret;
            return data.signUp(name, email, password, secret);
        },
        signIn: function (_, _a, _b) {
            var email = _a.email, password = _a.password;
            var data = _b.data, secret = _b.secret;
            return data.signIn(email, password, secret);
        }
    },
    User: {
        cartItems: function (user, args, _a) {
            var data = _a.data;
            console.log('cartItems ', user.id);
            return data.getItemsUser(user.id);
        }
    }
};
exports.default = UserResolvers;
