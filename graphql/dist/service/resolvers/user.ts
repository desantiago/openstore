const UserResolvers = {
    Query: {
        // subcategories: (_: any, __: any, { data }: any) => data.getSubCategories(),
        user: (_: any, { id }: any, { data, me }: any) => {
            //console.log(me);
            return data.getUser(id)
        },
        authenticatedUser: (_: any, { }: any, { data, me }: any) => {
            if (me) return data.getUser(me.id)
        }
        // subcategoryBySlug: (_: any, { slug }: any, { data }: any) => data.getSubCategoryBySlug(slug),
    },
    Mutation: {
        signUp: (_: any, { name, email, password }: any, { data, secret }: any) => {
            return data.signUp(name, email, password, secret);
        },
        signIn: (_: any, { email, password }: any, { data, secret }: any) => {
            return data.signIn(email, password, secret);
        }
    },
    User: {
        cartItems: (user: any, args: any, { data }: any) => {
            console.log('cartItems ', user.id);
            return data.getItemsUser(user.id)
        }
    }
    // SubCategory: {
    //     category: (subcategory: any, args: any, { data }: any) => {
    //         return data.getCategory(subcategory.categoryKey);
    //     }
    // }
};

export default UserResolvers;
