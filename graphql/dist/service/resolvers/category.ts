const CategoryResolvers = {
    Query: {
        categories: (_: any, __: any, { data }: any) => data.getCategories(),
        category: (_: any, { key }: any, { data }: any) => data.getCategory(key),
        categoryBySlug: (_: any, { slug }: any, { data }: any) => data.getCategoryBySlug(slug),
    },
    Mutation: {
        createCategory: (_: any, { description, slug }: any, { data }: any) => {
            return data.addCategory(description, slug);
        }
    },
    Category: {
        subcategories: (category: any, args: any, { data }: any) => {
            return data.getSubCategoriesCategory(category.key)
        }
    }
    // Book: {
    //     author: (book: any, args: any, { models }: any) => {
    //         return models.authors.find((author: any) => author.id === book.author) 
    //     }
    // },
};

export default CategoryResolvers;