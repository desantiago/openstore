
const CartItemResolvers = {
    Query: {
        cartItem: (_: any, { key }: any, { data }: any) => data.getCartItem(key),
        itemsUser: (_: any, { user }: any, { data }: any) => data.getItemsUser(user)
    },
    Mutation: {
        addCartItem: (_: any, { quantity, color, size, productKey }: any, { data, me }: any) => {
            //TODO: check if user is logged
            return data.addCartItem(quantity, color, size, productKey, me.id);
        },
        removeCartItem: (_: any, { key }: any, { data, me }: any) => {
            //TODO: check if user is logged
            return data.removeCartItem(key);
        }
    },
    CartItem: {
        product: (cartItem: any, args: any, { data }: any) => {
            return data.getProduct(cartItem.product);
        },
        color: (cartItem: any, args: any, { data }: any) => {
            return data.getColor(cartItem.color);
        },
    }
};

export default CartItemResolvers;