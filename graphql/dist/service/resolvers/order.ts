const OrderResolvers = {
    Query: {
        orders: (_: any, __: any, { data }: any) => data.getOrders(),
        order: (_: any, { key }: any, { data }: any) => data.getOrder(key),
        ordersByUser: (_: any, { user }: any, { data }: any) => data.getOrdersByUser(user),
    },
    Mutation: {
        createOrder: (_: any, { total, user, stripeId }: any, { data }: any) => {
            return data.addOrder(total, user, stripeId);
        },
        checkOut: (_: any, { stripeId }: any, { data, me }: any) => {
            if (!me) return null;
            return data.checkOut(stripeId, me);
        }
    },
    Order: {
        orderItems: (order: any, args: any, { data }: any) => {
            return data.getOrderItemByOrder(order.key)
        },
        user: (order: any, args: any, { data }: any) => {
            return data.getUser(order.user);
        }
    }
};

export default OrderResolvers;