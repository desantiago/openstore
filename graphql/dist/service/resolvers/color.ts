
const ColorResolvers = {
    Query: {
        colors: (_: any, __: any, { data }: any) => data.getColors(),
        color: (_: any, { key } : any, { data }: any) => data.getColor(key),
    },
    Mutation: {
        createColor: (_: any, { name, hex }: any, { data }: any) => {
            return data.addColor(name, hex);
        }
    }
};

export default ColorResolvers;