const ProductResolvers = {
    //products(category: string!, subcategory: string, brand: string!)
    Query: {
        products: (_: any, { category, subCategory, brands, colors, sizes, skip, first, }: any, { data }: any) => data.getProducts(category, subCategory, brands, colors, sizes, skip, first),
        aggregations: (_: any, { category, subCategory, brands, colors, sizes }: any, { data }: any) => data.aggregations(category, subCategory, brands, colors, sizes),
        search: (_: any, { term }: any, { data }: any) => data.searchProducts(term),
        product: (_: any, { key }: any, { data }: any) => data.getProduct(key),
    },
    Mutation: {
        createProduct: (_: any, { input }: any, { data }: any) => {
            return data.addProduct(input);
        },
        updateProduct: (_: any, { key, input }: any, { data }: any) => {
            return data.updateProduct(key, input);
        },
        deleteProduct: (_: any, { key }: any, { data }: any) => {
            return data.deleteProduct(key);
        }
    },
    Product: {
        brand: (product: any, args: any, { data }: any) => {
            return data.getBrand(product.brand)
        },
        category: (product: any, args: any, { data }: any) => {
            return data.getCategory(product.category)
        },
        subCategory: (product: any, args: any, { data }: any) => {
            return data.getSubCategory(product.subCategory)
        },
        colors: (product: any, args: any, { data }: any) => {
            return product.colors.map((color: string) => data.getColor(color));
        }
    },
    Images: {
        color: (image: any, args: any, { data }: any) => {
            return data.getColor(image.color);
        },
        images: ({ images }: any, args: any, { data }: any) => {
            //return data.getColor(image.color);
            //console.log("-------");
            //console.log(images);
            return images.map((imageKey: string) => data.getFile(imageKey));
        }
    },
    Prices: {
        color: (price: any, args: any, { data }: any) => {
            return data.getColor(price.color);
        }
    },
    Sizes: {
        color: (size: any, args: any, { data }: any) => {
            return data.getColor(size.color);
        }
    }
    // SubCategory: {
    //     category: (subcategory: any, args: any, { data }: any) => {
    //         return data.getCategory(subcategory.categoryKey);
    //     }
    // }
};

export default ProductResolvers;
