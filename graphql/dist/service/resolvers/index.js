"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var category_1 = __importDefault(require("./category"));
var subcategory_1 = __importDefault(require("./subcategory"));
var brand_1 = __importDefault(require("./brand"));
var product_1 = __importDefault(require("./product"));
var color_1 = __importDefault(require("./color"));
var file_1 = __importDefault(require("./file"));
var user_1 = __importDefault(require("./user"));
var cartItem_1 = __importDefault(require("./cartItem"));
var order_1 = __importDefault(require("./order"));
var orderItem_1 = __importDefault(require("./orderItem"));
exports.default = [
    category_1.default,
    subcategory_1.default,
    brand_1.default,
    product_1.default,
    color_1.default,
    file_1.default,
    user_1.default,
    cartItem_1.default,
    order_1.default,
    orderItem_1.default
];
